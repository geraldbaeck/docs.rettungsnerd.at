title:          PHTLS
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        praxis
template:       sop.html
md5:            e69f3f030f42f236edad5771ca58746c
date_modified:  2019/04/26
date:           2019/04/01

## 3S

### Scene, Safety, Situation

+ Scene: Einsatzmeldung; Was, Wo, Wie, Wetter?; Equipment
+ Safety: Einsatzstelle, PSA, Helme, Warnwesten, Schutzbrille
+ Situation: Wie viele Verletzte? Zusätzliche Rettungsmittel?

## GI

### General Impression

+ **<c> Signifikante Blutungen stoppen**
+ Bewusstsein
    + AVPU
    + Patientenverhalten (zittern, agitiert, etc.)
+ Hautbild
    + Farbe
    + Schweiß?
+ Atmung
    + Atemmuster
    + Tachypnoe / Bradypnoe
+ Unfallmechanismus beurteilen
+ Maßnahmen: ***Blutstillung, MILS, drehen oder aufsetzen, Sauerstoff, Defipads kleben***

## A

### Airway

+ Inspektion (Aufforderung Zunge herauszustrecken, Mund manuell öffnen)
+ partielle Verlegung durch Zunge => ***Chinlift/Esmarch => Güdel/Wendel***
+ Verlegung durch Fremdkörper => ***Absaugen, Ausräumen***

## B

### Breathing

+ Trachea mittelständig?
+ Halsvenen gestaut?
+ Thorax stabil?
+ Thoraxverletzungen, Thoraxexkursion und Stabilität
+ Atemfrequenz auszählen => <10 >30 => ***assistierte Beatmung***
+ Gebrauch der Atemhilfsmuskulatur?
+ hebt beidseitig?
+ Auskultieren immer im Seitenvergleich
+ Maßnahme: ***Sauerstoff, Lagerung, 3seiten-Verband***

## C

### Circulation

+ Pulsmessung peripher und zentral, ggf peripher beidseitig
    + Qualität
    + Stärke
    + Rhythmus
    + Frequenz
+ Haut
    + Kolorit
    + Schweißig?
    + Temperatur
+ Rekap
    + peripher und zentral
+ Blutungsräume kontrollieren
    + Abdomen
    + Becken ([KISS](/praxis/Schemen/KISS)) => ***Beckengurt***
    + Oberschenkel
+ Maßnahme: ***Beckengurt, Defipads kleben, Volumengabe (Ziel RR 90/x), Lagerung (CAVE: MCI)***

## D

### Disability

+ Pupillenbefund
+ MDS an allen Extremitäten
+ AVPU (GCS) bestimmen

## E

### Expose

+ Untersuchen suspekter Körperregionen
+ Maßnahme: ***Wärmeerhalt***

## E

### 10 for 10

+ Untersuchungsergebnisse zusammenfassen
+ Zeitmanagement beachten
+ Transportpriorität
+ Indikation zur Ganzkörperimmobilisation klären
+ **Entscheidung kritisch / nicht kritisch**

## &nbsp;

### Secondary Survey

+ SAMPLER - S
+ Bodycheck / Ganzkörperuntersuchung / Traumcheck
    + Palpieren und Sichten
    + MDS an allen Extremitäten
+ Vitalwerte: AF, S<sub>p</sub>O&#8322;, HF, RR, EKG, KKT, BZ (aka 7 Zwerge)

## &nbsp;

### Finally another 10 for 10

+ Spezifische Maßnahmen
+ Neubeurteilung
+ Lagerung des Patienten
+ Transportentscheidung
+ **Reevaluierung nach jeder Lageänderung und jeder Maßnahme**