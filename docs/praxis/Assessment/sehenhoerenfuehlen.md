title:          Hören, Sehen, Tasten und Riechen
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        praxis
md5:            315ad5772abff69e6ec02bef1e52b704
date_modified:  2019/06/07
date:           2019/04/20

| | |
| --- | --- |
| Hören | Atemgeräusche, Darmgeräusche, Herztöne |
| Sehen | Schwellungen, Blutung, Verletzung, Atembewegung, Hautkolorit |
| Tasten | Abdomen, Puls, Temperatur, Frakturen... |
| Riechen | Ausatemluft, Alkohol, Meläna (Blutstuhl) |