title:          Auskultieren
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        praxis
md5:            33767dd8f9dd2a30761a750a84dc92ec
date_modified:  2019/06/30
date:           2019/04/01

## Vorne

+ 4 Punkte
+ zweiter ICR media clavicular links und rechts
+ fünfter ICR vordere Axiliarlinie links und rechts

## Hinten

+ 6 Punkte