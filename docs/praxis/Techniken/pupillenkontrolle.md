title:          Pupillenkontrolle
date:           2019/01/06
version:        0.0.4
authors:        gerald@baeck.at
md5:            31a92353178f56e5e001c0c7b864365d
ankikat:        praxis
date_modified:  2019/06/27

## Pupillenkontrolle I (Efferenzkontrolle)

### Ungleiche Pupillenweite (Anisokorie)

Die gestörte Pupille reagiert weniger auf Licht.

### Weite Pupille

+ Ausfall des Parasympathikus auf dieser Seite.
+ evt Hirnödem, Aneurisma, Hirntumor
+ aber auch einseitig angewandte Augentropfen od Glasauge
+ oder Augapfelprellung

### Enge Pupille

+ Überstimulation des Parasympathikus auf dieser Seite
+ zb Augentropfen

### Beidseitig weite Pupillen

+ Parasympatholytika: zb Tollkirsche, Psychopharmaka, Antihistminika
+ Sympathomimethika: Kokain, Amphetamin, Adrenalin
+ aber auch Erblindung, Krampfanfall, Glaukom (grüner Star)

### Beidseitig enge Pupillen

+ Opioide
+ Kohlenmonoxid
+ Narkotika

## Pupillenkontrolle II (Afferenzkontrolle)

Vergleich der direkten und indirekten (=konsensuellen) Lichtreaktion

+ Patient blickt in die Ferne (Ausschaltung der Naheinstellungsreaktion)
+ Auge von unten beleuchten (gleichmäßiges Streulicht im Fundus)
+ Nach 2-3 Sekunden wechselt man schnell die Lichtquelle auf das andere Auge, wobei man nur das beleuchtete Auge beobachtet (Wechselbelichtungstest)
+ Normalerweise ändert die Pupille beim Übergang von der konsensuellen zur direkten Lichtreaktion ihre weite nicht.
+ Wird die neu beleuchtete Pupille aber weiter => **Afferenzdefekt**