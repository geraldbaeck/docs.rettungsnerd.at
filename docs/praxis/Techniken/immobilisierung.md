title:          Immobilisierung
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        praxis
md5:            67d8f61dc2dbf52573963a493a3cd01f
date_modified:  2019/05/03
date:           2019/04/22

| Möglichkeiten | Indikationen |
| --- | --- |
| Schaufeltrage / Vakuummatratze | Ws- Verletzungen, Oberschenkelfraktur, Schenkelhalsfraktur |
| Vakuumarmschiene | Schienung von Unterarmfrakturen |
| Vakuumbeinschiene | Schienung von Unterschenkelfrakturen, Ganzkörperimmo Kinder |
| Spineboard | Ganzkörperimmo, Alternative Vakuummatratze |
| Aluminiumpolsterschiene (Sam Splint) | Unterarm- & Sprunggelenksfrakturen, Druckkörper |
| Beckengurt | Beckenfraktur |
| Zervikalstütze | HWS-Verletzungen, immer mit Ganzkörperimmo |
| Dreieckstuch | Unterarmverletzungen, -frakturen |