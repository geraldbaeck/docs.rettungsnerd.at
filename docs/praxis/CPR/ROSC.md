title:          ROSC
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        praxis
md5:            f8b5cc19ee4d626c86263de26e9ba90e
date_modified:  2019/04/01
date:           2019/04/01

# A

+ ggf Intubieren od. Larynxtubus
+ ggf Narkoseeinleitung

# B

+ Belüftung/Ventilation prüfen
+ Tidalvolumen und Frequenz prüfen
+ Thoraxexkursionen, etCO&#8322;, Auskultation
+ O&#8322; Vorrat berechnen
+ Maschinelle Beatmung erwägen

# C

+ Pulsqualität überprüfen
+ RR messen
+ BZ?
+ EKG
+ Temperatur Messung?
+ Hautbeschaffenheit, -temperatur
+ Zugang legen/fixieren
+ ggf Infusion, Medikamente

# D

+ Auffälligkeiten
+ Schluckreflexe
+ Atemreflexe
+ Pupillen
+ ggf Narkoseeinleitung

# E

+ Anamnese
+ SAMPLER
+ Sonstige Auffälligkeiten, Verletzungen
+ Therapeutische Hypothermie 34°C KKT