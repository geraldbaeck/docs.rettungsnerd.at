title:          4 Hs und HITS
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        praxis
md5:            9f214989cf6b29ec599588a384baf77d
date_modified:  2019/04/01
date:           2019/04/01

# Hs

+ Hypoxie
+ Hypovolämie
+ Hypokaliämie / Hypoglykämie
+ Hypothermie

# HITs

+ Herzbeuteltamponade => Perikardpunktion
+ Intoxination => Antidot, Eliminationsverfahren
+ Thromboembolisches Geschehen => Thrombolyse
+ Spannungspneumothorax => Entlastungspunktion/Thoraxdrainage