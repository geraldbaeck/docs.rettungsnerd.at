title:          CPR Medikamente
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        praxis
md5:            6059330d2d33687b6bb0948794c28b2c
date_modified:  2019/05/15
date:           2019/05/15

## Schockbarer Rhythmus

Erwachsener:

+ L-Adrenalin (1:10.000) 1mg (=10ml) alle 4 min bzw. nach 3., 5., 7. und 9. Schock
+ Amiodaron (Sedacoron) 300mg auf 20ml 0,9% NaCl verdünnt nach 3. Schock (2 Ampullen)
+ Amiodaron (Sedacoron) 150mg auf 20ml 0,9% NaCl verdünnt nach 5. Schock (1 Ampulle)

Kind:

+ L-Adrenalin (1:10.000) 0,01mg / kg (= 1ml / 10kg) alle 4 min bzw. nach 3., 5., 7. und 9. Schock
+ Amiodaron (Sedacoron) 5mg / kg (= 1ml / 10kg) nach 3. Schock

## Nicht Schockbarer Rhythmus

Erwachsener:

+ L-Adrenalin (1:10.000) 1mg (=10ml) alle 4 min

Kind:

+ L-Adrenalin (1:10.000) 0,01mg / kg (= 1ml / 10kg) alle 4 min