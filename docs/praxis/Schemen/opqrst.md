title:          OPQRST
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        praxis
md5:            968a6974b412d0520f4381806f877e1b
date_modified:  2019/04/01
date:           2019/01/10

## Onset - Beginn

+ Wann haben die Schmerzen begonnen
+ Was hat der Patient getan (aktiv, inaktiv, gestresst) als die Schmerzen begannen
+ Traten die Schmerzen plötzlich oder schleichend auf
+ Sind die Schmerzen Teil eines chronischen Problems?

## Provocation, Palliation - Verstärkung, Linderung

Was verschlimmert oder lindert die Schmerzen. (Bewegung, Ruhe, spezifische Lage, Palpation)

## Quality - Qualität

+ Der Patient beschreibt seine Schmerzen
+ Fragen sollten nach Möglichkeit offen sein ("Können Sie mir den Schmerz beschreiben?")

Idealerweise resultiert die Qualitätserhebung zu einer detaillierten Schmerzbeschreibung wie zum Beispiel

+ stechend,
+ stumpf,
+ brechend, (wtf???)
+ brennend oder
+ zerreißend

gemeinsam mit dem Muster wie

+ pulsierend,
+ konstant oder
+ ansteigend.

## Region, radiation - Region, Ausstrahlung

+ ist nur auf eine bestimmte Region betroffen oder
+ strahlen die Schmerzen auf andere Regionen aus.

Dadurch können Hinweise für einen möglichen Herzinfarkt gefunden werden, der in den linken Arm oder in den Kiefer ausstrahlen kann.

## Severity - Stärke

Wie stark sind die Beschwerden? Üblicherweise wird eine Skala von 0 bis 10 verwendet, wobei Null keine Schmerzen und zehn der schlimmste vorstellbare Schmerz (zb Geburtsschmerz) ist.

## Time - Zeitlicher Verlauf

+ Sind solche Beschwerden früher schon einmal aufgetreten?
+ Haben sich die Beschwerden im zeitlichen Verlauf verändert?
+ Wie lange bestehen die Schmerzen bereits?