title:          AVPU
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        praxis
md5:            f9f416aea7128b4c0fa6f9107d9bb128
date_modified:  2019/04/01
date:           2019/04/01

Beurteilung des Patienten nach dem AVPU Schema ist schon im Rahmen des Ersteindrucks möglich.  

A
: Alert - wach, ansprechbar und orientiert - GCS 15  

V
: Verbal Response - reagiert nur auf laute Ansprache - GCS 12  

P
: Painful stimuli - reagiert nur auf Schmerz - GCS 8  

U
: Unresponsive - nicht ansprechbar - GCS 3