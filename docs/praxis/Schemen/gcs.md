title:          GCS
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        praxis
md5:            c85041d65bd661cb06506a16494069a3
date_modified:  2019/01/23
date:           2019/01/23

| Reaktion | Score |
| --- | --- |
| AUGEN öffnen{: .trtrenner} | {: .trtrenner} |
| spontan | 4 |
| auf Aufforderung | 3 |    
| auf Schmerzreiz | 2 |
| keine Reaktion | 1 |
| VERBALe Kommunikation{: .trtrenner} | {: .trtrenner} |
| konversationsfähig, orientiert | 5 | 
| konversationsfähig, desorientiert | 4 | 
| unzusammenhängende Worte | 3 | 
| unverständliche Laute | 2 | 
| keine verbale Reaktion | 1 |  
| MOTORISCHe Reaktion{: .trtrenner} | {: .trtrenner} |
| befolgt Aufforderungen | 6 | 
| gezielte Schmerzabwehr | 5 | 
| ungezielte Schmerzabwehr | 4 | 
| auf Schmerzreiz Beugesynergismen (abnormale Beugung) | 3 | 
| auf Schmerzreiz Strecksynergismen | 2 | 
| keine Reaktion auf Schmerzreiz | 1 |