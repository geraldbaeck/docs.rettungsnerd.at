title:          FAST+
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        praxis
md5:            8825ea3ff7b61d9094ba76817933b831
date_modified:  2019/04/20
date:           2019/04/01

## Face

+ lächeln lassen
+ Ist das Gesicht einseitig gelähmt/verzogen?
+ Hängen Mundwinkel? evt. Speichelfluss
+ bei Frauen evt. massiver Schluckauf

## Arms

+ Beide Arme nach vorne strecken
+ Handflächen nach oben
+ Augen schließen
+ Gleiche Kraft in beiden Händen?
+ Gibt es neurologische Ausfälle? (Kribbeln, Gefühllosigkeit)

## Speech

+ Einfachen Satz nachsprechen lassen
+ Klingt die Sprache verwaschen oder anders? (Fremdanamnese)

## Time

+ Seit wann bestehen die Symptome?
+ Time is brain!

## Plus

+ Beine
+ Herdblick

## APSS Score

### Face

| soll lächeln | Score |
| --- | --- |
| seitengleich | 0 |
| unterschiedlich | 2 |
| nicht beurteilbar | 0 |

### Arms

| Arme hochhalten | Score |
| --- | --- |
| seitengleich | 0 |
| Unterschied oder Absinken | 1 |
| Nur mit einem Arm möglich | 2 |
| nicht beurteilbar | 0 |

### Speech

| Satz wiederholen | Score |
| --- | --- |
| deutlich | 0 |
| undeutlich, verwaschen | 1 |
| konnte nicht sprechen | 2 |
| nicht beurteilbar | 0 |

### Beine

| Beine hochhalten | Score |
| --- | --- |
| seitengleich | 0 |
| Unterschied oder Absinken | 1 |
| Nur mit einem Bein möglich | 2 |
| nicht beurteilbar | 0 |

### Blickwendung

Falls PatientIn starr zur Seite blickt

| Kopf zur anderen Seite drehen | Score |
| --- | --- |
| Bewegt Kopf | 0 |
| Blick starr zur Seite, kein bewegen möglich | 2 |
| nicht beurteilbar | 0 |

Jeder Wert über Null bedeutet **Schlaganfallalarm**{: .info}

4 oder darüber **Schwerer Schlaganfall** (großer Gefäßverschluss) Thrombektomie wahrscheinlich{: .warning}

## Thrombektomie Interventionszentren

+ AKH
+ Barmherzige Brüder
+ Stiftung