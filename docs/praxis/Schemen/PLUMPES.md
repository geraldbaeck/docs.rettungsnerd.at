title:          PLUMPES
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        praxis
md5:            3478f7eca0f5402002bf1720d5dc64ac
date_modified:  2019/06/27
date:           2019/06/27

=Übergabeschema für Spital oder NEF

+ **P**atientendaten
    + Name
    + Alter
    + Auffindesituation
+ **L**eitsymptom
+ **U**ntersuchungsergebnisse
    + Vitalwerte
    + ABCD
+ **M**edikamente
+ **P**atienengeschichte
    + Vorerkrankungen
    + Spitalsaufenthalte
    + Beschwerdegeschichte
    + Allergien
+ **E**rfolgte Maßnahmen
    + Immobilisation
    + Wundversorgung
    + Zugang
    + Medikamentengabe
+ **S**ozialanamnese
    + Schlüssel
    + Entlassungsverhinderungen
    + Kontaktperson
    + Probleme
        + Sprache
        + Aggression
        + Demenz