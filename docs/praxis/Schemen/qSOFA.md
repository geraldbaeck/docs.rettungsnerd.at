title:          qSOFA
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        praxis
md5:            62e89da170dcfb872af3edf0bb41710a
date_modified:  2019/04/01
date:           2019/04/01

Vereinfachte Form des SOFA-Scores. Er kann zur ersten Einschätzung in präklinischen Situationen und Notaufnahmen bei Verdacht auf eine Infektion angewendet werden.

+ Atemfrequenz ≥ 22/min
+ verändertes Bewusstsein (Glasgow Coma Score < 15)
+ systolischer Blutdruck ≤ 100 mmHg

Bei Patienten, die zwei der folgenden drei qSOFA-Kriterien erfüllen, ist präklinisch von einer Sepsis auszugehen.