title:          APGAR
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        praxis
md5:            34bdddc52afbc9d2490f34aeca641abd
date_modified:  2019/05/14
date:           2019/05/10

| Punkte | **A**tmung | **P**uls | **G**rundtonus | **A**ussehen | **R**eflexe |
| --- | --- | --- | --- | --- | --- |
| 0 | keine | 0 | schlaff | blau, blass-grau | keine |
| 1 | unregelmäßig | <100 | träge Bewegung | stamm rosig | Grimassieren |
| 2 | mäßig | >100 | aktive Bewegungen | alles rosig | Husten, Niesen |

Der Score wird 1, 5 und 10 Minuten nach der Geburt überprüft.