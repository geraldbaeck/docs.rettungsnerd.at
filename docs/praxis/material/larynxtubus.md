title:          Larynxtubus
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        praxis
md5:            b4d468e8228dbf7c0908c132dc606a08
date_modified:  2019/01/23
date:           2019/01/23

## Größen

| Größe[^1][^2] | PatientIn | Körpergröße | Farbe |
| --- | --- | --- | --- |
| 0 | Neugeborene | <5 kg | transparent{: .transparent} |
| 1 | Babys | 5–12 kg | weiß{: .white} |
| 2 | Kleinkinder | 12–25 kg | grün{: .green} |
| 2 | Kinder | 125–150 cm | orange{: .orange} |
| 3 | kleine Erwachsene| <155 cm | gelb{: .yellow} |
| 4 | Erwachsene | 155–180 cm | rot{: .red} |
| 5 | große Erwachsene | >180 cm | violett{: .purple} |


[^2]: T. Asai, K. Shingu: [The Laryngeal Tube](http://bja.oxfordjournals.org/cgi/content/full/95/6/729). In: Br J Anaesth. 95 (6), Dez. 2005, S. 729–736.