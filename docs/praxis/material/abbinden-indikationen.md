title:          Abbinden Indikationen
date:           2018/12/22
version:        0.0.2
authors:        gerald@baeck.at
md5:            f13c50d2363f95e1330e7dbfbf699a18
ankikat:        praxis
date_modified:  2019/01/07

+ Amputation oder Teilamputation
+ Oberschenkelarterie verletzt
+ Einklemmung mit starker Blutung
+ ausgedehnte, stark zerfetzte Wunden an den Extremitäten
+ Massenanfall an Verletzten (MANV)