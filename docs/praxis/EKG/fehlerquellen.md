title:          EKG Fehlerquellen
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        ekg
md5:            a3384e826a492fa884f290a3b248ca99
date_modified:  2019/04/20
date:           2019/04/20

+ Wechselstrom
    + führt zu 60Hz Artefakten
    + Quellen: Heizdecken, elektrische Betten, Hochspannungsleitungen...
+ Muskuläre Einflüsse
    + Zittern
    + Bewegung
+ Schwankungen der Nulllinie
    + schlechter Elektrodenkontakt
    + Autofahrt
    + Atmen
    + Husten
    + Sprechen