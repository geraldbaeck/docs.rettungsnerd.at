title:          Ableitungen
version:        0.0.9
authors:        gerald@baeck.at
ankikat:        ekg
md5:            b63a1f9266f9b71c9106dcf1a6f8e8e5
date_modified:  2019/04/20
date:           2019/01/23

## Frontalebene

+ = Extremitätenebene
+ zeichnet alle Kräfte auf die nach rechts, links, superior und inferior wirken
+ kann zur Bestimmung der Herzachse verwendet werden
+ I, II, III nach Einthofen
    + bipolare Ableitungen, jede hat einen negativen und positiven Pol
    + RA negativ, LF positiv, LA in I positiv in III negativ
    + II ist die ideale Ableitung um ein "normales" PQRST zu sehen
+ aVR, aVL, aVF nach Goldberger
    + Gegenpole werden miteinander verschaltet und bilden einen fiktiven Nullpunkt in der Körpermitte
    + die entstehenden Ableitungspole sind alle positiv im Vergleich zum indifferenten Nullpunkt
    + = unipolare Ableitungen
    + die entstehenden EKG Ströme haben eine sehr kleine Amplitude und werden deswegen verstärkt
    + aV steht demnach für augmented Voltage
    + aVR = augmented Voltage Right (Arm)
    + aVL = augmented Voltage Left (Arm)
    + aVF = augmented Voltage Foot

![Ableitungen der Frontalebene](https://upload.wikimedia.org/wikipedia/commons/1/19/Limb_leads_of_EKG.png)
Ableitungen der Frontalebene[^1]
{. imagewithcaption}

## Horizontalebene

+ zeichnet alle Kräfte auf, die nach rechts, links, vorne (anterior) und hinten (posterior) wirken
+ V<sub>1</sub>, V<sub>2</sub>, V<sub>3</sub>, V<sub>4</sub>, V<sub>5</sub>, V<sub>6</sub> nach Wilson
    + ebenfalls unipolar zur Körpermitte
    + V<sub>5</sub> ist die ideale Ableitung um ein "normales" PQRST zu sehen