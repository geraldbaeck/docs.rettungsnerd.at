title:          Das normale EKG
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        ekg
md5:            d96ed48da893df076f83924c15ead606
date_modified:  2019/06/27
date:           2019/01/23

![Sinus Rhythmus](https://upload.wikimedia.org/wikipedia/commons/f/ff/SinusRhythm_withoutLabels.svg)
Normaler Sinusrhythmus des menschlichen Herzens[^1]
{: .imagewithcaption}

## P-Welle

+ Vorhoferregung
+ Sinusknoten sitz rechts und erregt zuerst die rechte Kammer und verzögert links über das Bachmann-Bündel
+ dh. erste Teil der P-Welle = rechter Vorhof, zweiter Teil = linker
+ am stärksten ausgeprägt in II und V&#8322;

## QRS-Komplex

+ Kammererregung
+ Q = erster negativer Ausschlag nach P
+ R-Zacke = erster positiver Ausschlag nach P
+ S-Zacke = negativer Ausschlag nach R

### RS-Komplex

+ folgt auf eine P-Welle eine positive Zacke muss es sich um ein R handeln
+ folgt auf das R eine negative Zacke => RS-Komplex

### QS-Komplex

+ folgt auf eine P-Welle nur ein negativer Ausschlag => QS-Komplex

### geknoteter QRS-Komplex

+ lassen sich Auf- und Abwärtsbewegungen nur schwer trennen => geknotet
+ kann man dagegen zwei positive Ausschläge erkennen, wird der zweite als R´ oder r´ bezeichnet
+ der größer der beiden R-Zacken erhält dabei den Großbuchstaben, genauso bei mehrere S-Zacken

## T-Welle

+ spiegelt die Repolarisierung der Kammern wieder, keine Herzmuskelaktivität
+ manchmal findet sich zwischen zwei QRS-Komplexen nur eine Welle, meist T-Welle , mehrere Ableitungen prüfen
+ manche Personen haben nach der T- noch eine kleiner U-Welle

## Zeiten

| EKG | Dauer | Vorschub (25 mm/s aka 5 Kasteln/s) |
| --- | --- | --- |
| P-Welle | bis 0,1 s | 2,5 mm |
| PQ-Zeit | 0,12 - 0,21 s | 3 - 5 mm |
| QRS-Komplex | =< 0,12 s | 3 mm |
| QT-Zeit | 0,3 - 0.45 s | 10mm |