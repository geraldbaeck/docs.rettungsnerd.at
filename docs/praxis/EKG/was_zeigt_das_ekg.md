title:          Was ist das EKG?
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        ekg
md5:            e889c41d677b0954af600ebcae428f88
date_modified:  2019/05/08
date:           2019/01/23

## Definition

+ zeigt die elektrische Spannung, die von einem Herzmuskel zu einem bestimmten Zeitpunkt erzeugt wird
+ das EKG macht am Herzen fließende Ströme sichtbar
+ daraus ergibt sich eine (Herz-)Spannungskurve

## Physiolgische Grundlage

+ Jeder Kontraktion des Herzmuskels geht eine elektrische Erregung voraus
+ die Erregung beginnt im Normalfall beim Sinusknoten
+ verläuft über das Reizleitungssystem
+ in die übrigen Herzmuskelzellen (Myokard)
+ **diese Spannungsänderungen werden gemessen und im Zeitverlauf aufzeichnet = EKG**

| | besteht aus | elektrische Leitgeschwindigkeit |
| --- | --- | --- |
| Myokard | Vorhöfe, Kammern | 1m/s |
| Reizbildungs- und Reizleitungsgewebe | Sinusknoten, Internodalbündel, AV-Knoten, His-Bündel, Tawara-Schenkel, Purkinje-Fasern | 2-4m/s |

+ Werte:
    + HF
    + Herzrhythmus
    + Lagetyp
    + elektr. Aktivität von Vorhof & Kammer
+ Diagnostik von
    + Rhythmusstörungen
    + Extrasystolen
    + Reizleitungsunterbrechungen (Blöcken)
    + Ischämiezeichen

CAVE: Das EKG zeigt nur die elektrische Aktivität des Herzmuskels an, nicht jedoch die tatsächliche Auswurfleistung.
{: .warning}