title:          Herzschrittmacher
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        EKG
md5:            11b61f59ae43131e2b79fce1b9b0cf18
date_modified:  2019/06/27
date:           2019/06/18

+ dient der Behandlung von Patienten mit
    + zu langsamen Herzschlägen (Bradykardie)
    + Reizleitungsstörungen
+ stimuliert regelmäßig den Herzmuskel mit Hilfe von elektrischen Impulsen und regt diesen so zur Kontraktion an.

## Funktionen

Schrittmacherstimulationsmodi werden in international einheitliche Buchstaben kodiert.

| 1. Stimulationsort | 2. Registrierungsort | 3. Betriebsart | 4. Frequenzadaptation | 5. Multifokale Stimulation |
| --- | --- | --- | --- | --- |
| A (Atrium) | A (Atrium) | T (getriggert) | R (adaptiv) | A (Atrium) |
| V (Ventrikel) | V (Ventrikel) | I (inhibiert) |  | V (Ventrikel) |
| D (Dual A+V) | D (Dual A+V) | D (Dual T+I) |  | D (Dual A+V) |

Durch den Spike, (Simulationsimpuls) im EKG als Zacke sichtbar.