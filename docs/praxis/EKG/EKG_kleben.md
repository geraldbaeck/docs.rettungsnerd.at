title:          EKG kleben
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        ekg
md5:            37a203bd997f79dc6e055878429083eb
date_modified:  2019/01/23
date:           2019/01/23

## Extremitätenableitungen
+ möglichst knöchern kleben
+ Rot/Gelb/Grün/Schwarz (Ampel von recht oben beginnend im Uhrzeigersinn)

## Brustwandableitungen
| Ableitung | Ableitungspunkt |
| --- | --- |
| V<sub>1</sub> | 4. ICR rechts parasternal |
| V<sub>2</sub> | 4. ICR links parasternal |
| V<sub>3</sub> | zwischen V<sub>2</sub> & V<sub>4</sub> |
| V<sub>4</sub> | 5. ICR MCL links |
| V<sub>5</sub> | 5. ICR vordere Axiliarlinie |
| V<sub>6</sub> | 5. ICR hintere Axiliarlinie |

![Brustwandableitungen nach Wilson](https://upload.wikimedia.org/wikipedia/commons/6/66/Precordial_Leads_2.svg)
Brustwandableitungen nach Wilson[^1]
{: .imagewithcaption}

![EKG-Ableitung unter Rettungsdienst-Bedingungen](https://upload.wikimedia.org/wikipedia/commons/4/4a/EKG-Ableitung_unter_Rettungsdienst-Bedingungen.png)
EKG-Ableitung unter Rettungsdienst-Bedingungen[^2]
{: .imagewithcaption}