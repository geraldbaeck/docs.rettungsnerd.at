title:          EKG Befundung
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        ekg
md5:            9aec24733651e2b8d7491b9062060d55
date_modified:  2019/04/21
date:           2019/04/20

1. elektr. Aktivität vorhanden?
2. Rhythmus regelmäßig oder unregelmäßig?
3. Frequenz?
4. P-Wellen vorhanden
5. P-Welle Beziehung zum QRS-Komplex? (PQ-Zeit max 0,2sek)
6. QRS- Komplex breite u. höhe (normal kl. 0.1sek) 2kl. Kästchen
7. ST-Strecke Vorhanden? Normal, Hebung, Senkung