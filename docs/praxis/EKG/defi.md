title:          Defi Funktionen
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        EKG
md5:            f22b78fa6b46026c6db6d0958abe252f
date_modified:  2019/06/27
date:           2019/06/18

## Defibrillation

+ ausschließlich im Rahmen der Reanimation
    + bei Kammerflimmern
    + PVT

## Kardioversion

+ zur Wiederherstellung des normalen Sinusrhythmus bei Herzrhythmusstörungen
    + Vorhofflimmern
    + SVT
    + VT
    + Vorhofflattern

+ Kardioversion mit Defi
    +Schock mit geringer Initialdosis (50-100 Joule)
    + EKG getriggert ausgelöst (es wird in der R-Zacke hineingeschockt)
+ Kardioversion mit Medikamenten
    + Adenosin (6ml-12ml-12ml)
    + Renimationsbereitschaft