title:          Wissendatenbank NFS
date:           2019/01/06
version:        0.0.6
authors:        gerald@baeck.at
md5:            a410f7662717c0b16ae68cc47e09c263
date_modified:  2019/06/30

## Über diese Seite

Meine persönlichen Unterlagen für die Tätigkeit als Notfallsanitäter in Wien.

Der Aufbau des Skripts ist oft stichwortartig und mit vielen Bulletpoints versehen, weil das mein Hirn anscheinend so besser verarbeiten kann. Deswegen eigenen sich diese Unterlagen nicht zum erstmaligem Kennenlernen der Materie, sondern vielmehr zur Wiederholung und Auffrischung.

## Lizenz

[CC0 1.0 Universell (CC0 1.0)](https://creativecommons.org/publicdomain/zero/1.0/deed.de) Public Domain Dedication

## Haftungsausschluss

Es wird keine Haftung für die Richtigkeit des Inhalts übernommen.