title:          Hubschraubereinsätze
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Rettungswesen
md5:            155bbdb16dcccda3081147ee72b0026e
date_modified:  2019/05/14
date:           2019/04/01

+ Einsatzzeit: von 6:00 Uhr (im Winter später) bis Sonnenuntergang.
+ Landung immer gegen den Wind, EinweiserIn wird per Funk verständigt
+ EinweiserIn stellt sich in ca. 5 m Entfernung vom Aufsetzplatz mit dem Wind im Rücken hin bis zum Aufsetzen
+ Blickkontakt zum Piloten halten, um Anweisungen befolgen zu können.
+ Gestreckte Hände = „YES“
+ nur von der Vorderseite nach Aufforderung nähern.
+ Übergabe des Patienten auf Vakuummatratze.