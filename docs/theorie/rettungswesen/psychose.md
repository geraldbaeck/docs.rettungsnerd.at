title:          Psychose
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Rettungswesen
md5:            93252f2bc1b8859f9ab3e54def4f2e2e
date_modified:  2019/07/03
date:           2019/04/01

+ = Überbegriff für verschiedene schwerwiegende psychische Störungen, bei denen Betroffene den Bezug zur Realität verlieren.
+ immer verbunden mit einer Selbst- oder Fremdgefährdung

+ Eigenschutz
+ Polizei / Amtsarzt
+ Respekt vor der erkrankten Person
+ Entschärfen der Situation
+ Beruhigen, Hilfe anbieten