title:          Nadelstichverletzung
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Rettungswesen
md5:            2d134e377475c1217e83afdf24a28f86
date_modified:  2019/06/12
date:           2019/04/01

## Maßnahmen

+ 3-4min Ausbluten/Ausdrücken der Wunde
+ mind. 30sec spülen (Handdesinfekt oder Wasser)
+ danach mit virusinaktivierendem Antiseptikum auf Mulltupfer benetzen.
+ Mull fixieren und immer wieder benetzen mind. 10 min
+ Kontaktaufnahme mit Journalarzt
+ Vorstellung bei Notfallstation (Verletzter und Infektionsträger)
+ Dokumentieren
+ Meldung bei AUVA und Dienstgeber

+ **Postexpositionelle HIV-Prophylaxe (PEP)**: innerhalb von 1-2 Std, aber innerhalb von 48-72 Std jedenfalls beginnen
+ **Postexpositionelle Meningitis-Prophylaxe**: Antibiotika

## Unfallverhütung

+ Kanülen nie in Hülle zurückstecken
+ Kanülen und geöffnete Glasampullen sofort nach Benutzung persönlich in Sammelbehälter entsorgen
+ nie in Sammelbehälter greifen
+ Sammelbehälter nur ca. zwei Drittel füllen, nicht umfüllen oder ausleeren

## Nadelstichverordnung (NastV)

+ Gefahren sind in die Arbeitsplatzevaluierung aufzunehmen.
+ Instrumente mit integrierten Sicherheits- und Schutzmechanismen sind bereitzustellen und zu verwenden
+ Recapping-Verbot
+ Entsorgung in geeignete Abwurfbehälter
+ Mitarbeiter sind zu unterweisen.
+ Informations- und Maßnahmenblatt für Nadelstichverletzungen ist aufzulegen.

[AUVA Infofolder](https://www.auva.at/cdscontent/load?contentid=10008.601435&version=1407917705)