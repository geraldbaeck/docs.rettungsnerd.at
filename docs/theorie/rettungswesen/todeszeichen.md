title:          Todesfeststellung
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Rettungswesen
md5:            d733f08ec34d1e2ae38a25d17573b132
date_modified:  2019/06/27
date:           2019/04/01

+ Mit dem Leben nicht vereinbare Verletzungen (Kopfabtrennungen, totale Deformation)
+ Totenflecken
+ Totenstarre: 1-2h Stunden, löst sich nach 2-3d
+ Verwesungserscheinungen
    + Leichengeruch
    + Fäulnis
    + aufgetriebener Bauch
    + Ausfluss von übelriechender Flüssigkeit aus dem Körper

## Exekutive verständigen bei

+ an öffentlichen Plätzen
+ Suizid
+ Fremdverschulden
+ Minderjährigen
+ Öffnungen der Wohnung
+ Unfall
+ Identität unbekannt
+ Ausländen Staatsbürgern