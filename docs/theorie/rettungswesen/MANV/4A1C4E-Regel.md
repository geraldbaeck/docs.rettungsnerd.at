title:          4A1C4E
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Rettungswesen
md5:            f85ba36ee133753608b4d07cae929991
date_modified:  2019/07/08
date:           2019/04/01

## Definition

+ Schema zur Beschreibung der wichtigsten Gefahrenschwerpunkte einer Einsatzstelle
+ durch systematisches Durchgehen wird sichergestellt, dass keine Gefahr übersehen wurde

## Schema

+ **4A**
    + **A**temgift
    + **A**ngstreaktion
    + **A**usbreitung
    + **A**tomare Gefahr
+ **1C**
    + **C**hemische Stoffe
+ **4E**
    + **E**xplosion
    + **E**insturz
    + **E**lektrizität
    + **E**rkrankung