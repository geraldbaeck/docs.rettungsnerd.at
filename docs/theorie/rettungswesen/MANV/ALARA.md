title:          ALARA Prinzip
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        nice2know
md5:            c61ba7226d53fa6d8af8207008ae95ba
date_modified:  2019/07/08
date:           2019/04/01

## Definition

+ Prinzip des Strahlenschutzes
+ fordert eine so niedrige Strahlenbelastung von Menschen, Tieren und Material (auch unterhalb von Grenzwerten)
+ wie sie unter Einbeziehung praktischer Vernunft und Abwägung von Vor- und Nachteilen machbar erscheint

## Schema

+ **A**s
+ **L**ow
+ **A**s
+ **R**easonable
+ **A**chieveable