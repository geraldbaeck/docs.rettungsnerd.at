title:          Gefährliche Stoffe
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Rettungswesen
md5:            af9a59fab1c0e05e3bc3b3baaff7dfab
date_modified:  2019/06/02
date:           2019/04/01

Zur Identifikation eines gefährlichen Stoffes gibt es folgende Möglichkeiten:

+ Kennzeichnung
    + Gefahrzettel (Rautentafel nach UN-Klassen)
    + Gefahrtafel
        + Gefahrnummer
        + UN-Nummer
    + Am Wasser: blaue Kegel
        + 1: entzündlich
        + 2: gesundheitsschädlich
        + 3: explosiv
    + Güterzug
        + oranger Strich in der Mitte
+ Sicherheitsdatenblatt
+ Beförderungspapier
+ eigene Wahrnehmung

Zum Abschätzen der ausgetretenen Menge ist es hilfreich zu wissen, aus welchem Behälter der Stoff ausgetreten ist:

| Behälter | Menge |
| --- | --- |
| Flasche | 0,1 - 5L |
| Kanister | 5 - 20L |
| Fass | 20 - 200L |
| [Intermediate Bulk Container (IBC)](https://de.wikipedia.org/wiki/Intermediate_Bulk_Container) | 500 - 3000L |
| Tank-LKW, Kesselwagen | ~10.000L |