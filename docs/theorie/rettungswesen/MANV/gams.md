title:          GAMS
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Rettungswesen
md5:            5815b8569f5c97c7f7bb83c1ac97834b
date_modified:  2019/06/27
date:           2019/04/01

=beschreibt das richtige Verhalten bei Gefahr

+ **G**efahr erkennen
+ **A**bstand halten, falls keine ausreichende Absicherung möglich ist
+ **M**enschenrettung durchführen (falls gefahrlos möglich)
+ **S**pezialkräfte anfordern und diese vorab informieren