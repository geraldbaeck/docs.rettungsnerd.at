title:          3A Regel
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Rettungswesen
md5:            405153e2ac999c66cf551414e0db7c53
date_modified:  2019/06/27
date:           2019/04/01

=beschreibt das richtige Verhalten bei Gefahrengutsituatioen

+ **A**bstand - so groß wie möglich
+ **A**ufenthaltszeit - so kurz wie möglich
+ **A**bschirmung - so gut wie möglich