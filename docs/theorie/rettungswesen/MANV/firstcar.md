title:          First Car
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Rettungswesen
md5:            4bbf495c46fcee68cbff247f9ce65135
date_modified:  2019/06/27
date:           2019/04/01

## GAms

+ Gefahr erkennen und absichern, Eigenschutz beachten

## Sichtmeldung

+ alles was vom Auto zu sehen ist, insbesondere:
+ Unfallhergang/-art
+ Ausmaß
+ Sind Verletzte zu sehen
+ Wer ist schon vor Ort?

## Lageerkundung und Beurteilung

+ anschließend aussteigen, Überblick verschaffen
+ mit anderen Organisationen vor Ort Kontakt aufnehmen

## Lagebericht

+ Schadenslage
    + Gefahren
    + Details zum Ereignis
    + Zahl der Verletzten/Betroffenen
+ Eigene Lage
    + Spezialkräfte/Rettungshinweise
+ Allgemeine Lage
    + Wetter/Gelände
    + Infrastruktur
    + Zufahrtswege

## Aktion

+ Grenze der Gefahrenzone gemeinsam mit Polizei/FW festlegen
+ Räume festlegen
+ Bildung der Patientenablage (PLS ausgeben)
+ Einsatz an eintreffenden EL übergeben