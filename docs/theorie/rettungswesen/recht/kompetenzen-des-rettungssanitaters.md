title:          RettungssanitäterInnen Kompetenzen
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Rettungswesen
md5:            24c18d184229f04f83ad387e201f5fa3
date_modified:  2019/06/27
date:           2019/06/27

Gesetzliche Grundlage: Sanitätergesetz (SanG)

+ **selbstständige, eigenverantwortliche Versorgung** Hilfsbedürftiger, die medizinischer Betreuung bedürfen insbesondere
  + "Blutabnahme aus der Kapillare" = Blutzuckermessung
+ **Übernahme und Übergabe** eines Patienten izH mit einem Transport
+ **Hilfestellung bei Akutsituationen** einschließlich Sauerstoffgabe
+ qualifizierte Durchführung von **lebensrettenden Sofortmaßnahmen** insbesondere:
  + Beurteilung, Wiederherstellung bzw Aufrechterhaltung der lebenswichtigen Funktionen
  + Defibrillation mit halbautom. Geräten
  + Herstellung und Durchführung des transports
+ sanitätsdienstliche Durchführung von **Sondertransporten**