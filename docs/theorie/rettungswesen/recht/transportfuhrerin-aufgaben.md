title:          TransportführerIn Aufgaben
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Rettungswesen
md5:            e40af6fe5ac31d5b7a9d7bd26cf249ff
date_modified:  2019/06/27
date:           2019/06/27

+ Versorgung
+ Dokumentation
+ sicherer Transport
+ weitere Tätigkeiten während des Einsatzes
+ dass medizinisches Material und Geräte vorhanden sind