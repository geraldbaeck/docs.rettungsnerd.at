title:          Vertretungsbefugnis nächster Angehöriger
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Rettungswesen
md5:            60e3dfb3b49c6a1a15e31176d3e95f07
date_modified:  2019/07/03
date:           2019/06/27

+ volljährige Person kann Rechtsgeschäfte des täglichen Lebens nicht mehr besorgen kann und 
+ hat keine SachwalterIn bestellt 
+ => nächste Angehörige kann die Vertretung der Person für das jeweilige Rechtsgeschäft übernehmen

Als nächste Angehörige gelten:

+ Ehegattin/Ehegatte (im gemeinsamen Haushalt lebend)
+ Eingetragene Partnerin/eingetragener Partner (im gemeinsamen Haushalt lebend)
+ Lebensgefährtin/Lebensgefährte (mindestens drei Jahre mit der Betroffenen/dem Betroffenen im gemeinsamen Haushalt lebend)
+ Volljährige Kinder
+ Eltern