title:          PatientInnenverfügung
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Rettungswesen
md5:            19572ed11423c66eb5ed0ec75f426668
date_modified:  2019/06/27
date:           2019/06/27

=Erklärung mit der ein Patient eine medizinische Behandlung ablehnt. Sie soll wirsam werden wenn der Patient zum Behandlungszeitpunkt nicht einsichts-, urteils- oder äußerungsfähig ist. Es kann nur auf eine Behandlung verzichtet aber keine Angeordnet werden.

## Verbindliche PatientInnenverfügung

+ Schriftlichkeitsgebot mit korrekter Umschreibung der abzulehnenden Maßnahme
+ Ärztliche Aufklärung (Bestätigung der Urteilsfähigkeit, nachvollziehbare Folgenabschätzung)
+ Errichtung vor einem Notar/RA/Patientenanwalt inkl Rechtsbelehrung
+ Geltungsdauer 5 Jahre

## Beachtliche PatientInnenverfügung

+ keine Formvorschriften
+ nicht verbindlich
+ bei der Ermittlung des mutmaßlichen Patientenwillens zu berücksichtigen

## §12 Patientenverfügungsgesetz

Nimmt die medizinische Notfallversorgung aus, sofern die Suche nach einer Patientenverfügung das Leben und die Gesundheit des Patiententen ernstlich gefährdet.

Bei der Übergabe muss auf das mögliche Vorliegen einer Verfügung aufmerksam gemacht werden. => Patientenverfügungsregister