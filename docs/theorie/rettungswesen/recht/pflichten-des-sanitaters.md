title:          SanitäterInnen Pflichten
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Rettungswesen
md5:            54c7d11e36fac5d69aa8bada9d61db54
date_modified:  2019/06/27
date:           2019/06/27

+ Sorgfaltspflicht
    + Versorgung ohne Ansehen der Person
    + Versorgung nach bestem Wissen und Gewissen => Fortbildungspflicht, Notartznachforderung
+ Dokumentationspflicht
    + Anamnese
    + Auffindesituation & Rahmenbedingungen
    + anwesende Kräfte
    + Messwerte
    + Art des Transportes
+ Verschwiegenheitspflicht
    + bekannt gewordenen Umstände sind geheim zu halten, insbesondere:
    + medizinische Daten und
    + Einsatzeindrücke
+ Auskunftspflicht
+ Hilfeleistungspflicht
    + gilt in jeder Lebenslage
    + => nach §95 Stgb Zumutbarkeitsschwelle beachten
    + Bei Einsatz gilt nicht die Hilfeleistungspflicht, sondern engeres Rechtsverhältnis nach SanG
+ Fortbildungspflicht