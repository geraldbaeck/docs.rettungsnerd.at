title:          Vorsorgevollmacht
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Rettungswesen
md5:            79e2940b754402aa76ad09bc0dba0b67
date_modified:  2019/06/27
date:           2019/06/27

Mit einer Vorsorgevollmacht kann eine Person schon vor dem Verlust

+ der Geschäftsfähigkeit
+ der Einsichts- und Urteilsfähigkeit oder
+ der Äußerungsfähigkeit selbst bestimmen,

wer als **Bevollmächtigte/Bevollmächtigter für sie entscheiden und sie vertreten kann**.

Sinnvoll bei:

+ Krankheiten, die mit fortschreitender Entwicklung das Entscheidungsvermögen beeinträchtigen kann
+ zb Alzheimer oder Altersdemenz leiden.
+ aber auch mögliche Einschränkungen nach einem Unfall

Die betroffene Person kann festlegen,

+ für welche Angelegenheiten die Bevollmächtigte/der Bevollmächtigte zuständig werden soll
+ auch möglich, mehrere Personen zu bevollmächtigen, die unterschiedliche Aufgaben übernehmen

So kann sich beispielsweise eine Vertrauensperson um die Bankgeschäfte kümmern, eine andere aber die Bezahlung der Miete übernehmen.

Voraussetzungen:

+ Geschäfts-, Einsichts- und Urteilsfähigkeit
+ Bevollmächtigte darf in keinem Abhängigkeitsverhältnis stehen
+ Widerruf jederzeit möglich, kann befristet werden
+ kann im Österreichischen Zentralen Vertretungsverzeichnis (ÖZVV) registriert werden