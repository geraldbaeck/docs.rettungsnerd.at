title:          Medizinproduktgesetz
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Rettungswesen
md5:            c250920716caf1d0b517a8018c0f20d2
date_modified:  2019/06/27
date:           2019/06/27

regelt die technischen, medizinischen und Informations-Anforderungen für das Inverkehrbringen von Medizinprodukten

+ Nur eingeschultes Personal darf MP verwenden
+ Vor Anwendung muss Funktionstauglichkeit, Betriebssicherheit und ordnungsgemäßer Zustand geprüft werden
+ Herstellerangaben sind bindend außer Chefarzt entscheidet anders
+ Eigenmächtiges Reparieren nicht ok