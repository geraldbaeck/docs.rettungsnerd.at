title:          PatientInnenrechte
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Rettungswesen
md5:            e1a8abcf7acb2c4001212a90813c02c7
date_modified:  2019/06/27
date:           2019/06/27

+ Pflichten des SAN sind im Umkehrschluss Rechte der PatientInnen, insbesondere  
    + Informations- und Aufklärungsrecht
    + Selbstbestimmungsrecht
    + Recht zur sachgemäßen Versorgung
    + Recht auf Dokumentationseinsicht
    + Recht auf Datenschutz/Verschwiegenheit

+ Es ist eine informierte Einwilligung einzuholen:
    + Aufklärung muss die Notwendigkeit, Wirkung und die möglichen Komplikationen beinhalten

+ Ab 14 darf jeder Patient selbst über Behandlung entscheiden, außer besonders schützendwerte Personengruppen, wie
    + psychisch Erkrankte
    + geistig Behinderte
    + Besachwalterte