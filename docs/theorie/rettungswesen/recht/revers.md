title:          Revers
version:        0.0.5
authors:        gerald@baeck.at
ankikat:        Rettungswesen
md5:            190fd0d2134c195df422f4549f20fab5
date_modified:  2019/06/27
date:           2019/04/01

## Definition

+ = allgemein ein „Verpflichtungsschein“
+ kommt gesetzlich nicht vor

In der Medizin wird der Begriff Revers im Rahmen der dauerhaften oder zeitweiligen Verweigerung einer medizinischen Behandlung verwendet.

## Voraussetzungen

+ &gt;= 14a
+ Patient einsichts- und urteilsfähig
+ Aufklärung über die möglichen gesundheitlichen Folgen
    + nachdrücklich
    + in verständlicher Form

## Inhalt

Der Revers ist eine schriftlich unterzeichnete Dokumentationsform mit der ein Patient

+ ablehnt:
    + Versorgung
    + Transport
+ bestätigt:
    + ordnungsgemäße Aufklärung als Voraussetzung für die Willensbildung
    + über die möglichen Nachteile durch die Verweigerung
    + Hinweiß zur Eigenverantwortung

## Zweck

+ rechtliche Absicherung für den Rettungsdienst
+ Möglichkeit für den Patient, seine Weigerung zu überdenken