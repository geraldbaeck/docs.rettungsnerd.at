title:          Atemmechanik
desc:           Funktionsweise der Atmung
date:           2019/01/06
version:        0.0.7
authors:        gerald@baeck.at
ankikat:        Anatomie
md5:            b6ddb323e3225814be27371fe065b9e3
date_modified:  2019/06/02

## Bauchatmung / Zwerchfellatmung

+ Zwerchfell kontrahiert => Einatmen
+ Zwerchfell entspannt => Ausatmen

## Brustatmung

+ Brustkorb wird durch Zwischenrippenmuskeln nach oben gezogen

## Atmungsantrieb

+ Zentraler Antrieb im Gehirn in der Medulla oblongata (unterester Teil des Gehirns) = zentraler Rhythmus Generator
    + inspiratorische Neuronen
    + expiratorische Neuronen (nur beim willentlichen Ausatmen)
+ Reflektorische Kontrollen:
    + Dehnungsrezeptoren messen die Aufblähung der Lunge
    + Dehnung groß genug => [Herning-Breuer-Reflex](https://de.wikipedia.org/wiki/Hering-Breuer-Reflex)
+ Chemische Kontrolle:
    + Chemorezeptoren messen den Liquor-pH-Wert, der durch einen Anstieg der Kohlenstoffdioxidkonzentration im Blut sinkt
    + Chemorezeptoren messen CO&#8322;, O&#8322; und pH (Säure)
    + Zentrale Sensoren im Atemzentrum im Gehirn in der medulla oblongata
    + Periphere Sensoren in der Aorta ([Glomera aortica](https://de.wikipedia.org/wiki/Glomera_aortica)) und in der Kopfschlagader ([Glomus Caroticum](https://de.wikipedia.org/wiki/Glomus_caroticum))
    + Am wichtigsten ist die CO&#8322; Antwort dann die pH Antwort und zb bei COPD ist die O&#8322; Antwort
+ Sonstige Antriebe:
    + Anstrengungen
    + Schmerzen
    + Schwangerschaft
    + Fieber
    + Adrenalin
    + Wille

[Atemsystem erklärt 2](https://www.youtube.com/watch?v=CorOqCOkTu8){: .youtube}