title:          Atmungsformen
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Anatomie
md5:            49a3948e132eee5313f844dbd8be839e
date_modified:  2019/03/15
date:           2019/03/15

Atmungsformen werden hinsichtlich ihres Zustands unterschieden. Normale, ruhige Atmungsformen werden als Eupnoe bezeichnet.

## Normale, ruhige Atmungsformen

+ Flankenatmung
+ Brustatmung
+ Bauchatmung

## Pathologische Atmungstätigkeiten

Bei den krankhaften Atmungstätigkeiten wird unterschieden zwischen:

+ **Dyspnoe (Atemnot)** subjektiv erschwerte Atmung, zB durch eine Verlegung der Atemwege, z.B. beim Schnarchen
+ **Hyperpnoe** verstärkte und vertiefte Atmung, die z. B. bei Fieber
+ **Apnoe (Atemstillstand, Atemlähmung)** medizinische Fachbegriff für Atemstillstand, zB Kennzeichen des Schlafapnoe-Syndroms

Pathologische Atmungsformen sind Störungen des Atemantriebs, die für bestimmte Erkrankungen oder Gehirnschädigungen charakteristisch sind.

+ **Kußmaul-Atmung** ist eine vertiefte Atmung mit normaler oder erhöhter Frequenz, verursacht durch metabolische Azidose
insbesondere beim entgleisten Diabetes mellitus.
+ **Seufzer-Atmung** (Schlafapnoe-Syndrom, Pickwick-Syndrom) folgt auf einen tiefen Atemzug eine Reihe von immer flacheren Atemzügen, dann eine Pause.
+ **Cheyne-Stokes-Atmung** ist durch abwechselnd flache und tiefe Atmung mit periodischem Atemstillstandsphasen gekennzeichnet. Sie entsteht bei Schäden des Atemzentrums im Gehirn und bei schwerer Herzinsuffizienz.
+ **Biot-Atmung** wird ebenfalls durch regelmäßige Pausen unterbrochen, die Atemtiefe ist jedoch gleichmäßig („Maschinenatmung“). Sie ist ebenfalls für schwere Schäden am Atemzentrum typisch.
+ **Schnappatmung** als präfinale Atmung bei längerandauernder Hypoxiezeit