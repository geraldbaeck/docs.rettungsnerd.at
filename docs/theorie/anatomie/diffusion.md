title:          Diffusion
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Anatomie
md5:            7d7d662a64252362b67a9c8af38f7cdc
date_modified:  2019/06/27
date:           2019/04/20

## Definition

+ =physikalischer Prozess, der zum Ausgleich von Konzentrationunterschieden führt
+ wird durch die zufällige Eigenbewegung der Stoffteilchen (Brown'sche Molekularbewegung) verursacht
+ => gleichmäßigen Verteilung und Durchmischung von Stoffen
+ => Erhöhung der Entropie (Unordnung)

## Faktoren

+ große Austauschfläche
+ kurzer Diffusionsweg
+ lange Kontaktzeit
+ großer Konzentrationsunterschied

Beispiel: Gasaustausch zw. Alveolen und Kapillaren (02-C02)