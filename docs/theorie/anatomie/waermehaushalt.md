title:          Wärmehaushalt
date:           2019/01/06
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Anatomie
md5:            4df81a1372fec5835d432c73bddbb5f1
date_modified:  2019/01/07

## Definition  
+ sehr stabil
+ dient dem geordneten Ablauf aller Lebensvorgänge
+ Damit chemische Prozesse im Körper reibungslos funktionieren können, ist konstante Körpertemperatur von 36-37°C erforderlich
+ Wärmeproduktion
    + Muskelaktivität
    + Verbrennung
+ Wärmeabgabe
    + Erweiterung der Blutgefäße => schnellere Criculation => Konvektion
    + Schweißabsonderung => Verdunstungskälte

## Komplikationen  
+ Sonnenstich
+ Hitzekollaps
+ Fieber(-krampf)
+ Hypothermie

Störungen durch Giftstoffe von Krankheitserregern oder  körpereigene Giftstoffe können Fehlregulation und dadurch zu Fieber führen. Beeinträchtigt Stoffwechsel, Wasser- und Elektrolythauhalt und somit auch Atmung, Kreislauf und unter Umständen das Bewusstsein.

Extreme Wärme oder Kälte können Wärmehaushalt sehr beeinträchtigen und zu Störungen des Stoffwechsels, des Wasser- und Elektrolythaushaltes, des Säure-Basen-Haushaltes, des Bewusstseins, der Atmung und des Kreislaufs führen.

[Dr. Max erklärt die Welt: Das Klimasystem des menschlichen Körpers](https://www.youtube.com/watch?v=jTRP4ZTtRrg&t=94){: .youtube}