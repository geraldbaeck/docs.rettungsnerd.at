title:          Skelett
date:           2019/01/06
version:        0.0.8
authors:        gerald@baeck.at
ankikat:        Anatomie
md5:            7f2d405bd0545e42b7011e30010f7630
date_modified:  2019/06/09

## Knochen

### Funktion

+ **Schutzfunktion** für Organe (Lunge Herz, Gehirn)
+ **Speicherfunktion** für Calcium und Phosphat
+ **Blutproduktion**: Knochenmark ist Bildungsstätte von roten und zum Teil weißen Blutkörperchen
+ **passiver Bewegungsapparat**
  + bilden das Skelett und verleihen Körper Halt und Form
  + dienen den Muskeln als Ansatz => Bewegungen

### Arten

+ **Röhrenknochen** (Ossa longa): an Extremitäten zu finden
+ **Platte Knochen** (Ossa plana): Schädel oder Becken
+ **kurze Knochen** (Ossa brevia): Handwurzelknochen
+ **luftgefüllte Knochen** (Ossa pneumatica): mit Schleimhaut gefüllte Hohlräume, zb Stirnbein
+ **unregelmäßige Knochen** (Ossa irregularia): Wirbel, Unterkieferknochen
+ **Sesambeine**: Knochen in Sehne eingelagert, zb Kniescheibe, Erbsenbein

### Aufbau

+ von Knochenhaut (Periost) umhüllt, die von Blutgefäßen und Nerven durchzogen ist
+ Kompakta
+ Spongiosa (Schwammknochen)
+ Knochenmark
    + ab dem Ende des vierten Embryonalmonats das wichtigste blutbildende Organ
    + füllt die Hohlräume der Knochen
    + Rückenmark ist kein Knochenmark
    + bildet fast alle Blutzellarten des Menschen
    + rot (Blutbildung) in den platten und würfelförmigen Knochen
    + gelb (Fett) in den großen Röhrenknochen
+ Wachstumsfuge
+ Gelenk oder Naht als Verbindung zwischen Knochen

### Gelenke

+ **Knochen sind durch Gelenke miteinander verbunden** und dadurch beweglich.
+ Gelenk wird **durch Gelenkskapsel und Bänder zusammengehalten**
+ Menisci (Beilagscheiben)
+ Schleimbeutel (Polster)

## Schädel

+ Schutzkapsel fürs Gehirn & Sinnesorgane
+ Grundlage fürs Gesicht
+ Beginn des Atmungs- und Verdauungstraktes

## Skelett Aufbau

+ Hirnschädel
  + Schädeldach
    + 2 Schläfenbeine
    + 2 Scheitelbeine
    + Stirnbein
    + Hinterhauptbein
  + Schädelbasis
    + Boden der Schädelhöhle, Gehirn liegt auf
    + Hinterhauptsloch, Übergang ins Rückenmark
+ Gesichtsschädel
  + Nasenbein
  + Oberkiefer
  + Jochbein
  + Unterkiefer

![Schädel](https://upload.wikimedia.org/wikipedia/commons/a/a9/Menschlicher_Sch%C3%A4del_Knochen_seitlich.svg)
Aufbau des knöchernen Schädels[^1]
{: .imagewithcaption}

[^1]: Quelle: [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Menschlicher_Sch%C3%A4del_Knochen_seitlich.svg), [CC0-1.0](https://creativecommons.org/licenses/by-sa/2.0/at/deed.de) public domain dedication = no copyright*

## Wirbelsäule

+ 7 Hals-,
+ 12 Brust- und
+ 5 Lendenwirbel
+ Kreuz- und Steißbein

Hals- und Lendenwirbelsäule sind leicht nach vorne gebogen, die Brustwirbelsäule nach hinten. (Doppel-S-Form) Im Halswirbelbereich ist die Wirbelsäule am beweglichsten. (Atlas, Axis)

Bis auf den 1. Halswirbel bestehen alle Wirbel aus  

+ Wirbelkörper
+ Wirbelbogen
+ verschiedenen Fortsätzen
+ Wirbelloch

Die Wirbellöcher bilden den Wirbelkanal durchlaufen von Rückenmark. Zwischen Wirbeln liegen Bandscheiben zum Abfedern.

## Brustkorb

Der Brustkorb wird von 12 Rippenpaare die hinten mit den Brustwirbeln verbunden sind gebildet. 11. und 12. Rippe sind nicht vorne am Brustbein befestigt.

## Obere Extremitäten

+ Schultergürtel
  + Schulterblättern
  + Schlüsselbeinen
  + Arme
    + Oberarmknochen
    + Ellbogen
    + Unterarmknochen (Elle, Speiche)
    + Handwurzelknochen mit Hand (Mittelhand- und Fingerknochen)

## Becken und untere Extremitäten

+ Beckengürtel
  + unteres Ende der Wirbelsäule (Kreuzbein und Steißbein) und
  + Hüftbeine (gelenkige Oberschenkelknochenverbindung)
    + Darmbein
    + Schambein
    + Sitzbein
  + Hüftgelenk
+ Oberschenkelkopf
+ Oberschenkelhals
+ Oberschenkelknochen
+ Kniescheiben
+ Unterschenkelknochen (Schienbein & Wadenbein)
+ Sprunggelenk
+ Sprungbein, Fersenbein
+ Fußwurzelknochen
+ Mittelfußknochen
+ Zehen