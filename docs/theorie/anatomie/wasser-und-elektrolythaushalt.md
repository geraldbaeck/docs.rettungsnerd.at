title:          Wasser- und Elektrolythaushalt
date:           2019/01/06
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Anatomie
md5:            4f74e9cd7fc212f171d143abcf45e75f
date_modified:  2019/01/07

## Aufgaben  
+ Steuerung wichtiger **physikalisch-chemischer Reaktionen**
+ Körper = 60% aus Wasser
    + in Zellen (intrazellulär) 40%
    + zwischen Zellen (im Interstitium) 14%
    + in Blut- und Lymphgefäßen (extrazellulär im Kreislauf) 5%
    + transzellulär (Liquor, Gelenksflüssigkeit) 1-2%
+ In Körperflüssigkeiten sind versch. Salze (Elektrolyte) gelöst.
    + ständigem Austauschvorgang und
    + Konzentrationsverschiebungen was für Steuerung aller Lebensvorgänge wichtig ist.

## Wichtigste Elemente  
+ Verdauung (Aufnahme)
+ Nieren (Entgiftung)
+ Lymphkreislauf (Wasserentsorgung der Zellen)
+ Haut (Schwitzen)
+ Lunge

Wird das Zusammenwirken durch Störung des Wasserhaushaltes (exzessive Flüssigkeitszufuhr oder Nierenversagen) und Elektrolytverluste (zB durch Erbrechen, Durchfall) gestört, kommt es zu bedrohlichen Störungen des Bewusstseins, Atmung, Kreislauf und aller wichtigen Organfunktionen.