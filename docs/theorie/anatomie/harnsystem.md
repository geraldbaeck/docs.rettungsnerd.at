title:          Harnsystem
date:           2019/01/06
version:        0.0.5
authors:        gerald@baeck.at
ankikat:        Anatomie
md5:            911ba31ab27e63627eec8f6e16b29826
date_modified:  2019/06/27

## Aufbau

+ Nieren
+ Harnleiter
+ Harnblase
+ Harnröhre

## Aufgaben

+ Endprodukte wie Harnstoff, Harnsäure auszuscheiden
+ Regulation des  Wasser- und Elektrolythaushalts
+ Produktion von Vitamin-D, Epo, Renin

## Nieren

+ hinter der Bauchhöhle
+ rechts und links neben Wirbelsäule
+ filtert das Blut
+ regelt die Konzentration (Salz, Säure, Elektrolyte, Flüssigkeit, pH Wert in Blut,)
+ liefert ca 160l Primärharn täglich, nach Rückresorbtion bleiben ca. 1,5l Harn

## Harnleiter (Ureter), Harnblase, Harnröhre (Urethra)

+ produzierter Harn sammelt sich in Nierenbecken
+ gelangt dort über Harnleiter in Harnblase
+ Harn sammelt sich in Harnblase bis bestimmter Füllungsdruck entstanden ist
+ wird dann über Harnröhre ausgegeben

[Niere Harn-System Teil 1](https://www.youtube.com/watch?v=0kyk3m313GE){: .youtube}