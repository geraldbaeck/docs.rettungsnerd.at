title:          Muskulatur
date:           2019/01/06
version:        0.0.5
authors:        gerald@baeck.at
ankikat:        Anatomie
md5:            387849560330956599f879c4f5988d55
date_modified:  2019/05/01

## Arten

+ **Glatte Muskulatur**:
    + unwillkürlich
    + Besonders reich an glatter Muskulatur sind: Blutgefäße, Darm, Lunge und Harntrakt
    + Wird vom vegetativen Nervensystem gesteuert
+ **Quergestreifte Muskulatur**
    + willkürlich
    + z.B.: Bizeps und Trizeps. (Ursprung, Ansatz)
    + Bewegung durch verkürzte Muskeln. Reize im Nervensystem lassen Muskeln zusammenziehen.
+ **Herzmuskulatur**
    + Ausnahme: quergestreift aber unwillkürlich

## Aufgaben

+ Bewegung
+ Haltung
+ Wärmeerzeugung
+ Energiehaushalt
+ Wichtig für venösen Rückstrom

## Aufbau

+ **Muskelfasern**
+ zu **Muskelbündeln** zusammengefasst
+ mehrere bilden einen **Muskel**
+ von einer **Bindegewebehülle** umgeben, die an den Enden zu **Sehnen** zusammen laufen.

Zur Versorgung der Muskeln dienen Arterien und Venen sowie Nerven zur Verbindung mit dem Gehirn.

## Funktionsweise

+ *Zusammenwirken von Muskulatur und Skelett* ermöglicht Bewegung
+ Bewegung entsteht durch *Verkürzung der Muskeln*
+ durch *Reize über Nervenbahnen* ziehen sich Muskeln zusammen
+ nach Beendigung des Reizes erschlaffen die Muskeln

+ *Skelettmuskulatur kann nur zusammen ziehen* aber nicht ausdehnen
+ dh. *jeder Muskel hat einen Gegenspieler* - Agonist und Antagonist - (zb Bizeps, Trizeps)
+ Muskeln sind immer in einem leichten Spannungszustand (Ausnahme Bewusstlosigkeit)

[Die Muskulatur](https://www.youtube.com/watch?v=3H1NbBOHXnU){: .youtube}