title:          Endokrines System
desc:           Hormonsystem
date:           2019/01/06
version:        0.0.5
authors:        gerald@baeck.at
ankikat:        Anatomie
md5:            f3efd5260b22aca033461d34d7434d97
date_modified:  2019/06/27

## Definition

Neben Nervensystem zur Steuerung körperlicher Vorgänge besitzt menschliche Körper das endokrine System. Hier werden Hormone gebildet und an Blut abgegeben. Zu dem System zählen:

+ endokrine Drüsen (geben Stoffe in die Blutbahn ab)
    + Hypothalamus (Vasopressin / ADH / Antidiuretisches Hormon)
    + Hypophyse (Somatropin / Wachstumshormon)
    + Epiphyse
    + Schilddrüse mit 4 Nebenschilddrüsen
    + Nebennieren (Adrenalin / Stresshormon, Testosteron)
    + Pankreas (Insulin)
    + Eierstöcke / Hoden (Östrogen, Testosteron)

## Beispiele

+ Blutzucker
+ Wasser- und Elektrolythaushalt (Osmoregulation)
+ Säure-/Basenhaushalt

## Funktionsweise

+ Hormone sind One2Many Botenstoffe
+ Hormone wirken nur auf Zellen mit passendem Rezeptor
+ Hormonsystem funktioniert Hierarchisch mit Feedback:) Hypothalamus > Hypophyse > Nebennieren