title:          Gewebe
date:           2019/03/07
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Anatomie
md5:            b190d5cb98babeb239c805fc855ce8d5
date_modified:  2019/03/15

## Definition

Verbände aus gleich aufgebauten Zellen mit gleicher Funktion

## Arten

+ Muskelgewebe
    + Glatte Muskulatur (nicht willkürlich, langsam, unermüdlich)
    + quergestreifte Muskulatur (willkürlich)
    + Herzmuskel (quergestreift, aber nicht willkürlich)
+ Nervengewebe
    + zentral
    + peripher
+ Bindegewebe
    + lockeres und straffes ( zb Sehnen) Bindegewebe
    + Fettgewebe (Sonderform, einzige Zellen die Fett als Strukturstoff speichern können, bei anderen Zellen ein Zeichen der Degeneration)
    + Stützgewebe (Knorpel- und Knochengewebe)
+ Epithelgewebe
    + bedeckt äußere und innere Oberflächen
    + Epidermis - Oberhaut
    + Schleimhaut
        + Darmepithel
        + Flimmerepithel der Atemwege