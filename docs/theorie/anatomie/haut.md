title:          Haut
desc:           Die äußere Schutzschicht des Menschen.
date:           2019/01/06
version:        0.0.5
authors:        gerald@baeck.at
ankikat:        Anatomie
md5:            dafa85c96d6c7a2933ebbd5ecc3d99a4
date_modified:  2019/04/21

## Arten

+ Leistenhaut: Hand- und Fußflächen, keine Haare, viele Schweißdrüsen
+ Felderhaut: Rest, Haare

## Aufbau

+ **Oberhaut** (Epidermis)
    + mehrschichtig verhorntes Plattenepithel
    + mit der darunter liegenden Lederhaut verbunden
    + keine Blutgefäße
    + besteht aus:
        + Hornschicht
            + Zellen abgestorben, enthalten keine Zellorganellen
            + Fette bilden gemeinsam mit den Hornzellen eine wasserabweisende Schutzschicht
        + Keimschicht
            + Hautfarbe wird hier produziert
            + UV Schutz

+ **Lederhaut** (Dermis)
    + Bindegewebe durchzogen von Kollagenfasern
    + Verlauf der Blut- und Lymphgefäße
    + freie Nervenendungen und Nervenfasern deren Nervenendungen (Schmerz-, Druck- und Temperaturkörperchen) reichen bis an die Oberhaut

+ **Unterhaut** (Subcutis)
    + hauptsächlich aus lockerem Bindegewebe und aus Fettzellen aufgebaut. (Speicher, Wärme, Stoßpuffer)

![Schichten der Haut](https://upload.wikimedia.org/wikipedia/commons/0/0a/HautAufbau.png)
Schichten der Haut[^1]
{: .imagewithcaption}

## Funktionen

+ **Sinnesfunktion**: Wahrnehmung von Schmerz, Druck, Vibration und Temperatur, größtes Sinnesorgan im Körper
+ **Schutzfunktion**: Schützt Körper von Umwelteinflüssen
+ **Temperaturregulation**: durch Erweiterung und Verengung der Hautgefäße sowie durch Flüssigkeitsabgabe mittels der Schweißdrüsen, Fett zum isolieren
+ **Wasserhaushalt**: Schutz vor Austrocknung, kontrollierte Abgabe von Flüssigkeit und Salzen

## Anhangsgebilde

+ Haare und Nägel
+ Talgdrüsen
+ Schweißdrüsen
+ Haarbalgmuskeln/Haaraufrichtemuskel => Gänsehaut
+ (Milchdrüse)

[Aufbau der Haut](https://www.youtube.com/watch?v=6AgHgyKET7M){: .youtube}
[Haut galore](https://www.youtube.com/watch?v=IcB90SA0-gU){: .youtube}

[^1]: [Wikimedia Commons](https://commons.wikimedia.org/wiki/File:HautAufbau.png?uselang=de), [CC-BY-SA-2.0](https://creativecommons.org/licenses/by-sa/2.0/at/deed.de)