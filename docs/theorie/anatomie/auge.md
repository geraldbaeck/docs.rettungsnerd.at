title:          Auge
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Anatomie
md5:            03b529cbaee43f56bb0661272046d94f
date_modified:  2019/06/07
date:           2019/03/15

## Aufbau

+ Hornhaut
+ vordere Augenkammer
+ Regenbogenhaut
+ Pupille
+ hintere Augenkammer
+ Linse
    + zwischen Regenbogenhaut (Iris) und Glaskörper
    + mit Zonulafasern am Ziliarmuskel befestigt, beeinflussen die Brechkraft der Linse
    + Muskel entspannt: Linse rundet sich => Nahakkomodation
    + Muskel zieht zusammen: Linse flacht ab => Fernakkomodation
+ Glaskörper
+ Netzhaut
    + im hinteren Auge
    + Fotorezeptoren
        + Zapfen (Farbsehen)
        + Stäbchen (Hell/Dunkel)
    + wandelt einfallendes Licht in Nervenimpulse um
    + gelber Fleck (scharf Sehen, viele Zapfen)
+ Sehnerv (blinder Fleck)

![Wirbeltierauge](https://upload.wikimedia.org/wikipedia/commons/a/a5/Eye_scheme.svg)
Wirbeltierauge[^1]
{: .imagewithcaption}

[^1]: [Talos, colorized by Jakov / Wikimedia Commons](https://commons.wikimedia.org/wiki/File:Eye_scheme.svg), [CC-BY-SA-3.0](https://creativecommons.org/licenses/by-sa/3.0/deed.en)