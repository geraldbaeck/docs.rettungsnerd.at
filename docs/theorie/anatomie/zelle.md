title:          Zelle
date:           2019/03/07
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Anatomie
md5:            e46ded7fda9a14affa449440cfa440c8
date_modified:  2019/06/27

## Definition

= kleineste Bau- und Funktionseinheit des Lebendigen

## Grundfunktionen

+ Reizbeantwortung
+ Eigenbewegung
+ Stoffwechsel
+ Teilung

## Aufbau

+ Zellkern (Nukleus)
    + Nukleolus
+ Zellleib (Zytoplasma) (mit Organellen)
    + Endoplasmatisches Retikulum: Kanälchensystem rund um den Zellkern, dient als Transport- und Speichersystem
    + Ribosomen: dienen der Proteinsynthese
    + Golgi-Apparat: TODO
    + Lysosomen: TODO
    + Mitochondrien: TODO
    + Zentriolen: TODO
+ Zellmembran

## Funktionen

| Funktion | Zellart |
| --- | --- |
| mechanische Kraft | Muskelzelle |
| Leitung von (elektr.) Impulsen | Nervenzelle |
| chemische Prozesse (Stoffwechsel) | Leberzelle |
| Abdeckung von Oberflächen | Epithelzelle |
| Sauerstoffstransport | rotes Blutkörperchen (Erythrozyt) |