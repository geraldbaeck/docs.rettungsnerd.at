title:          Bipolare Störung
desc:           manisch-depressiv
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Psychiatrisch
md5:            847945aebb77749e1839b3e122b2e707
date_modified:  2019/01/07

## Definition  
Manische und depressive Phasen wechseln sich ab.

## Depression  
+ gedämpfte Stimmung
+ Antriebslosigkeit
+ Ausdruck der Hoffnungslosigkeit
+ Konzentrationsstörungen
+ Suizidgedanken

## Manie  
+ Gehobene Stimmung
+ Rededrang
+ Impulsivität
+ Antriebssteigerung