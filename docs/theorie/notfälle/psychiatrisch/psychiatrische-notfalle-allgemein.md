title:          Psychiatrische Notfälle
date:           2019/01/06
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Notfälle Psychiatrisch
                HeavyRotation
md5:            335076ef37a7638e1e72882d8ff0fe44
date_modified:  2019/06/27

## Definition

Akute schwerwiegende Störung  

+ des Denkens,
+ der Stimmung,
+ des Verhaltens oder
+ sozialer Beziehung

wodurch es zur Selbst- oder Fremdgefährdung kommen kann.

Bei Psychisch auffälligen Patienten ist trotzdem immer zuerst von einer behandlungsbedürftigen Notfallerkrankung auszugehen.

## Arten

+ Organische Psychose
+ Substanzinduzierte Psychose
+ Nichtorganische Psychose
    + Schizophrene Psychose
    + Affektive Psychose
        + Manie
        + Bipolare Störung
        + Schwere Depression

## Symptome

+ Gedächtnisstörung
    + = nicht orientiert
+ Emotionale Veränderung
    + Gereiztheit
    + Aggression
    + Angstzustände
    + sich bedroht fühlen
+ Suizidalität
+ Verhaltensänderung
    + Euphorie
    + dauerndes Reden
    + Unruhe
    + Katatonie (=Zustand der äußerlichen Reglosigkeit bei massiver inneren Anspannung)
+ Soziale Veränderung
    + Rückzug
    + Verlässt Wohnung nicht mehr
+ Wahrnehmungsstörungen
    + Stimmen hören
    + Strahlen spüren
    + Tiere sehen
+ Beeinflussungsideen/Ich-Störung
    + Gedanken werden ferngesteuert
    + Radiosprecher redet über mich,...
    + ich erhalte Botschaften

## Maßnahmen

Nicht aggressiver Patient:

+ Gespräch suchen, persönliche Anrede verwenden
+ Interesse signalisieren, gezielt nachfragen, aufmerksam zuhören
+ bei Selbstgefährdung evt. Amtsarzt

Aggressiver Patient:

+ Selbstschutz
+ Keine Gewalt anwenden
+ Aggressionsabbau abwarten
+ Polizei verständigen

## Differentialdiagnosen

+ Hypoglykämie
+ Epilepsie

## Unterbringung

In einer geschlossenen psychiatrischen Abteilung darf nur untergebracht werden, wer

+ an einer psychischen Krankheit leidet und im Zusammenhang damit sein Leben, Gesundheit oder das Leben, Gesundheit anderer ernstlich und erheblich gefährdet (=Selbst- oder Fremdgefährdung) und
+ nicht in anderer Weise, insbesondere außerhalb einer psychiatrischen Abteilung, ausreichend ärztlich behandelt oder betreut werden kann.

Ob die Voraussetzung einer Unterbringung gegeben ist, muss von 2 unabhängigen Fachärzten entschieden werden.