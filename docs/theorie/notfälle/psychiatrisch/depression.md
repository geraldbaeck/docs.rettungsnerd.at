title:          Depression
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Notfälle Psychiatrisch
md5:            5bcbe0687de877bbce39c0400ee7af95
date_modified:  2019/04/27
date:           2019/04/27

## Definition

= psychische Erkrankung, die mit gedrückter Stimmung, Freud- und Interesselosigkeit sowie Antriebsarmut einhergeht

## Symptome

+ Drei Hauptsymptome
    + Gedrückte, depressive Stimmung
    + Interessensverlust und Freudlosigkeit
    + Antriebsmangel und erhöhte Ermüdbarkeit
+ Sieben Zusatzsymptome
    + verminderte Konzentration und Aufmerksamkeit
    + vermindertes Selbstwertgefühl und Selbstvertrauen (Insuffizienzgefühl)
    + Schuldgefühle und Gefühle von Minderwertigkeit
    + pessimistische Zukunftsperspektiven (hoffnungslos)
    + Schlafstörungen
    + verminderter Appetit
    + **Suizidgedanken oder -handlungen**

## Schweregrad nach ICD-10

| Grad | Symptome |
| --- | --- |
| leichte Depression | zwei Hauptsymptome und zwei Zusatzsymptome |
| mittelschwere Depression | zwei Hauptsymptome und drei bis vier Zusatzsymptome |
| schwere Depression | drei Hauptsymptome und fünf oder mehr Zusatzsymptome |

## Maßnahmen

+ Beruhigen
+ Verständnisvoller Umgang
+ Bei Uneinsichtigkeit NA-Nachforderung erwägen