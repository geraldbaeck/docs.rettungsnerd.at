title:          Akute Belastungsreaktion
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Psychiatrisch
md5:            acadc00b8a8ad30eb4c7e0389086f59c
date_modified:  2019/01/07

## Definition  
Reaktion auf eine außerordentliche psychische Belastung, für deren Bewältigung der Betroffene keine Strategie hat.

Häufige Ereignisse sind:

+ Tod eines Angehörigen
+ Gewalterfahrung
+ Unfallerlebniss

## Symptome  
+ Bewusstseinseinengung (wirkt wie betäubt)
+ Desorientiertheit
+ Verzögerte Reaktion
+ Symptome einer Panikattacke

## Maßnahmen  
+ Beziehungsaufbau (keine Vorwürfe)
+ evt Kriseninterventionsteam