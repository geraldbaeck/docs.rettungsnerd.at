title:          Asthma vs COPD
date:           2018/12/27
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Pulmonal
md5:            fbbe5648524f177bfc799d1db9ed32e8
date_modified:  2019/01/07

| &nbsp; | Asthma | COPD |
| --- | --- | --- |
| `Beginn` | meist Kindheit/Jugend | >40 |
| `Auslöser` | Allergie, Stress, Infekt | Noxen |
| `Dyspnoe` | anfallsartig | gestaltungsabhängig |
| `Verlauf` | gleichbleibend | progredient |