title:          Hypoxie
desc:           Sauerstoffmangel im Gewebe
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Pulmonal
                HeavyRotation
md5:            c1e68446427d733ef3beca80c72e877f
date_modified:  2019/06/09
date:           2019/06/07

## Definition

+ = Sauerstoffmangel im Gewebe
+ im Gegensatz zu Hypoxämie (=Sauerstoffmangel im arteriellen Blut)

## Ursachen

+ **Hypoxämische Hypoxie**: Sauerstoffmangel mit Folge der Unterversorgung von Organen (zb respiratorisch)
+ **Anämische Hypoxie**: Blutmangel, sodass nicht ausreichend Sauerstoff transportiert werden kann
+ **Ischämische Hypoxie**
+ **Histotoxische Hypoxie**: Zellen können den Sauerstoff nicht verwerten (beispielsweise bei Zyankali-Vergiftung, durch Überkonsum an Alkohol, Schlafmitteln oder Anti-Brechmitteln)
+ **Hypobare Hypoxie**, Erniedrigung des Umgebungsluftdruckes bei gleichbleibender Sauerstoffkonzentration (von ungefähr 20 %) => Sauerstoffpartialdruck sinkt
+ **Normobare Hypoxie**: die Sauerstoffkonzentration wird verringert (z. B. durch Zufuhr von Stickstoff oder CO&#8322;), bei unverändertem Luftdruck => Sauerstoffpartialdruck sinkt

## Maßnahmen

+ O&#8322;

Erst wenn mindestens 5% Hämoglobin nicht mehr oxygeniert sind, lässt sich klinisch eine (zentrale) Zyanose feststellen! Beim ausgebluteten, zentralisierten oder schwer anämischen Patienten kann daher trotz schwerster Hypoxie eine Zyanose fehlen.