title:          Hyperventilationssyndrom
date:           2019/01/06
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Notfälle Pulmonal
md5:            a9a39421e542b970e9993532e9765903
date_modified:  2019/05/18

## Definition

+ =**verstärkte Abatmung von CO&#8322;** durch **verstärkten Atemantrieb (Hyperpnoe)**
+ => Kalziummangel => Kontraktion der Muskeln (Hyperventilationstetanie)
+ => respiratorische Alkalose (hoher PH = basisch)

Bevor Maßnahmen ergriffen werden, muss ausgeschlossen sein, dass andere Gründe verantwortlich sind, wie SHT, hohes Fieber, O&#8322;-Mangel aufgrund von Verletzungen….

## Ursachen

+ Psychogenes Hyperventilationssyndrom: durch eine emotionale Stresssituation wie Angst, Aufregung, Ekstase oder Schmerz

## Symptome

+ schnelle Atmung, vom Patienten real empfundene Atemnot jedoch S<sub>p</sub>O&#8322; ~100%
+ seitengleiches Kribbeln in Armen, Beinen (peripher aufsteigend), Gesicht und Lippen
+ Hyperventilationstetanie
    + Symmetrische Pfötchenstellung
    + Grimassieren (Karpfenmund)

## Komplikationen

+ Synkopen

## Maßnahmen

+ Patienten beruhigen, ernst nehmen
+ Lagerung mit leicht erhöhtem Oberkörper, beengende Kleidung öffnen
+ keine O&#8322;-Abgabe
+ Zu **Rückatmung** anleiten => O&#8322; Maske ohne Sauerstoffgabe aufsetzen
+ Notarzt
    + evt. Sedierung
    + Psychopax

## Anamnese

+ Befragung nach dem Hergang

## Differentialdiagnosen

Eine Verwechslung mit der Tachypnoe bei der Lungenembolie ist leicht möglich! Ein möglicher Unterscheid ist der niedrige S<sub>p</sub>O&#8322; Wert bei der Lungenembolie.

Bevor Maßnahmen ergriffen werden, muss ausgeschlossen sein, dass andere Gründe verantwortlich sind, wie

+ SHT
+ hohes Fieber
+ O&#8322;-Mangel aufgrund von Verletzungen…
+ echte [Hypokalzämie](https://de.wikipedia.org/wiki/Hypokalz%C3%A4mie) (= Kalziummangel im Blut): Ebenfalls Tetanie, Gefahr der Herzrhythmusstörungen, aber verlängerte QT-Strecken