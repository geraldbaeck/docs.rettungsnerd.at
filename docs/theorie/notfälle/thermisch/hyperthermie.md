title:          Hyperthermie
date:           2019/01/06
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Notfälle thermisch
md5:            85adbace4b0b0bc75da04f223ed30e6e
date_modified:  2019/04/21

## Definition

Durch körperliche Anstrengung und schwere Arbeit in heißer Umgebung und erschwerte Schweißabgabe kommt es zu einem Wärmestau und zu einer Erhöhung der Körpertemperatur.

= die Thermoregulation versagt und zugeführte Wärme wird rascher absorbiert als abgegeben.

+ Wärmeregulierung durch Schwitzen ist nicht mehr möglich (zb hohe Luftfeuchtigkeit, keine Verdunstungskälte)
+ Hyperthermie entsteht im Sinne eines Kontinuums von Überwärmungszuständen, beginnend mit
    + Hitzestress
    + Hitzeerschöpfung
    + Hitzschlag
    + bis zu Multiorganversagen & Tod

## Risikofaktoren

+ Alter
+ Kardiovaskuläre, Haut- oder Schilddrüsen- erkrankungen
+ Medikamente (Entwässerung, Betablocker)
+ Schlechte soziale Unterstützung
+ Flüssigkeitsmangel
+ Fettleibigkeit
+ Alkohol

## Ursachen

+ Anstrengung
+ Luftfeuchtigkeit
+ hohe Umgebungstemperaturen

## Hitzestress

+ normale oder leicht erhöhte Temperatur
+ Hitzeödemen der Füße oder Knöchel => betroffenes Beine hochlagern und kühlen
+ Hitzesynkope (Vasodilatation mit Hypotonie)
+ Hitzekrampf
+ Maßnahmen
    + körperliche Ruhe an einem kühlen Ort
    + Flüssigkeitszufuhr
    + Salzzufuhr

## Hitzeerschöpfung

+ = Systemische Reaktion auf verlängerte Hitzeexposition (Stunden bis Tage)
+ bedrohliches Zustandsbild, kann rasch zum Hitzschlag führen
+ Symptome
    + KKT 37° - 40°C
    + Kopfschmerzen
    + Schwindel
    + Übelkeit
    + Erbrechen
    + Tachykardie
    + Hypotonie
    + Schwitzen
    + Muskelschmerzen
    + Abgeschlagenheit
    + Krämpfe
    + Bluteindickung,
    + Hyper- oder Hyponatriämie
+ Maßnahmen
    + iv Flüssigkeitszufuhr, Eispackungen für schwere Fälle
    + Monitoring, psych. Betreuung
    + Hospitalisierung

## Hitzschlag

+ = Systemische Entzündungsreaktion mit Körperkerntemperaturen über 40,6°C
+ Symptome
    + KKT >= 40,6°C
    + Heiße, trockene Haut (aber auch Schwitzen ist möglich)
    + Müdigkeit, Kopfschmerzen, Übelkeit, Durchfall
    + Kardiovaskuläre Störungen
    + Atembeschwerden
    + Krämpfe, Koma
    + Tod durch MOV
+ Maßnahmen
    + Entsprechende Lagerung
    + Kühlung
    + Infusionen
    + Monitoring
    + möglicherweise notärztliche  Intubation