title:          Verbrennungen
date:           2019/01/06
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Notfälle thermisch
md5:            83373dfe9fef0fbc5088ad4b1bf7c188
date_modified:  2019/05/30

## Definition

Kontakt mit

+ Feuer oder heißen Gegenständen
+ elektrischem Strom oder
+ Strahlung

führt zur thermischen Schädigungen des Gewebes.

**Verbrühung**: heiße Flüssigkeiten oder Dampf.

**Lebensgefahr ab 20% Verbrennung (2. Grades)** = großflächig (10%Kind, 5%Säugling)
{: warning}

+ Handflächenregel: Handfläche des Patienten = 1%
+ 9er Regel
  + Kopf, Arm l/r 9%
  + Bein l/r, Rumpf v/h 18%

![9er Regel bei Verbrennungen](https://rawgit.com/geraldbaeck/RS_WRK/master/charts/verbrennung_9erregel.svg)
9er Regel bei Verbrennungen[^1]
{: .imagewithcaption}

[^1]: [Gerald Bäck](mailto:gerald@baeck.at) with a [CC0-1.0](https://creativecommons.org/licenses/by-sa/2.0/at/deed.de) public domain dedication = no copyright

## Symptome

| Grad | Symptome | Schädigung | Beispiel |
| --- | --- | --- | --- |
| `1` | Rötung, Schwellung, Schmerzen | Epidermis | Sonnenbrand |
| `2a` | Blasen, Schwellung, Schmerzen | Epidermis und oberer Anteil der Dermis | typ. Brandblase |
| `2b` | Blasen, Rötung, kaum Schmerzen | Tiefe Schichten der Dermis mitbetroffen | |
| `3` | grauweiß, trocken, lederartige Hautgebiete, keine Schmerzen | Epidermis + Dermis (= Cutis) und Subcutis verbrannt | |
| `4` | Verkohlung | Tiefer liegende Schichten mitbetroffen (z.B. Muskeln, Fett, Faszien, Knochen) | |

## Komplikationen

+ Atemnot
+ Zusatzverletzungen
+ Verbrennungskrankheit => **Schocksymptomatik durch Flüssigkeits- und Proteinverlust**
+ Thermisches Inhalationstrauma (Symptom: verrußter Mund & Nase) => **toxisches Lungenödem**
+ Wundinfektion => **Sepsis**

## Verbrennungskrankheit

Freisetzung von gefäßaktiven Substanzen im Körper bewirkt Flüssigkeitsverlust und Entstehung von massiven Ödemen. Schockzustand wird durch Abgabe von Flüssigkeit über Wundflächen verstärkt. Kann zu Versagen lebenswichtiger Organe (besonders Nieren und Lunge) führen. 

## Thermisches Inhalationstrauma

Schädigung der Atemweg sehr wahrscheinlich wenn Pat. aus Bränden in geschlossenen Räumen gerettet werden. Kann zu toxischem Lungeödem führen.

## Managementnach PHTLS

+ SSS
    + Spezialkräfte
    + Eigenschutz
    + Wer löscht?
    + Mehrere Verletzte?
+ GI
    + Rettung nötig?
    + Verbrennung und Bewusstseinslage abschätzen, Hals od. Gesicht betroffen
    + Eigenschutz
    + Sauerstoff
+ A
    + auf gerußte Atemwege/Nase achten
    + frei?
+ B
    + Inhalationstrauma?
    + Lungenödem?
+ C
    + betroffene Körperfläche
    + Verbrennungsgrad
+ D
+ E
    + Wärmemanagement

+ Transport
    + Stationäre Aufnahme
        + > 10%
        + Kinder >5%
    + Verbrennungsbett
        + 2° >20%
        + 3° >10%
        + bei kritischen Regionen (Hals, Gesicht)

## Maßnahmen

+ Hitzezufuhr unterbrechen
+ Kleiderbrände löschen + vorsichtig entfernen
+ Kühlen (lauwarmes Wasser, vorsicht vor Unterkühlung)
+ Wärmeerhalt
+ nasse Kleidung entfernen
+ Entfernung von Schmuckstücken etc.
+ keimfreie Versorgung
+ Bei Verdacht auf Inhalationstrauma = SPO&#8322;-Gabe 10-15l
+ Schockbekämpfung
+ Flüssigkeitsgabe (Formel: 4ml Ringer x kg KG x % der Verbrennung)