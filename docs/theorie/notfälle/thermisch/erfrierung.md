title:          Erfrierung
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle thermisch
md5:            e5de8f9eeded323450d1969c4111372c
date_modified:  2019/01/07

## Definition
Durch Kälte hervorgerufene Gewebsschäden. Die Beurteilung ist schwierig, im Zweifelsfall immer von einer Erfrierung ausgehen.

## Symptome:
| Grad | Symptome |
| :---: | --- |
| `1` | blasse Hautfarbe, Schwellung der Hautpartie, prickelnde Schmerzen |
| `2` | blau-rote Hautfarbe, Blasenbildung |
| `3` | beinahe schmerzfreies Absterben des Gewebes |
| `4` | Vereisung und völlige Gewebezerstörung |

## Komplikationen  
+ Infektionen
+ Verlust der betroffenen Körperteile

## Maßnahmen  
+ Körperwärme erhalten
+ Beengende Kleidung öffnen
+ Keimfreie Wundversorgung
+ Betroffene Körperteile vor weiterer Kälteeinwirkung und Belastung schützen
+ Verabreichung warmer Getränke