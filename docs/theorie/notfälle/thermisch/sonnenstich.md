title:          Sonnenstich
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle thermisch
md5:            e0eccf1856de2f602e281f627127f8ee
date_modified:  2019/01/07

= Hirnhautreizung

## Symptome  
+ Heißer, roter Kopf
+ Kopfschmerzen, evtl. Nackensteifigkeit
+ Schwindel, Übelkeit, Erbrechen
+ Hyperventilation, Krämpfe
+ Bewusstseinsstörungen

## Komplikationen  
+ Bewusstlosigkeit
+ Hirndrucksteigerung

## Maßnahmen  
+ in den Schatten bringen
+ Kleidung möglichst entfernen
+ Pat. kühlen, Flüssigkeit verabreichen
+ Lagerung mit erhöhtem Oberkörper