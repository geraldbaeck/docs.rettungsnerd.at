title:          Unterkühlung
desc:           Hypothermie
date:           2019/01/06
version:        0.0.5
authors:        gerald@baeck.at
ankikat:        Notfälle thermisch
md5:            72b4089e62d04524983f69d04674eb68
date_modified:  2019/04/21

## Definition

= Absinken der Kernkörpertemperatur unter Normtemperatur von 35° Celsius.

+ mild 32-35°C
+ moderat 30-32°C
+ schwer < 30°C

## Maßnahmen

+ Kontrolle der Lebensfunktionen
+ lebensrettende Sofortmaßnahmen
+ vorsichtige Rettung in vorgefundener Stellung
+ Pat. vor weiterem Wärmeverlust schützen (Decke - Rettungsdecke - Decke)
+ kalte oder nasse Kleidung entfernen, den Patienten abtrocknen, zudecken und windgeschützt lagern
+ warme Infusionen

## Reanimation

+ Atemwege frei machen
+ falls keine Spontanatmung vorliegt, mit hoher O&#8322;- Konzentration beatmen
+ ev. vorsichtig intubieren (kann Kammerflimmern auslösen)
+ Puls an einer größeren Arterie tasten und eine Minute lang das EKG beobachten, bevor man die Feststellung eines Herzstillstandes trifft
+ im Zweifel CPR starten (30: 2 im normalen Rhythmus)
+ Bei Kammerflimmern unter 30°C Körpertemperatur 1 Schock, die weiteren erst, wenn der Patient auf über 30°C erwärmt ist.
+ Verabreichung von Medikamenten im doppelten Intervall solange der Patient stark unterkühlt ist.
+ Arrhythmien (mit Ausnahme von Kammerflimmern) können sich nach Erreichen von normalen Temperaturwerten normalisieren

## Todesfeststellung

Die Todesfeststellung bei hypothermen Patienten ist schwierig, weil

+ langsamer, fadenförmiger, unregelmäßiger Puls
+ nicht messbarer Blutdruck
+ Hypothermie schützt das Gehirn und lebenswichtige Organe, die Arrhythmien sind potentiell reversibel.
+ Bei  18°C kann das Gehirn 10x längere Phasen von Herzstillstand tolerieren als bei 37°C
+ Weite Pupillen können viele Ursachen haben und dürfen nicht als sichere Todeszeichen gewertet werden.

No one is dead until warm and dead!
{: .info}