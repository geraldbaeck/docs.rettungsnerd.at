title:          Lage des Kindes
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Gynäkologisch
md5:            04052a8ffd744665a8bee3e49e8ca205
date_modified:  2019/01/07

| % | Lage |
|--:|:--|
| `96%` | Längslage mit Kopf als führenden Körperteil (Hinterhauptlage) |
| `3%` | Steißlage (Beckenendlage) |
| `1%` | Querlage |

## Beckenendlage (Steißlage)
+ Steiß zuerst, Kopf zuletzt
+ Geburtsfortschritt meisten gering => Krankenhaus kann erreicht werden; außer Frühgeburt od Mehrgebährende