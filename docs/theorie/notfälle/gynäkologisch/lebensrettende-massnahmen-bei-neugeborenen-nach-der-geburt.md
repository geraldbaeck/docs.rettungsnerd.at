title:          Lebensrettende Maßnahmen nach der Geburt
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Gynäkologisch
md5:            4f17ec5933e54d527de1ecd59ab9e0f4
date_modified:  2019/01/07

Unmittelbar nach der Geburt wird im ersten Eindruck beurteilt:

## Atmung und Kreislauf
+ Atmet das Kind kräftig, schreit es?
+ Hebt sich der Brustkorb seitengleich?

## Hautfarbe
+ rosig, zyanotisch oder blass
+ Ein gesundes Baby wird bei effektiver Spontanatmung in 30s rosig

## Muskeltonus
+ Guter Tonus: kärftige Bewegungen
+ reduziert: langsam, träge, evt nach Reiz
+ schlaff

## Sicherstellen der Lebensfunktionen
+ Sorgfältig abtrocknen (Stimulation regt zur Spontanatmung an)
+ Sofort abnablen
+ Wärmeerhalt
+ Atemwege frei machen. Kopf in Neutralstellung, evt Absaugen
+ Atemkontrolle => Säuglingsreanimation