title:          Gynäkologische Notfälle
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Gynäkologisch
                HeavyRotation
md5:            6df3ea12bac22a25aec623c66616a54a
date_modified:  2019/06/27
date:           2019/05/09

## Leitsymptome

+ Gynäkologische Blutung
    + Starke Unterleibsschmerzen
    + Tachykardie
    + Unruhe, Angst
    + Dyspnoe
    + Evt. Harte Bauchdecke
+ Schmerzen
    + nur Vaginale Blutungen sind spezifisch gynäkologische Beschwerden
    + Schmerzen hingegen sind unspezifisch und dem Akuten Abdomen einzuordnen
    + Es gibt keinen typischen, spezifischen auf eine Gyn Ursache hinweisenden Schmerz

## Ursachen

+ Ohne Schwangerschaft
    + Hypermenorrhö (verstärkte Regelblutung)
    + Verletzungen (Unfall, Sexualdelikt)
    + Vulvahämatom
    + Karzinom
    + azyklische-irreguläre Uterusblutung
    + Ovarialtorsion
+ mit Schwangerschaft
    + Abortus
    + Plazenta praevia
    + vorzeitige Plazentaablösung
    + Uterusruptur
    + Eileiterschwangerschaft (=Tubaria, Extrautaringravidität)

## Anamnese

+ letzte Regelblutung: Charakter, Stärke, Dauer, Zeitabstand
+ Schwanger (Ja/Nein)
+ Wann war die letzte gyn. fachärztliche Untersuchen?
+ Hatten Sie vor kurzem einen gyn. Eingriff (Operation)

## Maßnahmen

+ Lagerung nach Fritsch
+ Schockbekämpfung
+ evt. NA