title:          Schulterdystokie
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Gynäkologisch
md5:            a3279adc3b35b513fb92aa840c76c175
date_modified:  2019/07/08
date:           2019/04/01

## Definition

Schulter bleibt hinter dem Beckenknochen hängen. Kopf wird geboren und dann passiert nichts mehr. 

+ Turtle Sign (=Nach der Geburt des Kopfes zieht dieser sich wieder zurück und scheint dem Beckenausgang förmlich aufgepresst.)
+ Keine Drehung
+ => Sauerstoffmangel => Hirnschädigung

## Risikofaktoren

+ Übergewicht der Mutter
+ Übergröße des Kindes (>4kg)
+ diabetische Mutter
+ forciertes Geburtsmanagement

## McRoberts Manöver

![McRoberts Manöver](https://upload.wikimedia.org/wikipedia/commons/c/c3/McRoberts_maneuver.svg)Mc Roberts Manöver[^1]
{: .imagewithcaption}

[^1]: [geraldbaeck / Wikimedia Commons](https://commons.wikimedia.org/wiki/File:McRoberts_maneuver.svg), [CC0](https://creativecommons.org/publicdomain/zero/1.0/deed.de)