title:          Abortus
desc:           Fehlgeburt
date:           2019/01/06
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Notfälle Gynäkologisch
md5:            3ebc6b8135512c9fe4a9b4754ac587a7
date_modified:  2019/05/10

## Definition

Auf Grund verschiedener Ursachen kann es zu einer Ausstoßung des Fötus (unter 500g) kommen. = Fehlgeburt

## Symptome

+ vaginale Blutung
+ Wehenartige, ziehende Schmerzen
+ evt Abgang von Gewebeteilen/Fruchtwasser

## Komplikationen

+ starke Blutung

## Maßnahmen

+ Transport → Gyn. Abteilung
+ Mitnahme des Fötus