title:          Gestationsdiabetes
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Notfälle Gynäkologisch
md5:            a2b27d1a7c9b639b3091c19f499fd98c
date_modified:  2019/06/27
date:           2019/05/09

## Definition

+ = Diabetes der Mutter, der erstmals in der Schwangerschaft diagnostiziert wird
+ Meistens Normalisierung nach der Geburt
+ auch: Schwangerschaftsdiabetes, Gestationsdiabetes mellitus (GDM) oder Typ-4-Diabetes

## Risikofaktoren

+ Übergewicht
+ &gt; 30a
+ erbliche Vorbelastung mit Diabetes Mellitus

## Komplikationen

+ Hypertonie
+ Präeklampsie
+ anfälliger für Harnwegsinfektionen und Scheidenentzündungen
+ erhöhte Kaiserschnittrate

## Maßnahmen / Vorbeugung

+ Screening mit Mutter-Kind-Pass!