title:          Maßnahmen bei der Geburt
date:           2018/12/28
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Notfälle Gynäkologisch
md5:            8e5ba4788e066af8b46302a3be4785d4
date_modified:  2019/01/07

+ Lagerung der Mutter liegend und linksseitig
+ Wenn das Hinterhaupt in der Wehenpause sichtbar bleibt == Mutter in Geburtsposition (Sitzend Beine angezogen oder knieend)
+ Fahrzeug abstellen und gut heizen
+ Geburtspaket vorbereiten inkl Sauerstoff
+ Geburt des Kopfes möglichst langsam, um Dammriss zu vermeiden, durchatmen und hecheln, nicht mehr pressen
+ ist der Kopf geboren, mit steriler Wundkompresse abwischen
+ **ACHTUNG: Kind ist rutschig** => abwischen
+ Atmung und Puls kontrollieren
+ Abnabelung: mindestens 1 Minute warten, reglose sofort abnabeln
+ Nachbetreuung:
    + Kind: Wärmerhalt, Versorgung der Nabelschnur
    + Mutter: Nabeschnur am Oberschenkel kleben, Unterlage tauschen, Lagerung nach Fritsch

Der vollständige Austritt des Körpers gilt als Geburtstermin
{: .info}