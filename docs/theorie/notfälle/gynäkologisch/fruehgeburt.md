title:          Frühgeburt
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Gynäkologisch
md5:            bacc288234dd0cbd7f38d0784ee799e7
date_modified:  2019/01/07

## Definition
Vor der 37. SSW. Überlebenschance besteht ab der 24. SSW

## Symptome
+ Geburtsbeginn

## Komplikationen für das Kind
+ Atemstörung
+ Atem-Kreislaufstillstand
+ Unterkühlung

## Maßnahmen
+ wie normale Geburt
+ Wärmeerhalt
+ Kontrolle der Lebensfunktionen