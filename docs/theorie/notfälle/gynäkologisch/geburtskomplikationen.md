title:          Geburtskomplikationen
date:           2019/01/06
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Notfälle Gynäkologisch
md5:            8939712d1a0586f508dad679ca0900d1
date_modified:  2019/06/27

## Definition

| Bezeichnung | SSW | Gewicht | Lebenszeichen |
| --- | --- | --- | --- |
| [Fehlgeburt (Abortus)](abortus.md) | - | < 500g | tot geboren |
| Totgeburt | - | > 500g | tot geboren |
| [frühgeborene Lebendgeburt (Frühgeburt)](fruehgeburt.md) | < 37 SSW | > 500g | Nach Scheidung vom Mutterleib:<ul><li>Herzschlag oder</li><li>Nabelschnurpulsation oder</li><li>Lungenatmung oder</li><li>willkürliche Muskelbewegung</li></ul> |