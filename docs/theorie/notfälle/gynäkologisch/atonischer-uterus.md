title:          Atonischer Uterus
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Notfälle Gynäkologisch
md5:            16c873b24c37c1f8a0f6a6db12e4c0f3
date_modified:  2019/05/10
date:           2019/05/10

## Definition

+ = Kontraktionsschwäche (Atonie) der Gebärmutter
+ = fehlende oder ungenügende Fähigkeit der Gebärmuttermuskulatur, sich nach der Geburt des Kindes zusammenzuziehen
+ => starke bis lebensbedrohliche Blutung, die ein unverzügliches Eingreifen erfordert
+ häufigste Ursache mütterlicher Mortalität
+ auch: Uterusatonie, atonische Nachblutung

## Symptome

+ massive Nachblutungen nach Geburt

## Maßnahmen

+ Lagerung, Monitoring, i.V. Kristalloide Lösung
+ Wehen fördern → NA
+ rascher Transport → Gyn. Abteilung