title:          Schwangerschaftsinduzierte Hypertonie und Eklampsie
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Gynäkologisch
md5:            4dc23612d1c339c18533d1ec230068bb
date_modified:  2019/01/07

## Definition
In de letzten Monaten der SW oder während der Geburt kann es zu Krampfanfällen kommen, die das Leben von Mutter und Kind gefährden können.

## Symptome
+ Hypertonie
+ Ödeme
+ Krämpfe

## Komplikationen
+ Lebensgefahr für Mutter und Kind

## Maßnahmen
+ Leicht erhöhter Oberkörper in linker Seitenlage