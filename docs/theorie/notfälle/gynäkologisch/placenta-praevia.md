title:          Placenta praevia
date:           2019/01/06
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Notfälle Gynäkologisch
md5:            c4f522c22e89ad0e27719d221eaa2f2b
date_modified:  2019/04/01

## Definition

Mutterkuchen liegt vor Muttermund – Geburtskanal verdeckt. Dadurch kann es zu Blutungen kommen. Fehllage wird üblicherweise bei routinemäßigen Ultraschalluntersuchungen entdeckt.

## Symptome

+ Schmerzfreie vaginale Blutung

## Komplikationen

+ Lebensgefahr für Mutter und Kind

## Maßnahmen

+ Lagerung nach Fritsch