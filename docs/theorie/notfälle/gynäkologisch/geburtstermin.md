title:          Geburtstermin
date:           2018/12/28
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Gynäkologisch
md5:            608cc29dc322180c7623d97abda05e21
date_modified:  2019/04/01

**Dauer**: 40 Wochen ab Befruchtung der Eizelle

**Errechneter Geburtstermin** (EGT):

+ post conceptionem vs post menstruationem
+ Abweichungen bis zu 2 Wochen sind normal
+ mit Hilfe Ultraschall kann der Termin genauer bestimmt werden

Naegele Regel:

+ ein Jahr nach dem ersten Tag der letzten Regelblutung plus sieben Tage minus 3 Monate
+ = letzte Regelblutung + 372d - 3M = 280d = 40 Wochen