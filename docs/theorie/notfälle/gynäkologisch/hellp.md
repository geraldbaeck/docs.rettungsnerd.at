title:          HELLP Syndrom
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Gynäkologisch
md5:            0c8ef2ccbd9544d8f6b38dc7d5ea7c57
date_modified:  2019/06/02
date:           2019/05/09

## Definition

= komplizierte Form der Präeklampsie

## Risikofaktoren

+ Plazentationsstörung
+ DM / Gestationsdiabetes
+ vorbestehende Hypertonie
+ Antiphosphorlipid-Syndrom
+ Präeklampsie
+ Mehrlingsschwangerschaft
+ Adipositas

## Symptome

+ **H**ämolysis (Auflösung der roten Blutkörperchen)
+ **E**levated **L**iver (erhöhte Leberwerte)
+ **L**ow **P**latelet (Verminderung der Blutplättchen)

Zusätzlich:

+ starke re. Oberbauch Schmerzen
+ evtl. Gelbfärbung der Haut

## Komplikationen

+ Leberzellschaden
+ Leberruptur
+ akutes Nierenversagen
+ vorzeitige Ablösung der Plazenta