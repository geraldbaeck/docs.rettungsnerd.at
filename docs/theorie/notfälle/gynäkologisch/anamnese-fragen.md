title:          Anamnese vor der Geburt
date:           2018/12/28
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Gynäkologisch
md5:            ff097fbe866d58541cf93c57f2f5e746
date_modified:  2019/04/01

+ Wie viele vorgehende Geburten?
+ Wehen, wenn ja welche Abstände?
  + länger als 3-7 min = Eröffnungsperiode
  + alle 2 min, Dauer 60-90 Sek. => **Austreibungsphase, Fahrt nicht mehr antreten**
+ Blasensprung bereits erfolgt?
    + Wie schaut das Fruchtwasser aus? (klar == gut)
    + Wann?
    + Kindsbewegungen
+ Geburtstermin
+ Komplikationen
+ Blutungen
+ Mutter Kind Pass