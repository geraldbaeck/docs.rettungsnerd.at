title:          Präeklampsie
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Gynäkologisch
md5:            a259e0d915cf9fe5b56856854271143c
date_modified:  2019/05/10
date:           2019/05/09

## Definition

+ = hypertensive Erkrankung während der SS
+ auftreten im letzten Drittel der SS ab ca. 20 SSW
+ auch: EPH-Gestose

## Symptome

+ Hypertonie
+ Proteinurie (Eiweiß im Urin)
+ Ödeme

## Komplikationen

+ Eklampsie
+ HELLP Syndrom

## Maßnahmen

+ Bei Diagnose stationäre Aufnahme & engmaschige Med. Überwachung
+ kann zu schweren Komplikationen führen