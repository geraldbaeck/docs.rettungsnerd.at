title:          Glaukom
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Notfälle Augen
md5:            63b88f2c759adc139ed53d927db4e081
date_modified:  2019/05/30
date:           2019/05/06

## Definition

+ Glaukom = Grüner Star
+ = Erhöhung des Augen-Innendruckes durch Störung der Kammerwasser- Zirkulation
+ kann zur Zerstörung des Sehnerv führen

## Komplikationen

+ akuter Glaukom Anfall
    + Augen- Innendruck steigt durch eine Abflussbehinderung des Kammerwassers sehr rasch an
        + tastbar (palpatorisch) steinharter Augapfel
        + meist nur ein Auge betroffen
        + Schmerzen
        + Rötung des Auges
        + Sehverschlechterung
        + Allgemeinsymptome (Kopfschmerzen, Magen- Darm- Beschwerden wie z. B. Übelkeit...)

## Maßnahmen

+ an akuten Glaukom Anfall denken