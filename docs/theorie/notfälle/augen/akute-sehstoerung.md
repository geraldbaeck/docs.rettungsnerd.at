title:          Akute Sehstörung
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Notfälle Augen
md5:            d0a4633e24b393c6338cd1d1b5d3d0ed
date_modified:  2019/05/06
date:           2019/05/06

Die Ursache einer akuten Sehstörung kann im Auge selbst liegen

+ Glaukomanfall
+ Netzhautablösung
+ Verschluss von Augen- Vene oder –Arterie...

oder auch Symptom eines Schlaganfalles sein.

## Sonderfälle

+ Pupillenveränderungen: siehe Neurologie
+ Monokel- oder Brillenhämatom: siehe Unfallchirurgie