title:          Augapfelprellung
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Notfälle Augen
md5:            e32e8211308436d8dface8de9f0b7a41
date_modified:  2019/05/06
date:           2019/05/06

## Definition

+ = stumpfes Trauma des Augapfels. zb durch
    + Faustschlag
    + Schneeball
    + Sektkorken
    + Tennisball

## Symptome

+ Schmerzen
+ Eingeschränktes Sehvermögen
+ Doppelbilder
+ Schwellung der Lider und Konjunktiva

## Komplikationen

+ Netzhautablösungen
+ Linsenluxation

## Maßnahmen

+ kühlen
+ lockerer Verband beider Augen