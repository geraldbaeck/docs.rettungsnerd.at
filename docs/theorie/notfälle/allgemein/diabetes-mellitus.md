title:          Diabetes Mellitus
date:           2019/01/06
version:        0.0.6
authors:        gerald@baeck.at
ankikat:        Notfälle
md5:            a0815fc14b9f7a10446accd366b5c4d8
date_modified:  2019/05/18

## Definition

+ =Störung des Kohlehydratstoffwechsels
+ =Gestörtes (unausgewogenes) Verhältnis BZ/Insulin

+ Körper benötigt Energie, die zum Großteil von im Blut vorhandenem Zucker (aufgenommen mit Nahrung) stammt
+ Zur Zuckerverwertung, ist das in Bauchspeicheldrüse gebildete Hormon Insulin notwendig
+ Blutzuckerspiegel erhöht sich und Zucker, wenn die sog. Nierenschwelle (>160-180 mg/dl) überschritten ist, wird dann über Nieren ausgeschieden.

## Typ I - IDDM - Insulin Dependent Diabetes Mellitus

+ genetische Prädisposition und Virusinfektion führen zur
+ Zerstörung der "Inselzellen" in der Pankreas
+ => Produktionsstörung in der Pankreas
+ => Insulin fehlt

+ Insulin muss immer von außen zugeführt werden.
+ entwickelt sich hauptsächlich im Kinder- und Jugendalter.

## Typ II - NIDDM - Non Insulin Dependent Diabetes Mellitus

+ Insulin wird produziert aber nicht in Zellen eingeschleust (Insulinresistenz)
+ => Insulinproduktion erschöpft sich => Insulingabe nötig

+ wird mit Medikamenten behandelt,
+ => Gewebe für Insulin empfindlicher (**[Insulin-Sensitizer](https://de.wikipedia.org/wiki/Insulin-Sensitizer)**)
+ Entsteht meist ab 40 wegen Überernährung und zu wenig Bewegung

Normwert beim Erwachsenen: <110mg/dl
{: .info}

## Symptome

+ häufiges Wasserlassen (Polyurie) nächtliches Wasserlassen (Nykturie)
+ starker Durst
+ geringer Appetit u. Gewichtsverlust
+ Heißhungerattacken
+ Müdigkeit, Kraftlosigkeit
+ Mundtrockenheit u. nächtliche Wadenkrämpfe

## Komplikationen

+ Infektionen
+ Erblindung
+ Nierenversagen
+ Schädigung der Blutgefäße => periphere Durchblutungsstörungen
+ Polyneuropathie
+ Arteriosklerose => Hypertonie
+ Hyper-/Hypoglykämie