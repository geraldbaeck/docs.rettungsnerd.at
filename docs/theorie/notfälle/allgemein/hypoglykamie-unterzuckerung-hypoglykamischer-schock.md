title:          Hypoglykämie
desc:           Unterzuckerung / Hypoglykämischer Schock
date:           2019/01/06
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Notfälle
md5:            9f246eefa495553908c6aa2f46457827
date_modified:  2019/04/20

## Definition

Werte unter 50mg/dl. Erste Symptome oft schon bei <70mg/dl.

## Ursachen

+ zu geringe Aufnahme von Kohlenhydraten (Diätfehler)
+ relative Insulin Überdosierung (Applikationsfehler)
+ Erniedrigung des Insulinbedarfs zb durch körperliche Belastung, nach Gewichtsreduktion, nach Entbindung
+ Alkoholintoxikation
+ Nebenniereninsuffizienz

## Symptome

+ Heißhunger
+ Müdigkeit, Schwäche
+ Herzklopfen
+ Schweißausbruch, Zittern, evt Doppelbilder
+ Aggressive Gereiztheit
+ Bewusstseinseintrübung, Bewusstseinsveränderung
+ Desorientiertheit
+ => kann wie alkoholisiert wirken

CAVE: kann alkoholisiert wirken{: .warning}

## Komplikationen

+ Somnolenz od Bewusstlosigkeit

## Maßnahmen

+ Zucker geben
+ Absaugbereitschaft
+ Blutzuckermessung

## Anamnese

+ Zuckerpass
+ Letzte Mahlzeit

## Zu beachten

+ Messtoleranzen von bis 20% können vorkommen. Daher muss bei Messwerten <60mg/dl  mit entspr. Symptomatik gehandelt werden (Glykosezufuhr oral).
+ längere hypoglykämische Bewusstlosigkeit kann zu Hirnschäden führen. Bei Auffälligkeiten Klinik mit Schädel-CT anfahren.
+ Patienten können verwirrt und aggressiv sein und enorme Kräfte entwickeln => Eigenschutz
+ Ersthelfer könnten schon eine Zuckergabe durchgeführt haben