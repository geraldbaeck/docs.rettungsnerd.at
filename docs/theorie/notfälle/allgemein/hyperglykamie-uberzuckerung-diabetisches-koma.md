title:          Hyperglykämie
desc:           Überzuckerung / Diabetisches Koma
date:           2019/01/06
version:        0.0.6
authors:        gerald@baeck.at
ankikat:        Notfälle
md5:            68c11feb4c662b5d45181825c00eb02c
date_modified:  2019/05/16

## Definition

= Stark erhöhter Blutzuckerspiegel => langsame Bewusstseinseintrübung bei

+ bekannter Diabetes Mellitus durch Infektionen oder Insulinunterdosierung
+ aber auch Erstmanifestation.

Der Insulinmangel bewirkt eine hormonelle Gegenreaktion durch Stresshormone.

Führt chronisch zu:

+ Schädigung von Gefäßen und Nerven
+ Durchblutungsstörungen in Augen, Nieren und Extremitäten
+ Störung der Blutgerinnung

## Ketoazidose

+ = Übersäuerung durch absolutem Insulinmangel, v. a. bei Typ 1- Diabetikern
+ = Lebensgefährlicher Zustand! Entwickelt sich innerhalb von Stunden bis Tagen
+ Keton in Blut und Harn

Symptome:

+ Übelkeit, Erbrechen, Bauchschmerzen
+ Hyperventilation (Kussmaulsche Atmung, angestrengte, vertiefte Atmung)
+ Azetongeruch der Atemluft
+ Müdigkeit
+ Bewusstlosigkeit

## Hyperosmolares Koma

+ Komplikation des Typ 2- Diabetes
+ relativer Insulinmangel
+ Flüssigkeitsverlust durch vermehrte Zucker-/ Wasserausscheidung durch die Niere
+ Austrocknung
+ Keine Blutübersäuerung, aber BZ-Werte über 600mg/dl

## Komplikationen

+ Somnolenz bis Bewusstlosigkeit

## Maßnahmen

+ BZ- Check
+ trinken lassen
+ Bei Bewusstlosigkeit entsprechende Basismaßnahmen
+ Monitoring
+ O&#8322;
+ Notfallanamnese (Diabetikerausweis!)
+ (venöser Zugang, Flüssigkeit)
+ Hospitalisierung
+ Absaugbereitschaft

## Anamnese

+ Diabetikerausweis und Medikamente mitnehmen
+ Krankengeschichte