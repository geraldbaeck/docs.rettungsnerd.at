title:          Dehydration
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Notfälle allgemein
md5:            fcfd1fa670298be0cfe96fcabccc1018
date_modified:  2019/05/09
date:           2019/04/20

## Definition

+ **Dehydration**
    + = Abnahme des Körperwassers
    + = Volumenmangel der extrazellulären Flüssigkeit
+ **Exsikkose**
    + = Schwere Form der Austrocknung
    + Folge der Dehydration

## Ursachen

+ Erbrechen
+ Durchfall
+ Schweiß
+ zu wenig trinken
+ erhöhter Urin-Ausstoß
+ starke Blutungen
+ Verbrennungen
+ Salzwasser trinken
+ Morbus Addison (Nebennierenunterfunktion)
+ Diuretika Überdosierung

## Symptome

+ trockene Schleimhäute (Durst)
+ Geringe Harnproduktion
+ stehende Hautfalten
+ eingefallenes Gesicht
+ RR niedrig
+ Bewusstseinsstörung
+ ev. Krämpfe

## Komplikationen

+ Kreislaufschwäche
+ Bewusstseinsstörungen wie Lethargie, Verwirrtheit und Delirium bis hin zum Koma
+ Schock

## Maßnahmen

+ Rehydrierung = Flüssigkeit (oral, iv)