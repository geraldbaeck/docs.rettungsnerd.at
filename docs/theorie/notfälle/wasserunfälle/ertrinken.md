title:          Ertrinken
des:            Verstauchung
date:           2019/01/06
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Notfälle Wasserunfälle
md5:            b735e80f444c23e6bdef43f16c1855bc
date_modified:  2019/06/18

## Definition

= Tod durch Ersticken in Folge eines Untertauchens und dem damit verbundenen Eindringen von Wasser in die Atemwege

## Mechanismus

+ Primäres Ertrinken
    + Betroffene tauchen mehrmals auf und wieder unter (Überlebenskampf)
    + Schlucken und aspirieren Wasser
+ Primäres Versinken
    + Auftauchphasen fehlen (zB: bei Verletzungen oder bei Kindern)
+ Sekundäres Ertrinken
    + Bis 48h nach Unfall kann es zu Verschlechterung des Zustandes kommen, daher stationäre Überwachung wichtig.
    + Lungenödem möglich

## Phasen

+ Abwehrphase
+ Atemanhaltephase
+ Dyspnoische Phase
+ Atem-Kreislaufstillstand
+ Schnappatmung

## Ursachen

+ Nichtschwimmer
+ Ermüdung und Krämpfe
+ Vagusreizung = Synkope durch plötzliches Eintauchen in sehr kaltes Wasser ggf nach reichhaltigem Essen
+ [ACS](/theorie/notfälle/kardiovaskulär/angina-pectoris-und-herzinfarkt-akutes-koronarsyndrom/)

## Symptome

+ Feucht-blasse Haut, Zyanose
+ Kältezittern
+ Verwirrtheit bis Bewusstlosigkeit
+ Erhöhte Atem- und Pulsfrequenz bis Schnappatmung und Atem-Kreislauf-Stillstand
+ Blutdruckabfall
+ Verletzungen

## Komplikationen

+ Atem-Kreislauf-Stillstand
+ Schnappatmung
+ Hypoxische Krämpfe

## Maßnahmen

+ Bergung aus dem Wasser
+ Atemwege frei machen
+ Sauerstoffgabe
+ Nasse Kleidung entfernen
+ Wärmeerhalt
+ Leicht erhöhter Oberkörper
+ Absaugbereitschaft
+ Ausgedehnte Reanimation bei Hypothermie
+ CAVE: HWS Trauma, Hypothermie

## Anamnese

+ Unfallhergang beachten, uU voran gegangenes Ereignis zb Herzinfarkt