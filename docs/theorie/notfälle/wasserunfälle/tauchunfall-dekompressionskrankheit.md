title:          Dekompressionskrankheit
des:            Verstauchung
date:           2019/01/06
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Notfälle Wasserunfälle
md5:            3c184ea1124ac83cc1098bccf4297199
date_modified:  2019/05/10

## Definition

+ Durch den erhöhten atmosphärischen Druck unter Wasser wird verstärkt N2 (Stickstoff) im Blut gebunden.
+ Bei langsamer Drucksenkung wird dieser Stickstoff wieder frei und wird abgeatmet.
+ Bei schnellem Druckabfall bzw zu langer Tiefenzeit perlt der Stickstoff ins Gewebe, Gefäße, Knochen etc.
+ => Gefäßverschlüssen (Gasembolien), Rupturen, Nervenirritationen

## Symptome

+ Milde Form DCS I
    + Müdigkeit, Schwächegefühl
    + Hautjucken (Taucherflöhe)
    + Hautrötungen
    + Muskel und Gelenksschmerzen

+ Schwere Form DCS II
    + Alle DCS I Anzeichen
    + Taubheitsgefühl, Kribbeln, Gefühllosigkeit
    + Lähmungen, Krämpfe
    + Atemnot
    + Seh-, Hör- und Sprachstörungen
    + Schwindel, Übelkeit
    + Schmerzen hinter dem Brustbein
    + Bewusstseinsstörungen

## Komplikationen

+ Atem-Kreislauf-Stillstand

## Maßnahmen

+ AMLS
+ Sauerstoff
+ Hyperbare Oxygenierung in der Druckkammer