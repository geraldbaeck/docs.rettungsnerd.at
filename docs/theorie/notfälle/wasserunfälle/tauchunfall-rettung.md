title:          Tauchunfall - Rettung
des:            Verstauchung
date:           2019/01/06
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Notfälle Wasserunfälle
md5:            798d1f4f0d8c551f86ce066c215d3f42
date_modified:  2019/06/27

## Maßnahmen

+ Bleigurt entfernen
+ Tarierweste entfernen
+ Tauchcomputer sicheren
+ Flache Lagerung auf weichen Untergrund, Druckstellen vermeiden
+ Maximale Sauerstoffgabe
+ Anleitung zur Atmung
+ Wärmeerhalt
+ Flüssigkeit zum Trinken anbieten
+ evt Deko Kammer rechtzeitig avisieren

## Anamnese

+ Tauchcomputer sichern
+ Alle in den letzten 24h durchgeführten Tauchgänge ermitteln (Nullzeitüberschreitungen, Dekostopps, Tauchtiefen, Tauchdauer)