title:          Tauchunfall - Barotrauma
des:            Verstauchung
date:           2019/01/06
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Notfälle Wasserunfälle
md5:            962128563338dc501ed68bf9dcf05cf9
date_modified:  2019/06/09

## Definition

Gasgefüllte Körperhöhlen können durch entstehenden Überdruck beim zu schnellen Auftauchen geschädigt werden.
Besonders gefährdet sind:

+ Paukenhöhlen im Mittelohr (häufig)
+ Nasennebenhöhlen
+ Lunge
+ Zähne
+ Magen, Darm (selten)
+ Haut (sehr selten)
+ Auge (sehr selten, eher harmlos)

## Ursachen

+ Zu schnelles Auftauchen (10m tiefe herrscht doppelter Druck)
+ selten Fliegen
+ selten maschinelle Beatmung
+ Zähne: nur möglich bei Karies oder schlechten Füllungen
+ Haut: nur mit Trockentauchanzug möglich (Anzug-Squeeze)
+ Auge: Unterdruck in der Tauchmaske

## Symptome und Komplikationen

+ Paukenhöhlen:
    + Trommelfellruptur
+ Lunge:
    + Lungenruptur
    + Pleuraverletzung => Spannungspneumothorax
    + [Arterielle Lungenembolie](tauchunfall-arterielle-gasembolie.md)
+ Zähne:
    + starke Schmerzen
    + Schmelzruptur
+ Haut:
    + Quetschungen
    + Hämatome
+ Auge:
    + Augenschmerzen