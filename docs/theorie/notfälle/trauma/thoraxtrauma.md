title:          Thoraxtrauma
date:           2019/01/06
version:        0.0.7
authors:        gerald@baeck.at
ankikat:        Notfälle Trauma
md5:            6854c64898371b28efb6811e89226ed2
date_modified:  2019/06/02

## Definition

+ entsteht durch Gewalteinwirkung auf den Brustkorb
+ mit Verletzung des knöchernen Thorax
+ oder der ihm umgebenen Organe

Verletzungen des Brustkorbs können in

+ **stumpfe**
+ oder **penetrierende** (offene) Traumata eingeteilt werden

## Unfallmechanismen

+ stumpf
    + Aufprall aufs Lenkrad
    + Einklemmung oder Verschüttung
    + Sturz aus großer Höhe
    + Überrolltrauma
+ penetrierend
    + Messerstich
    + Schusswunde
    + Pfählung

## Leitsymptome

+ A
    + Atemwegsobstruktion
+ B
    + obere Einflussstauung
    + atemabhängige Schmerzen
    + Hämoptyse
    + Dyspnoe
    + Paradoxe Atmung
    + Hautemphysem
    + Thoraxkompressionsschmerz
+ C
    + Tachykardie mit Blutdruckabfall
    + komplexe Herzrhythmusstörungen
    + Kreislaufdepression
    + Gasaustauschstörung (periphere Sauerstoffsättigung)
    + Blutdruck- u./o.Pulsdifferenzen an den Extremitäten
+ E
    + Prellmarken
    + Rippenfrakturen

## Verletzungsmuster

+ Brustwand
    + Brustkorbprellung bzw. –quetschung mit Weichteilverletzungen
    + (Serien-)Rippenbrüchen
        + häufigste Thoraxverletzung
        + Meistens 4.- 8. Rippe lateral
        + Verletzungen der Leber und Milz möglich
        + häufigste Beschwerden: Schmerzen und Atemnot
+ Pleura
    + Pneumothorax
    + Hämatothorax
    + Spannungspneumothorax
    + Mediastinalflattern
        + bei einem offenen Pneumothorax
        + durch Druckschwankungen in der Brusthöhle
        + => bewegt sich das Mediastinum beim Ein- und Ausatmen
        + => kann zu Atem- und Kreislaufbeschwerden führen
+ Lunge
    + Lungenkontusion
    + Trachea- und Bronchusverletzungen
    + Mediastinalemphysem = Luft im Mediastinum
+ Kardiovaskulär
    + Herzerschütterung (Commotio cordis)
    + Herzprellung (Contusio cordis), Herzquetschung (Compressio cordis)
        + Verletzungen am Endokard => Reizleitungsstörung
        + Ruptur Herzkranzgefäße
        + Verletzung des Epikards
        + => Herzbeuteltamponade

## Deadly Six

+ Instabiler (floating) Thorax
+ Massiver Hämatothorax
+ Spannungspneumothorax
+ Offener Pneumothorax
+ Herzbeuteltamponade
+ Atemwegsobstruktion

## Hämatothorax

+ Frakturen (Rippenbrüche) od Gefäßrupturen führen zu
+ Eindringen von Blut in den Pleuraspalt
+ => Thoraxdrainage

## Pneumothorax

+ Eintritt von Luft in den Pleuraspalt
+ führt zum Kollabieren eines Lungenflügels

## Spannungspneumothorax

+ Pneumothorax, bei dem die Luft nicht mehr entweichen kann (Ventilmechanismus)
+ => bei Inspiration strömt Luft nach
+ => Kompression der Restlunge
+ => Abknicken/-drücken der Hohlvenen (Cava-Kompressionssyndrom) mit Füllungsbehinderung des Herzens
+ => akute Lebensgefahr
+ Maßnahmen: Chest Seal, 3 Seitenverband, Thoraxpunktion

## Pneu vs Spannungspneu

| | Pneumothorax | Spannungspneumothorax |
| --- | --- | --- |
| Verletzungsmuster | Stumpf oder penetrierend | Stumpf oder penetrierend |
| Trachea | fast immer median | oft verlagert |
| Atemgeräusch der betroffenen Seite | abgeschwächt oder fehlend | fehlend |
| Hautemphysem | eventuell | meistens |
| Atemnot | leicht | massiv |
| Halsvenen | normal | gestaut |
| RR | - | sinkend |
| HF | - | steigend |

## Komplikationen

+ hochgradige Atemnot
+ Schock
+ Polytrauma?

## Management nach PHTLS

+ GI
    + Sauerstoff
+ A
    + Trachea verlegt?
+ B
    + gestaute Halsvenen?
    + Trachea mittelständig?
    + hebt und senkt beidseitig?
    + beidseits belüftet?
    + AF
    + Maßname: Sauerstoff
+ C
    + Puls peripher & zentral
    + Hautkolorit
    + Rekap peripher & zentral
    + Abdomen oB?
    + große Blutungsräume
    + Maßnahme: Volumen
+ E
    + Begleitverletzungen

+ Immobilisierung
    + nur bei stumpfem Trauma
    + OK hoch
    + ggf auf verletzte Seite

+ Transport
    + Lange Transportzeit ev. Intubieren
    + Volumengabe
    + evtl. Entlastungspunktion bei Verschlechterung
    + Auf Spannungspneu vorbereitet sein
    + Rasch ad. Schockraum
    + evtl. NAH

## Maßnahmen

+ Lagerung mit erh. Oberkörper, Stabilisierung durch Handauflegen
+ evt. Seitenlage auf der verletzten Seite
+ Pat. beruhigen zu ruhiger Atmung verhelfen
+ SPO&#8322;-Gabe
+ Absaugbereitschaft