title:          Luxation
des:            Verrenkung
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Trauma
md5:            8683bda40bba6547df9c49c2e3c615b6
date_modified:  2019/01/07

## Definition  
Wenn die gelenkbildenden Knochen durch Krafteinwirkung ihren Zusammenhalt verlieren und in dieser abnormen Stellung bleiben.

## Symptome  
+ Schmerzen
+ Schwellung
+ Bluterguss (Hämatom)
+ Abnorme Stellung des Gelenks
+ Störung der Motorik, Durchblutung, Sensibilität

## Komplikationen  
+ Überdehen und Zerreißen der Gelenkskapsel und der Bänder
+ Blutung ins Gelenk

## Maßnahmen  
+ MDS-Kontrolle
    + Motorik
    + Durchblutung
    + Sensibilität
+ ruhig stellen