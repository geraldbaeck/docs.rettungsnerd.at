title:          Wirbelsäulentrauma
des:            Quetschung, Prellung
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Trauma
md5:            284371d67bd444d6e41ba9bc7c83bee3
date_modified:  2019/01/07

## Definition
Stürze (ab ca 1,5m) und Schleudervorgänge  

+ Verletzungen der Wirbelsäule
+ zb Wirbelkörperbrüche, Verrenkungen od beides (Luxationsfraktur)
+ dabei kann es zur Schädigung des Rückenmarks kommen
+ Lähmung, Querschnittslähmung

Besonders gefährdet:  

+ ZweiradfahrerInnen
+ aus dem Fahrzeug geschleudert
+ nach Auffahrunfällen
+ Sturz aus mind. 1,5m Höhe
+ Kopfsprung in seichtes Wasser
+ Kletterunfall
+ mit SHT, Polytrauma

## Ursachen  
+ direktes Trauma (Stich, Schuss)
+ indirektes Trauma durch Zug - und Scherkräfte => Schleudertrauma
+ pathologische Frakturen (zb bei Tumor, Metstasen)

## Symptome  
+ Schmerzen im Bereich der Wirbelsäule
+ Kraftlosigkeit bis zur Bewegungsunfähigkeit
+ Taubheitsgefühl bis zur Empfindungslosigkeit
+ unwillkürlicher Harn- und Stuhlabgang
+ Atemstörungen bei hohem Querschnitt (C3 - C5)

## Komplikationen  
+ Je nach Höhe der Querschnittslähmung (zB Ausfall der Atmung)
+ Schock

## Maßnahmen  
+ HWS-Schienung
+ Schaufeltrage
+ Vakuummatratze
+ traumatologischer Notfallcheck
+ Absaugbereitschaft
+ allg. Maßnahmen