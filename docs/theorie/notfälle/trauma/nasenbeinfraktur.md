title:          Nasenbeinfraktur
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Notfälle Trauma
md5:            e1805186a5f69a1fb5936584dc36eaa6
date_modified:  2019/05/01
date:           2019/05/01

## Definition

+ vorspringender Körperteil ist vermehrt Gewalteinwirkungen ausgesetzt
+ relativ häufige Verletzung
+ häufig auch das knorpelige Nasenseptum gebrochen => Septumhämatom
+ auch die benachbarten Knochenstrukturen könnten betroffen sein

## Symptome

+ Epistaxis
+ Nasenfehlstellung
    + Boxernase
    + eingesunkener Nasenrücken
+ Krepitation (Knirschen)

## Maßnahmen

+ bei geschlossener Fraktur: Transport → HNO
+ bei offener Fraktur: zuerst steril Abdecken
+ benachbarte Strukturen übeprüfen
+ auf SHT überprüfen