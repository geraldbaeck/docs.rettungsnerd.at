title:          Schädel-Hirntrauma (SHT)
date:           2019/01/06
version:        0.0.7
authors:        gerald@baeck.at
ankikat:        Notfälle Trauma
md5:            517d1a89f86af7e8247ad35ecc3a6cd2
date_modified:  2019/06/02

## Definition

+ =Verletzung des Gehirns aufgrund einer äußeren Ursache (Krafteinwirkung)
+ bezieht sich nicht auf ggf. damit verbundene Schädelfrakturen oder Kopfplatzwunden

## Primäre Verletzungen

+ = mechanische Verletzungen des Gehirns
+ als direkte Folge eines Traumas
+ idR irreversibel

## Sekundäre (indirekte) Verletzungen

= entwickeln sich als Folge der Gewalteinwirkung

+ Raumforderungen durch
    + intrakranielle Blutungen (epidural, subdural, subarchnoidal, intrazerebral)
    + Hirnödem
+ Hypoxie durch
    + Atemwegsverlegungen
    + Kreislaufstörungen
    + Ventilationsstörungen
    + Raumforderungen
+ Hypotonie
    + unzureichender zerebraler Blutfluss (CBF)
    + O&#8322;- und Glukosedefizit
+ Apoptose
    + intrazelluläre Suizid Kaskaden als Folge der primären Hirnschädigung

## Einteilung

| Grad | Bezeichnung | auf gscheit | GCS | Bewusstlosigkeit | Beschreibung |
| --- | --- | --- | --- | --- | --- |
| 1 | Gehirnerschütterung | Commotio Cerebri | 12-15 | Sofort einsetzende, sekunden bis Minuten | häufig mit Amnesie verbunden |
| 2 | Gehirnprellung | Contusio Cerebri | 9-12 | > 15 min | Schädigung der Hirnsubstanz durch Beschleunigungseffekte, auch Contre-Coup-Verletzung möglich |
| 3 | Gehirnquetschung | Compressio Cerebri | 3-8 | tlw Tage | durch direkte Verletzung oder Drucksteigerung => Einklemmungen, Cushing Triade |

+ offenes SHT
    + = alle Verletzungen mit Eröffnung der Dura
    + Frakturen der Schädelbasis (=Kontakt zur Außenwelt über pneumatisierte Höhlen!!) Präklinisch nur erkennbar durch Austritt von Blut aus dem Ohr, Liquor oder Hirngewebe.
+ geschlossenes SHT

## Amnesien

+ retrograde Amnesie
    + Gedächtnisverlust für den Zeitraum vor dem Unfall
+ antegrade Amnesie
    + Gedächtnisverlust für den Zeitraum nach dem Unfall
    + Frage "Was ist passiert" wird immer wieder gestellt
+ kongrade Amnesie
    + Gedächtnisverlust für den Zeitpunkt des Ereignisses

## Symptome

+ Leitsymptom: Bewusstseinsstörung
+ Kopfschmerzen
+ Übelkeit
+ Erbrechen
+ Schwindel
+ Sehstörungen, Pupillenveränderungen (seitendifferenz, fehlende Lichtreaktion)
+ absinkende Pulsfrequenz
+ evt. Krämpfe
+ Atemstörungen, Stokes Atmung, Biot Atmung
+ Monokel-/Brillenhämatom
+ [Battle Zeichen](https://de.wikipedia.org/wiki/Battle-Zeichen)

## Komplikationen

+ Bewusstlosigkeit
+ Krämpfe
+ Atem-Kreislauf-Stillstand

## Erstuntersuchung und Management nach PHTLS

+ B
    + Cheyne Stokes Atmung
    + Biot Atmung
+ C
    + Druckpuls
    + Bradykardie
    + Hypertension
+ D
    + Bewusstseinstrübung
    + Sprachstörung
    + Krämpfe
    + Amnesie
    + Schwindel
    + Erbrechen
    + Pupillendifferenz → Zeichen für Blutung/Raumforderung
+ Ganzkörperuntersuchung
    + Blutung aus Ohr/Nase
    + Monokel/Brillenhämatom
    + Battle Zeichen

## Komplikationen

+ Bewusstlosigkeit
+ Atem-Kreislauf Stillstand

## Behandlungsziele

+ Hirndruck senken
+ Hypoxämie verhindern
+ Mitteldruck halten

+ RR stabil 100-120; CAVE bei Va Blutung 80-100
+ SP02 > 94%,
+ Temp. ca. 37°C
+ etC02 30-35 mmHg
+ BZ 80-140 mg/dl

## Maßnahmen

+ Oberkörper 15-30 Grad hochlagern (bei ausreichendem systemischen Druck)
+ Knickung der HWS vermeiden (venöse Abflussbehinderung, Zervikalstütze!!!)
+ Intubation/ Beatmung: Aspirationsschutz
    + Normoventilation (keine massive Hyperventilation)
    + Kontrollierte Beatmung
+ RR stabil halten (100-120 mmHg syst.)
+ Analgosedierung (CAVE: RR Abfall)
+ Schonender Transport (Hubschrauber)