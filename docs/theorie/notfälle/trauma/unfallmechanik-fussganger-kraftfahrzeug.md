title:          Unfallmechanik
date:           2019/01/06
version:        0.0.5
authors:        gerald@baeck.at
md5:            790077d2865c0603218d54b7b29315b9
ankikat:        Notfälle Trauma
date_modified:  2019/04/27

## Definition Unfall

= ein mehrstufiger Vorgang

+ Fahrzeugkollision
+ Körperkollision
+ Organkollision (nicht sichtbar)

Aus dem Ersteindruck an der Unfallstelle kann man Schlüsse ziehen auf die Kinematik des Unfallgeschehens, d.h. welche Kräfte waren am Werk, wie haben sie gewirkt und welche Verletzungen sind zu erwarten.

## Faktoren

+ Geschwindigkeit (sehr wichtig, E = 1/2(m*v²))
+ Gurte ja/nein?
+ weicher Untergrund kann kinetische Energie absorbieren
+ Sturzhöhe
+ Komprimierbarkeit der Kontaktfläche
+ erstkontaktierender Körperteil (Extremität oder Kopf)

## Fußgänger vs Kraftfahrzeug

| Geschwindigkeit | Verletzungsmuster |
| --- | --- |
| bis 50km/h | Kopf auf Kühlerhaube |
| 50-70km/h | Kopf vs Windschutzscheibe |
| ab 70km/h | FußgängerIn fliegt über das Dach |

### Unfallphasen

1. primärer Aufprall
    + **Unterschenkelverletzungen** (Stoßstangenverletzungen)
    + Platz- und Quetschwunden am Schienbein
    + keilförmige Brüche
    + Blutungen häufig sehr ausgedehnt
    + Kleinwüchsige & Kinder: häufig Beckenverletzungen

2. Aufschlagen des Rumpfes und Kopfes am Fahrzeug

3. Abgleiten oder Abwurf vom KFZ
    + häufig eher Armbrüche & Kopfverletzungen
    + auffallend gering