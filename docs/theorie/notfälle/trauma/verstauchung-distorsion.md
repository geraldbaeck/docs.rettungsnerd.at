title:          Distorsion
des:            Verstauchung
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Trauma
md5:            6386ef806ac2bc75435dfdfb75a06a3c
date_modified:  2019/01/07

## Definition
Wenn die gelenkbildenden Knochen durch Krafteinwirkung kurz gegeneinander verschoben werden, aber sofort wieder in ihre ursprüngliche Stellung zurückkehren.

## Symptome
+ Schmerzen
+ Schwellung
+ Bluterguss (Hämatom)

## Komplikationen
+ Einreißen der Gelenkskapsel
+ Zerrung oder Riss der Bänder
+ Blutung ins Gelenk

## Maßnahmen
+ MDS-Kontrolle
    + Motorik
    + Durchblutung
    + Sensibilität
+ hoch lagern
+ ruhig stellen
+ kühle Umschläge