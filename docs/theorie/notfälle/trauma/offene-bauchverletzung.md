title:          Offene Bauchverletzung
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Trauma
md5:            e046a1bf2d7e77e9197b28f70a58ac01
date_modified:  2019/01/07

## Definition
Bauchverletzung in Folge von Gewalteinwirkung von außen durch scharfen oder spitzen Gegenstand. (zb Stich- & Schusswaffen, Pfählungsverletzungen, herausgerissene Sonde)

## durch Stoma, PEG-Sonde & Suprapubischen Katheter
Kommt es zu Verletzungen durch die unsachgemäße Verwendung von künstlichen Bauchausgängen, ist prinzipiell wie bei einem offenen Bauchtrauma vorzugehen.

+ Stoma (künstliche Öffnung des Verdauungstraktes)
+ PEG-Sonde (künstliche Ernährung durch die Bauchdecke)
+ Suprapubischer Katheter (Harnableitung über die Bauchdecke)

## Symptome
+ Wunde im Bereich der Bauchdecke
+ evt. Austreten von Darmschlingen
+ starke Schmerzen

## Komplikationen
+ starke Blutung
+ Einriss/Perforation eines Bauchorgans
+ Gefäßverletzung oder Verletzung des Mesenteriums (Gekröse)

## Maßnahmen
+ keimfreie Wundversorgung
+ evt ausgetretene Eingeweide belassen
    + mit sterilen Wundauflagen bedecken
    + mit Ringer-Lösung vor Austrocknung schützen
    + zusätzlich Rettungsdecke
+ Sonden belassen, evt ausgetreten Körpersubstanzen vorsichtig entfernen
+ Gegenstände in Wunde belassen und fixieren
+ Flachlagerung auf Vakuummatratze mit angezogenen Beinen oder nach Wunsch des Patienten