title:          Kontusion
des:            Quetschung, Prellung
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Trauma
md5:            f5152aed7b7b68c8695e4895e3df683d
date_modified:  2019/01/07

## Definition  
+ Schädigung durch Gewalt von außen
+ muss nicht mit sichtbaren Hautverletzungen einhergehen
+ Folge sind Ödeme des Gewebes und Blutaustritt aus beschädigten Kapillaren in das umliegende Gewebe
+ = Bluterguss oder Prellmarke
+ mit Schmerzen und Schwellung der betroffenen Region verbunden

## Symptome  
+ Schmerzen
+ Schwellung
+ Hämatom

## Komplikationen  
+ Blutverlust ins Gewebe

## Maßnahmen  
+ hoch lagern
+ ruhig stellen
+ kühle Umschläge