title:          Stromunfall
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Notfälle Trauma
md5:            c57c99ef1ef6cd747f20acd6e8124136
date_modified:  2019/05/09
date:           2019/04/22

## Definition

+ = Verletzung durch elektrischen Strom
+ = Unfall mit Elektrogeräten oder auch ein Blitzschlag

+ Hochspannung: >1000V zB Baustellen, öBB, Oberleitung, Blitzschlag (bis zu 300kV) => **Lichtbogengefahr**
+ Niederspannung: <1000V (häuslicher, gewerblicher Bereich)

+ Schädigung entsteht durch
    + Reizeffekt auf Muskeln, Nerven und Myokard
    + hohe Wärmeentwicklung

## Symptome

+ Elektrophysiologische Reizwirkung
    + schwere Rhythmusstörungen bis Asystolie
    + Kammerflimmern
    + Muskelkontraktionen, Muskel- und Bänderrisse
    + Frakturen durch unkontrollierte Muskelkontraktion
    + Gefäßspasmen
    + Sensibilitätsstörungen
+ Elektrothermische Reizwirkung
    + Verbrennungen
    + Strommarken
    + Verletzung der inneren Organe
    + Verblitzen der Augen
+ Traumata durch Wegschleudern
+ Niederspannungsunfall: Alles oder Nichts Ereignis => keine später auftretenden Symptome zu erwarten

## Rettung

+ Eigenschutz
+ bei Niederspannung
    + isolierten Standort suchen
    + Gerät/Netz abschalten
    + Sicherung entfernen
    + [Schrittspannung](https://de.wikipedia.org/wiki/Schrittspannung) beachten
    + Trennung des Verunglückten kann mit einem isolierten Gegenstand erfolgen (zb Stange, Leiter)
+ bei Hochspannung
    + Warten!!!
    + Abstand halten 1cm/1000V, Lichtbogen!!!
    + Spezialkräfte anfordern
    + Freigabe abwarten

## Management nach PHTLS

+ SSS
    + Eigenschutz
    + Spezialkräfte?
    + Hoch-/Niederspannung?
+ GI
    + Warten! genau umsehen
    + Eigenschutz
    + Unfallmechanismus
    + Hoch-/Niederspannung?
    + Lichtbogenverletzung?
    + Rettung nötig?
    + große Verbrennungen sichtbar?
    + Maßnahme: große Verbrennungen zudecken, Sauerstoff
+ AB
+ C
    + HF, rhythmisch?
    + HF schnell/Langsam
    + EKG ist Pflicht
    + Maßnahme: Pads kleben, iV
+ D
    + Bewusstseinslage und Orientierung genau prüfen
    + Pupillen
+ E
    + Hauptbeschwerde: kardial, thermisch, neurologisch
    + Wärmeerhalt
    + Wo war der Lichtbogen?

+ Transport
    + ggf. unter Reanimationsbereitschaft
    + Monitoring
    + ggf. NA