title:          Stumpfe Bauchverletzung
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Trauma
md5:            1e074f302845d35d225c8a2a7cede8de
date_modified:  2019/01/07

## Definition  
Durch stumpfe Gewalteinwirkung, Verletzung von Milz, Leber, Niere, Darm, Zwerchfell, Blase. Aber auch Beckenbruch

## Symptome  
+ starke Bauchschmerzen
+ Prellmarken
+ Bretthart gespannt Bauchdecke, Abwehrspannung
+ Übelkeit, Erbrechen

## Komplikationen  
+ innere Blutung
+ Einriss/Perforation eines Bauchorgans
+ Prellung eines Organs (Kontusion)
+ Gefäßverletzung oder Verletzung des Mesenteriums (Gekröse)

## Maßnahmen  
+ vorsichtige Flachlagerung auf Vakuummatratze mit angezogenen Beinen oder Lagerung nach Wunsch des Patienten
+ allg. Maßnahmen