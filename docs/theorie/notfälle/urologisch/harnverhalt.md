title:          Akuter Harnverhalt
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Notfälle Urologisch
md5:            d7b59f2a8300aa9260a632d25d5c4e3d
date_modified:  2019/05/08
date:           2019/05/08

## Definition

= Abflusshindernis im Bereich der Blase oder Harnröhre

## Ursachen

+ vergrößerte Prostata
+ eingeklemmte Steine
+ Fremdkörper
+ Verletzungen
+ Tumore

## Symptome

+ schmerzhafter, starker Harndrang
+ übervolle Blase
+ erschwertes/unmögliches Harnlassen

## Komplikationen

+ Überlaufblase

## Maßnahmen

+ Katheder