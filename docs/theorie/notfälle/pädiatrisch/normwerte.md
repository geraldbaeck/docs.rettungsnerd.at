title:          Normwerte
date:           2018/12/28
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Notfälle Kinder
md5:            e44ef10de40d34d6e5a4407341168afe
date_modified:  2019/07/03

| Alter | AF | AZV | HF | RR Systole |
| --- | --- | --- | --- | --- |
| `Neugeborenes`| 40-60 | 20-40 | 120 | - |
| `Säugling` | 20-30 | 50-100 | 120 | 60 |
| `Kind` | 15-20 | 100-200 | 100 | 90 |
| `Erwachsener` | 12-15 | 500 | 80 | 120 |

## Formeln

+ Gewicht: (Alter+4)*2
+ Tubus: Alter/4 + 4
+ Volumen: 20ml/kg KG
+ Blutmenge: 6-8% des KG