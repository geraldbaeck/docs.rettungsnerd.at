title:          Atmungsorgane
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Kinder
md5:            4bf92a2502daae08b47cd183d41a881c
date_modified:  2019/01/07

## Atmungsorgane
+ Nasengänge relativ eng
+ ebenso die tieferen Luftwege (Kehlkopf, Luftröhre, Bronchien)
+ Schwellungen können dadurch die Atemwege sehr schnell verengen/schließen
+ Zunge ist relativ groß, Speichelfluss stark
+ Der Kehlkopf liegt höher und ist gekippt => höhere Atemnotswahrscheinlichkeit