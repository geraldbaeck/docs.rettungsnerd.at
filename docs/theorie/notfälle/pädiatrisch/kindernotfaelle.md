title:          Kindernotfälle
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Notfälle Kinder
md5:            af82b5751c8fb51e8c9ad622864eaa9d
date_modified:  2019/04/28
date:           2019/04/28

## Besonderheiten

+ Kindernotfälle sind selten!
+ Kinder sind keine kleinen Erwachsenen!
+ Laut schreiende, laufende Kinder sind in der Regel nicht schwer krank
+ Untersuchung des Kindes auf dem Arm/Schoß der Mutter/des Vaters/der Bezugsperson
+ Auf Augenhöhe des Kindes gehen
+ Eltern / Angehörige beruhigen

## ABCDE

+ GI
    + Pädiatrisches Dreieck
        + Äußeres Erscheinungsbild
            + Muskeltonus
            + Bewusstsein (Fokussieren, Sprechen, Weinen)
            + "Allgemeinzustand" (Trinkverhalten, Bewegungsfreude, Spielverhalten)
        + Atmung
            + Atemgeräusche (Stridor, Giemen, Stöhnen, Husten?)
            + Atmungsarbeit (Einziehungen, Nasenflügeln?)
        + Kreislauf
            + Hautfarbe (Marmorierungen?)
    + Anschließend Entscheidung
        + kritisch: Kopf bis Fuß Untersuchung
        + nicht kritisch: Fuß bis Kopf

+ Was nehme ich wahr?
    + Sehen
        + Schockzeichen
        + Anstrengung durch Dyspnoe
        + Hautfarbe
    + Hören
        + Dyspnoe
        + Husten
        + Schreien
    + Tasten
        + Abdomen
        + Haut
+ Anamnese!