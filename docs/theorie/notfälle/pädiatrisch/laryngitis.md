title:          Laryngitis
date:           2019/01/06
version:        0.0.6
authors:        gerald@baeck.at
ankikat:        Notfälle Kinder
md5:            98c3601d54c2b23445db7b915d792408
date_modified:  2019/07/03

## Definition

= virale entzündliche Erkrankung des Kehlkopfes

## Symptome

+ Heiserkeit bis zur Stimmlosigkeit
+ Schmerzen
+ Hustenreiz
+ typischer bellender Husten
+ meist in den Nachstunden u. sehr plötzlich

## Maßnahmen

+ Kalte-feuchte Luft
+ Beruhigen (Kind & Eltern)
+ Transport unter Beobachtung der Vitalwerte
+ evtl. 02-Gabe
+ NA (Kortison- Zäpfchen, Adrenalin ü. Verneblermaske)

## Komplikationen

+ Bewusstlosigkeit
+ Ersticken
+ Atem-Kreislauf-Stillstand

[Krupp Hörbeispiel](https://www.youtube.com/embed/Qbn1Zw5CTbA?list=PLhC2h1cbsdE1TUa2uDimZ--EWI9g-eeOm){: .youtube}