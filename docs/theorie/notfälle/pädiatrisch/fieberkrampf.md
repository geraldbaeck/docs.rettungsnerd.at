title:          Fieberkrampf
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Kinder
md5:            1158c2c9b711d6211ab47b5cf6866d78
date_modified:  2019/05/08
date:           2019/04/28

## Definition

= in Verbindung mit einer fieberhaften Erkrankung auftretender epileptischer Anfall

+ Häufiges Anfallsereignis bei Kindern zw. 6 Monate - 5. Lebensjahr
+ Meist zu Beginn eines Infektes
+ Ausgelöst durch rasches Anfiebern gr. 38°C

## Symptome

+ Infektzeichen, Temp gr. 38°C
+ Tonisch-Klonischer Anfall mit Bewusstseinsverlust
+ Beim Eintreffen meist schläfrig/unauffällig

+ Unkomplizierter (einfacher) Fieberkrampf
    + Dauer < 15min
    + keine Wiederholung innerhalb 24h
    + keine Herdzeichen
+ Komplizierter (atypischer) Fieberkrampf
    + Dauer > 15min oder
    + Wiederholung innerhalb von 24h oder
    + herdförmig (= bei dem Anfall erlischt nicht von Beginn an das Bewusstsein, es sind nur Teile des Körpers betroffen) oder

## Maßnahmen

+ Eltern beruhigen
+ Fieber senken
+ Hospitalisierung