title:          Epiglottitis
date:           2019/01/06
version:        0.0.6
authors:        gerald@baeck.at
ankikat:        Notfälle Kinder
md5:            c6520258bb9f8ad61301c2389370ade0
date_modified:  2019/07/03

## Definition

= bakterielle Entzündung des Kehlkopfdeckels

## Symptome

+ wirkt schwer Krank
+ hohes Fieber
+ Speichelfluss aus Mund

## Maßnahmen

+ keine Manipulation im Rachenraum
+ Beruhigen
+ Transport unter Beobachtung der Vitalwerte
+ 02-Gabe
+ "Load and Go" in Reanimationsbereitschaft
+ bei Vitaler Bedrohung NA

## Komplikationen

+ Bewusstlosigkeit
+ Ersticken
+ Atem-Kreislauf-Stillstand

[Krupp Hörbeispiel](https://www.youtube.com/embed/Qbn1Zw5CTbA?list=PLhC2h1cbsdE1TUa2uDimZ--EWI9g-eeOm){: .youtube}