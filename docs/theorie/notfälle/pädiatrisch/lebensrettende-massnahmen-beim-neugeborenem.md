title:          Lebensrettende Maßnahmen beim Neugeborenem
date:           2019/01/06
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Notfälle Kinder
md5:            ca6a60d22241eb311afe8b0ec6379d69
date_modified:  2019/06/27

![Säuglingsreanimation](https://rawgit.com/geraldbaeck/RS_WRK/master/charts/Lebensrettende_Massnahmen_beim_Saeugling.svg)Reanimationsalgorithmus bei Neugeborenen[^1]
{: .imagewithcaption}

[^1]: [Gerald Bäck](mailto:gerald@baeck.at) with a [CC0-1.0](https://creativecommons.org/licenses/by-sa/2.0/at/deed.de) public domain dedication = no copyright