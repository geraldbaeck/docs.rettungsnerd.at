title:          Medikamentenvergiftung
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle toxikologisch
md5:            8cced11d724cb47979a39fec4e490d60
date_modified:  2019/05/16
date:           2019/05/10

+ häufigste Vergiftungsursache
+ hauptsächlich durch Schlaf- u. Beruhigungsmittel

## Ursachen

+ Vergiftung durch Unachtsamkeit (unbekannte Arznei u. Dosis)
+ gemeinsam mit Alkohol
+ oder in suizidaler Absicht

## Maßnahmen

+ Vitalwerte, Monitoring, 02
+ bei B-los - Lagerung
+ Medikamente mitnehmen
+ ggf. Info von Vergiftungszentrale [+43 (1) 406 43 43](tel:+4314064343)