title:          Vergiftungen
date:           2019/01/06
version:        0.0.7
authors:        gerald@baeck.at
ankikat:        Notfälle toxikologisch
md5:            92134dc52909c9165337e4ac748d818f
date_modified:  2019/06/02

## Definition

+ Krankheitsbilder, die durch dosisabhängige Einwirkung chemischer Stoffe auf den Organismus entstehen
+ Verlauf und Schweregrad abhängig von
    + **Substanz**,
    + **Dosis** und
    + **Aufnahmeweg**

## Aufnahmewege

+ **Ingestion**: über Verdauungstrakt
+ **Inhalation**: über Lunge
+ **Perkutan**: über Haut
+ **Parenteral**: durch Injektion
+ **Kombiniert**: über Haut, Lunge, Magen-Darm-Trakt

## Ursachen

+ Unfälle (Verwechslungen, Fehlanwendungen)
+ Absicht (kriminell, Suizid, Rausch)
+ Störfälle in der Industrie, Transportunfälle

## Symptome

| Organsystem | Symptome |
| --- | --- |
| ZNS | Krämpfe, Sprachstörungen, Rausch, Unruhe, Verwirrtheit, Pupillenweite |
| Atmung | Atemlähmung, Atemdepression, Lungenödem |
| Herz-Kreislauf | Rhythmusstörungen, Kreislaufstillstand, Schock |
| Abdomen | Übelkeit, Erbrechen, Durchfall, Bauchkrämpfe |
| Haut | Verätzungen, Verbrennungen, Hautablösung, Rötung |

## Komplikationen

+ Atem-Kreislaufstillstand

## Maßnahmen

+ Eigenschutz beachten => Hygienepaket, GAMS-Regel
+ Prophylaktische Seitenlage
+ Absaugbereitschaft
+ Giftanamnese
+ Erhalt der Vitalfunktionen, i.V. "Verdünnen"

### A-Regel nach Brockstedt

+ Aufrechterhaltung der Vitalfunktionen, Aspirationsprophylaxe
+ Aqua, Abspülen, Verdünnen bei äußeren Verletzungen (**nicht zum Trinken auffordern**)
+ Anamnese
+ Asservieren
+ Anrufen (VIZ, [+43 1 4064343](tel:+4314064343))
+ Aufschreiben
+ <del>Antidotgabe</del> nicht erlaubt für NFS
+ <del>Aktivkohle</del> nicht erlaubt für NFS

### Anamnese

+ WER? Alter, Geschlecht, Gewicht, Zustand
+ WAS? Bezeichnung des Stoffs, Produkts od. Pflanzenteils
+ WIE VIEL? möglichst exakte Menge
+ WIE? Aufnahmeweg
+ WANN?
+ WARUM? Selbst- oder Fremdschädigung, Fehlanwendung, Unfall etc