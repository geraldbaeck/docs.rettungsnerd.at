title:          Vergiftung durch Rauch- und Reizgase
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle toxikologisch
md5:            748d5b23329969021a9c13da0436286c
date_modified:  2019/01/07

## Definition
Je nach Gastypus können sich die Symptome mit Verzögerung einstellen. Reizgase können die Lungenbläschen schädigen und zu einem toxischem Lungenödem führen.

## Wichtige Gase
+ Brandgase
+ Reizgase wie Tränengas und chemische Kampfstoffe, CS- und CN-Gas (Augenzeizstoffe)
+ Ammoniak
+ saure Gase (Enstehen beim Verdampefen von Säuren)
+ Nitrosegase - Stickoxide zb Salpetersäure in Düngemittel
+ Halogene
+ Schwefelwasserstoff
+ Phosgen - entseht beim Verbrennen von PVC oder beim Schweißen

## Symptome
+ Hustenreiz
+ Tränenfluss
+ Schmerzen im Brustbereich
+ Atemnot
+ Zyanose
+ Schweiß
+ erhöhter Puls und Blutdruckabfall
+ Rasselnde Atemgeräusche

## Komplikationen
+ Atemwegsobstruktion
+ Toxisches Lungenödem

## Maßnahmen
+ Luftzufuhr
+ Rettung aus Gefahrenbereich (Eigenschutz)
+ Maximale Sauerstoffgabe
+ Absaugbereitschaft