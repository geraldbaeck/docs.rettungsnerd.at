title:          CO₂ Vergiftung
date:           2019/01/06
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Notfälle toxikologisch
md5:            c811b939654b9cbaa4f25b4ea5c23659
date_modified:  2019/05/10

## Definition

+ farblos
+ schwerer als Luft, säuerlich riechend
+ entsteht - wie bei menschlicher Atmung - als Stoffwechselendprodukt bei organischen Gärvorgängen
+ Schädigung direkt durch Beeinträchtigung des Atemzentrums

Vorkommen:

+ Weinkeller
+ Höhlen
+ Brunnenschächte
+ Silos

## Symptome

+ Kopfschmerzen
+ Schwindel
+ Bewusstseinsstörung
+ Atemstörung bis Atemstillstand
+ Zyanose

## Komplikationen

+ Bewusstlosigkeit
+ Krämpfe
+ Atem-Kreislauf-Stillstand

## Maßnahmen

+ Luftzufuhr
+ Rettung aus Gefahrenbereich (Eigenschutz)
+ Maximale Sauerstoffgabe
+ Absaugbereitschaft