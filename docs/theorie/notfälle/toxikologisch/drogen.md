title:          Drogenintoxikation
date:           2019/01/06
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Notfälle toxikologisch
md5:            a3b1a94b895d4f9a9bd255db00c519db
date_modified:  2019/05/30

## Definition

+ vielfältiges Angebot => klinische Toxikologie für einzelne Drogen fast unmöglich
+ Symptome können vielfältig und uncharakteristisch sein

Letztlich muss sich der Rettungsdienst anhand des Zustands des Patienten, der Umgebung und Situation und durch Fremdanamnese ein Bild machen.

## Symptome

+ Uppers
    + Tachykardie, Hypertonie, Tremor
    + Agitation, Rhythmusstörung
    + Gefäßspasmen, Organinfarkte
    + Krampfanfälle
+ Downers
    + Kreislaufdepression
    + Bewusstseinstrübung
    + Koma, Atemstillstand
+ Halluzinogene
    + Realitätsverlust
    + Panikattacken
    + Horrortrips, Psychosen

## Komplikationen

+ Bewusstlosigkeit
+ Atemstörungen
+ Blutdruckanstieg
+ Hirnblutungen
+ Atem-Kreislauf-Stillstand