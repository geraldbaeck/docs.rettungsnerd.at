title:          Vergiftung durch Pflanzenteile und Pilze
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle toxikologisch
md5:            0a13b2705314dfa280cddafd68bd06ab
date_modified:  2019/01/07

## Definition
Oft unbeachsichtigte Einnahme durch Verwechlung zb:

+ Herbstzeitlosenblätter
+ Tollkirsche
+ Goldregensamen
+ Knollenblätterpilz

Selten Absicht:

+ Fliegenpilz

## Symptome
+ Übelkeit, Erbrechen, Durchfall