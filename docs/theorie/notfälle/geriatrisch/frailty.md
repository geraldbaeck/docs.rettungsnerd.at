title:          Frailty
date:           2018/12/28
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Notfälle Geriatrisch
md5:            46a552d53561e3146e0c4f961b62e4a7
date_modified:  2019/01/07

Umfassender Begriff für im Alter abnehmende Leistungsbreite und Belastbarkeit.

Warnsignale:

+ allg. Müdigkeit
+ Antriebslosigkeit
+ Muskelschwäche
+ geringere Gehgeschwindkeit
+ unbeabsichtiger Gewichtsverlust

Erscheinungsbild = Gebrechlichkeit
{: .info}