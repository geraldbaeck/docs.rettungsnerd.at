title:          Geriatrische Erkrankungen
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Geriatrisch
md5:            fdc3ab68807d3e847a0057b49f82ea34
date_modified:  2019/01/07

## Demenz
+ fortschreitende Krankheit des Gehirns
+ Störung kortikaler Funktionen (Gedächtnis, Denken, Orientierung, Auffassung,....)
+ keine Bewusstseinstrübung

## Delir
Verlust der Fähigkeit, mit der üblichen Klarheit und Struktur zu denken (=Verwirrtheit)

+ Verwirrtheit
+ Orientierungsunfähigkeit
+ unangemessene Reaktionen auf Umwelt
+ Unterscheidung mit Demenz schwierig

## Mangelernährung

## Sarkophenie
Abbau von Muskelmasse und Kraftverlust

## Sturzneigung