title:          Arterielle Embolie
date:           2019/01/06
version:        0.0.6
authors:        gerald@baeck.at
ankikat:        Notfälle Kardiovaskulär
md5:            710bf90312b087d87b736826bdff407b
date_modified:  2019/05/01

## Definition

+ = Verschleppte Thromben verstopfen eine zuführende Arterie
+ meist aus dem linken Herzen verursacht durch
    + Herzrhythmusstörungen
    + Vorhofflimmern
    + Mitralklappenfehler
    + Entzündungen

### Chronisch

+ durch Entstehung eines Plaques bei Arteriosklerose
+ => Aushungern des Organs:
    + Koronare Herzkrankheit => Angina Pectoris
    + Gehirn => CVI Cerebralsklerose
    + paVk => Schaufensterkrankheit ([Claudicatio intermittens](https://de.wikipedia.org/wiki/Claudicatio_intermittens)) => [Gangrän](https://de.wikipedia.org/wiki/Gangr%C3%A4n) (Gewebs-Nekrose)

### Akut

führt zum raschen und schmerzhaften Gewebsuntergang

+ Herzinfarkt
+ Mesenterialinfarkt => nekrotischer Darm
+ Schlaganfall
+ Akuter Peripherer Gefäßverschluss

## Ursachen

+ Vorhofflimmern
+ künstliche Herzklappen
+ Periphere arterielle Verschlusskrankheit (pAVK)
+ Verletzungen / Operationen

## Symptome

"6 x P" nach Pratt (an der betroffenen Extremität):

+ pain: plötzliche Schmerzen (evt Peitschenschlagartig. bei Belastung verstärkt)
+ paleness: Blässe, kühle Haut
+ paranthesia: Gefühls- und Bewegungsstörungen
+ paralysia: Lähmungserscheinungen
+ pulselessness
+ (prostration: evt. Schocksymptomatik)

+ evt Schmerzlinderung bei Tieflagerung

## Komplikationen

+ Verlust der Gliedmaße

## Maßnahmen

+ Tieflagerung der betroffenen Gliedmaße
+ Weichlagern
+ warm halten
+ Extremität weich lagern (sonst Mangeldurchblutung/Druckschäden)

## Zu beachten

Patient muss für wahrscheinliche OP nüchtern bleiben.