title:          Venenthrombose
date:           2019/01/06
version:        0.0.5
authors:        gerald@baeck.at
ankikat:        Notfälle Kardiovaskulär
md5:            8c8d0b705ec340a5780c2788736d30ce
date_modified:  2019/04/28

## Definition

+ = Gefäßerkrankung, bei der sich ein Blutgerinnsel (Thrombus) in einem Blutgefäß bildet
+ können in allen Gefäßen auftreten
+ meistens in den Venen, speziell der tiefen Bein- und Beckenvenen (= Phlebothrombose)

## Ursachen

+ 3 Faktoren (Virchow'sche Trias):
    + Verlangsamter Blutfluss
        + Gips
        + Bettlägrigkeit
        + Krampfadern
        + Schwangerschaft
        + Flugreisen
        + Teilimmobilisation
    + Veränderte Blutzusammensetzung
        + Flüssigkeitsmangel
        + Bluteindickung
        + Pille
        + Rauchen
        + Schwangerschaft
    + Wandveränderungen
        + traumatische Schäden
        + Degeneration (Alter)
        + Entzündungen
        + Diabetes Mellitus

## Risikofaktoren

+ Rauchen
+ Übergewicht
+ Frauen
+ Medikamente (Pille)
+ Schwangerschaft
+ Krebs
+ Operationen
+ Genetisch bedingt

## Symptome

+ Blau-rötliche Verfärbung der Gliedmaße, auch glänzend, glasig
+ Schmerz, Spannungsgefühl, Druckschmerz (Test: Vorderfuß hochziehen)
+ Schwellung (Messen mit Markierung)
+ betroffene Gliedmaße wärmer
+ Schmerzlinderung bei Hochlagerung

## Komplikationen

+ Lungenembolie

## Maßnahmen

+ Hochlagern
+ Weichlagern
+ allg. Maßnahmen

## Zu beachten

+ Zielklinik: Dermatologie