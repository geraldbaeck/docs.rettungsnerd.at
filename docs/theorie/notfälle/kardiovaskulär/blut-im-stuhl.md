title:          Blut im Stuhl
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Kardiovaskulär
md5:            f05a83bfcd102ae78fd57f8f58786daa
date_modified:  2019/01/07

## Ursachen
+ Blutung im Verdauungstrakt
    + Hämorrhoiden
    + Schwere Darminfektion
    + Polypen
    + tumore

## Symptome
+ Blut im Stuhl
+ teerfarbener Stuhl
+ dem Stuhl aufgelagertes Blut (Hämorrhoiden)

## Maßnahmen
+ Flache Rückenlage mit angezogenen Beinen (Knierolle)