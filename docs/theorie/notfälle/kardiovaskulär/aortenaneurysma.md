title:          Aortenaneurysma
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Notfälle Kardiovaskulär
md5:            2ee84c173235e29a1d3e3efefa31aadb
date_modified:  2019/04/21
date:           2019/04/21

## Definition

+ Aneurysma = Aussackung einer Arterie
    + Aneurysma verum  (echtes Aneurysma) = sackförmige Ausbuchtung
    + Aneurysma spurium (falsches Aneurysma) = Bluthöhle durch Verletzung
    + Aneurysma dissecans (Dissektion) = Riss der Intima, Intima wird von der Media abgelöst => falsches Lumen, tlw. mit Reentry

## Ursachen

+ Arteriosklerose
+ Hypertonie
+ entzündliche Erkrankungen
+ genetische Erkrankungen
+ Männer häufiger
+ im Alter häufiger

## Komplikationen

+ Bildung von Thromben an der Aneurysmawand
+ => Emboliequelle => akuter peripherer Verschluss
+ Dissektion (eher im Thorax)
+ Ruptur (eher im Abdomen)

## Thorakales Aortenaneurysma (TAA)

+ akuter, teilweise wandernder Vernichtungsschmerz (80%) im Brustkorb oder zw den Schulterblättern
+ manchmal Ausstrahlung ins Abdomen
+ ähnlich ACS
+ eventuell auch Apoplex-Symptomatik wenn ein Gerhirnversorgendes Gefäß betroffen
+ Puls- und Blutdruckdifferenz

## Bauchaortenaneurysma

+ meist uncharakteristische Symptome
+ eher selten plötzlicher Zerreißungsschmerz im Abdomen + Volumenmangelschock
+ häufiger sind unspezifische Ischalgie-ähnliche Symptome
    + Lendenwirbelsäule
    + ins Gesäß und Beine ausstrahlend
    + CAVE: Verwechslungsgefahr mit Lumboischialgie
+ fehlende oder abgeschwächte Leistenpulse

## Maßnahmen

+ leicht erhöhter Oberkörper
+ NEF
+ engmaschige Kontrolle der Vitalfunktionen
+ Ruptur: Blutdruck Systole bei 90 halten (Permissive Hypotension)