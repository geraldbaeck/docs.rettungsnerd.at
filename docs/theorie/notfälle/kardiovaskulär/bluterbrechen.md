title:          Bluterbrechen
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Kardiovaskulär
md5:            354b7afaf78fb20a9882c869a44fa679
date_modified:  2019/01/07

## Ursachen  
+ Erkrankungen des oberen Verdauungstraktes
+ Magen- oder Zwölffingerdarmgeschwür
+ Krampfadern der Speiseröhre (Ösophagusvarizen)

## Symptome  
+ Kaffesatzartiges Erbrechen
+ Übelkeit

## Maßnahmen  
+ erhöhter Oberkörper
+ evt prophylaktische Seitenlage