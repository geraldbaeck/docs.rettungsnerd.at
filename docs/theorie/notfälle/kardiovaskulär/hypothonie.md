title:          Hypotonie
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Kardiovaskulär
md5:            60fd04427c631fdd6f2b1fda58df12b1
date_modified:  2019/04/21
date:           2019/04/20

## Definition

RR <100 / <60

## Ursachen

+ Erblich bedingt
+ Blutarmut
+ Herzrhythmusstörung
+ Med. Nebenwirkungen
+ zu rasches Aufsetzen

## Symptome

+ Blässe
+ kalte Extremitäten
+ Schwindel
+ Schwäche
+ Schwarz vor Augen

## Komplikationen

+ Kollaps
+ Synkopen
+ Schockzeichen

## Maßnahmen

+ Flache Lagerung, erhöhte Beine
+ Trinken lassen
+ Beengende Kleidungsstücke öffnen
+ zu ruhiger Atmung anleiten
+ allg. Maßnahmen

## Anamnese

+ Krankengeschichte
+ Blutdruckprotokoll
+ Medikamentenliste