title:          Herzrhythmusstörungen
date:           2019/01/06
version:        0.0.6
authors:        gerald@baeck.at
ankikat:        Notfälle Kardiovaskulär
md5:            8516a9036f7815ed52995f32a93b9a26
date_modified:  2019/06/05

## Formen

+ Tachykardie oder Bradykardie, mit schmalen oder breiten QRS Komplexen
+ unregelmäßige Herztätigkeit (Arryhthmie)
    + Zusatzschläge (Extrasystole)
    + Vorhofflimmern
+ AV-Blöcke
    + Grad I
        + verzögerte Erregungsleitung
        + PQ-Zeit >200ms (>5mm)
    + Grad II Wenckebach
        + PQ Zeit verlängert sich kontinuierlich
        + bis QRS ausfällt
        + 70% AV-Knoten 30% HIS-Bündel
    + Grad II Mobitz
        + QRS Komplexe fallen aus
        + ohne verlängerte PQ Zeit
        + 30% HIS-Bündel 70% darunter
    + Grad III
        + P-Welle und QRS-Komplex agieren unabhängig
+ Ersatzrhythmen
    + AV-Knoten-Rhythmus
    + Ventrikulärer Ersatzrhythmus (20-40)
+ Schenkelblöcke

+ führen zu Minderdurchblutung des Gehirns, des Herzmuskels sowie auch anderer Organe
+ genaue Art der Störung ist nur mittels EKG feststellbar
+ häufigste Störung: Vorhofflimmern

### Elektrische Formen des Kreislaufstillstandes

| schockbare | nicht schockbar |
| --- | --- |
| Kammerflimmern/-flattern | PEA |
| Pulslose ventrikuläre Tachykardie | Asystolie |

## Symptome

+ Herzrasen, Herzklopfen, Herzstolpern
+ Schwindel, Hypotonie, Synkope
+ Beklemmungsgefühl
+ Retrosternales Druckgefühl
+ Atemnot
+ Bewusstseinstrübung bis Bewusstlosigkeit
+ Pulslosigkeit
+ Von harmlos (VHF) bis tödlich (VF) alles möglich

## Komplikationen

+ Atem-Kreislauf-Stillstand

## Maßnahmen

+ Lagerung mit erhöhtem Oberkörper
+ Beengende Kleidungsstücke öffnen
+ Allg. Maßnahmen
+ Evtl. vorhandene Schrittmacherpass ins KH mitnehmen

## Anamnese

+ Herzschrittmacher vorhanden?
+ Krankengeschichte