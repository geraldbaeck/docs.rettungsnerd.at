title:          Hypertensive Krise
desc:           Hochdruckkrise
date:           2019/01/06
version:        0.0.6
authors:        gerald@baeck.at
ankikat:        Notfälle Kardiovaskulär
                HeavyRotation
md5:            6e241d2ac68b2f19b787abe6e2f03ee9
date_modified:  2019/06/12

## Definition

+ =starker Anstieg des Blutdrucks (>200mmHg systolisch)
+ mit lebensbedrohlicher Organbeteiligung = **Hypertensiver Notfall**.

## Ursachen

+ oft nicht genau erkennbar
+ chronische arterille Hypertonie
+ Medikationsfehler
+ Nebennierentumor
+ Alkoholentzug
+ Drogenmissbrauch (Kokain, Amphetamine)

## Symptome

+ Schwindel, Sehstörungen
+ Kopfschmerzen
+ Gesichtsrötung
+ Angst
+ Verwirrtheit
+ Übelkeit, Erbrechen
+ Epistaxis
+ Einblutung ins Auge
+ Alle Symptome von Schlaganfall und Akutem Koronarsyndrom

## Komplikationen

+ Angina Pectoris
    + höherer Widerstand im Gefäßsystem
    + => Mehrarbeit des Herzens mit erhöhtem Sauerstoffverbrauch
    + Angina-Pectoris-Anfall möglich
+ Schlaganfall aufgrund einer Gefäßzerreißung
+ Überbelastung => akutes Linksherzversagen mit Lungenödem

## Maßnahmen

+ Keine körperliche Anstrengung
+ Lagerung erhöhter Oberkörper, Beine runter => Vorlast senken
+ Beengende Kleidungsstücke öffnen
+ zu ruhiger Atmung anleiten
+ allg. Maßnahmen

## Anamnese

+ Krankengeschichte
+ Blutdruckprotokoll
+ Medikamentenliste