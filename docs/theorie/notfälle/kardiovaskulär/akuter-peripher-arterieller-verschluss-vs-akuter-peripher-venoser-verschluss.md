title:          Arterieller- vs. Venöser Verschluss
desc:           Akuter peripher-arterieller Verschluss vs. Akuter peripher-venöser Verschluss
date:           2018/12/27
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Notfälle Kardiovaskulär
md5:            9a055562328db4c5b3c00b73fc79e0f9
date_modified:  2019/01/07

| Symptom an der Gliedmaße | arterieller Verschluss | venöser Verschluss |
| --- | --- | --- |
| `Hautfarbe` | blass | gerötet bis zyanotisch |
| `Hauttemperatur` | kalt | warm |
| `peripherer Puls` | nicht tastbar | tastbar |
| `Venenfüllung` | schlecht | gut |
| `Ödeme` | keine | meistens |
| `Schmerzen` | plötzlich, oft Peitschenschlagartig | Druckschmerz |