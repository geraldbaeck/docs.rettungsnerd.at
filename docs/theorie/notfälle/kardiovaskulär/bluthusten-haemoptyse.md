title:          Bluthusten
desc:           Hämoptyse
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle Kardiovaskulär
md5:            24c95046b7136d310b8a37e59ce3444b
date_modified:  2019/01/07

## Ursachen  
+ TBC
+ Lungenkrebs
+ Lungenembolie
+ Herzkrankheiten

## Symptome  
+ schaumig-blasiger Auswurf
+ Atemnot
+ Unruhe
+ Angst vor Ersticken

## Komplikationen  
+ ja nach Grunderkrankung

## Maßnahmen  
+ Erhöhter Oberkörper
+ Nierentasse und Zellstoff
+ O&#8322; bei Atemnot
+ PSA nicht vergessen!!!