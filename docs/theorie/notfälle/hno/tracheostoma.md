title:          Tracheostoma Komplikationen
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Notfälle HNO
md5:            aed2079c4867ced6355004d3dbda895a
date_modified:  2019/05/08
date:           2019/05/01

## Ursachen

+ Nach Operationen im Mund-/Kehlkopfbereich (nach Mundboden-CA, Larynx-CA)
+ Entfernung des Kehlkopfes

## Komplikationen

+ Filterfunktion von Nase und Mundhöhle fallen weg
+ Schleimhäute trocknen rasch aus => Neigung zu Schleimbildung & Entzündungen
+ Pat. kann nicht mehr sprechen.
+ **Notfall: Atemnot aufgrund Verstopfung der Kanüle**

## Maßnahmen

+ Absaugen
    + Entfernung von Innenkanüle (Seele), Sprechventil und/oder HME Filter (einteilige Kanülen nicht entfernen)
    + Absaugen (steriles Arbeiten, Schlingengriff)
    + Katheder ohne Sog einführen
    + Mit Sog unter Drehbewegung herausziehen (max 10-15 sek)
    + mindestens 30 sec Pause zwischen Absaugvorgängen
+ Beatmung
    + direkt über das Tracheostoma
    + falls nicht möglich mit Kindermaske über dem Tracheostoma
+ O&#8322;-Versorgung der Öffnung
+ ggf Transport