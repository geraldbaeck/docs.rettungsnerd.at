title:          Blutungen aus dem Mund
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Notfälle HNO
md5:            9acd42e682ede0b71f7a4d0650a096e3
date_modified:  2019/05/01
date:           2019/05/01

## Ursachen

+ Hämoptyse (Bluthusten)
+ Hämatemesis (Bluterbrechen)
+ Verletzung im Rachen oder Zungen (Zungenbiss nach Epi)
+ Tumorblutung
+ postoperativ (Tonsillektomie)

## Maßnahmen

+ Blutungsquelle feststellen (Anamnese)
+ Bei leichter Blutung: Transport auf entsprechende Abteilung
+ Starke Blutung: NA nachrufen
+ Monitoring
+ Kreislaufstabilisierung → zügiger Transport