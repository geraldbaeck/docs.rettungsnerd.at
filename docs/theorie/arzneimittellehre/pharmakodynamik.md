title:          Pharmakodynamik
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Arzneimittellehre
md5:            56d228389977836d8626d9821468beba
date_modified:  2019/06/13
date:           2019/04/04

## Definition

beschreibt die Wirkung und Nebenwirkung von Medikamenten.

## Wirkungsort

+ Rezeptoren
    + ⍺1 im Gefäßsystem > Vasokostriktion Adrenalin
    + β1 am Herzen > Schlagkraft und Herzfrequenz erhöht, sowie Erregungsleitung beschleunigt Metoprolol (β-Blocker)
    + β2 in der Lunge > Bronchodilatation Fenoterol (Berodual)
+ Transportsysteme
    + Protonenpume im Magen Pantoprazol(Pantoloc)
+ Ionenkanäle
    + Na-Kanal Lidocain (Lokalanästhetikum, Antiarrhythmikum)
+ Enzyme
    + Tenecteplase (Metalyse)
+ Proteinbiosynthese

## Wirkungmechanismus

### Agonismus

Medikament löst eine bestimmte Wirkung aus
zB Diazepam/Benzos;

### Antagonismus

Medikament verdrängt den Agonisten vom Rezeptor

#### Kompetetiver Antagonismus

Medikament verdrängt einen Agonisten von der Bindungsstelle.

| Rezeptor | Agonist | Antagonist | Erwünschte Wirkung |
| --- | --- | --- | --- |
| | Morphin | Naloxon | |
| H1 | Histamin | Cimetidin (Fenistil) | Antiallergene Wirkung |
| ß1 | Adrenalin | Metoprolol (ß-Blocker) | HF- & RR-Senkung |
| Benzodiazepin-Rezeptor | Diazepam | Flumazenil (Anexate) | Antagoniserung der sedierenden Wirkung |

#### Funktioneller Antagonismus

Medikament wirkt dem Agonisten über einen anderen Mechanismus entgegen.
Bsp: Bronchospasmus durch Histamin (über H1- Rezeptor), Broncholyse durch Agonismus an β2- Rezeptoren durch Fenoterol

## Dosierung

abhängig von:

+ Alter
+ Gewicht
+ Begleiterkrankungen und Organfunktionen

## Nebenwirkungen

= Wirkungen eines Medikamentes, die nicht erwünscht sind und neben der erwünschten Wirkung auftreten

+ Sind von der Dosierung abhängig
+ „Nur die Dosis macht das Gift!“
+ Von leichten Beschwerden bis Anaphylaktischer Schock und Tod

## Wechselwirkungen

= Wirkungsveränderungen eines Medikamentes, die von einem anderen Medikament ausgelöst werden, das gleichzeitig im Körper vorhanden ist.

Auswirkungen:

+ Wirkungssteigerung
+ -abschwächung
+ -verlängerung
+ –verkürzung
+ Wirkungsverlust