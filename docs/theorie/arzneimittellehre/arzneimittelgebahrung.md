title:          Arzneimittelgebarung
version:        0.0.5
authors:        gerald@baeck.at
ankikat:        Arzneimittellehre
md5:            086bf156556af15dba974a1414ed0325
date_modified:  2019/06/27
date:           2019/04/04

## Lagerung im Fahrzeug/Ampullatorium

+ alphabetisch nach Wirkstoff
+ alt vor neu
+ Temperatur kühl
+ Ablaufdatum nur gültig bei **sachgerechter Lagerung**:
    + dunkel
    + erschütterungsfrei
    + keine Temperaturschwankungen
+ Sondermüll wenn:
    + ausgeflockt
    + trüb
    + gefroren