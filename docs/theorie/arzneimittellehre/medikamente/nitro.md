title:          Nitrolingual-Spray
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Arzneimittellehre
md5:            ae5d0629ec2fd07954c775bcdea182ee
date_modified:  2019/05/01
date:           2019/04/04

| | |
| --- | --- |
| Wirkstoff | Nitroglycerin |
| Wirkung | Glatte Muskulatur der Gefäße wird entspannt, Rückstrom zum Herzen verringert, O&#8322;-Bedarf des Herzens nimmt ab |
| Indikation | Kardiales Lungenödem |
| Klinik | Dyspnoe, Tachypnoe, Angst, Unruhe, Rasselndes/Brodelndes Atemgeräusch, Husten, schaumiger Auswurf |
| Dosierung | Pumpspray: 1 Hub unter die Zunge |
| Nebenwirkungen | Kopfschmerzen, Flush-Symptomatik, Rascher RR- Abfall |

## PatientInneninformation

+ Aufklärung über Maßnahme
+ Übelkeit / Kopfschmerzen möglich
+ Sublinguale Verabreichung

## Kontraindikationen

+ RR syst. < 130 mmHg
+ Einnahme von Phosphodiesterasehemmern (z.B. Viagra, Levitra oder Cialis) innerhalb der letzten 48 Stunden
+ Hirnblutung Hirnödem
+ Kinder (relative Kontraindikation)
+ Bekannte Allergie gegen Nitropräparate