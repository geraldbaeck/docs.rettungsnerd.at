title:          Mexalen
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Arzneimittellehre
md5:            45414ceaffba02a71c4ee296d9a160a2
date_modified:  2019/04/04
date:           2019/04/04

| | |
| --- | --- |
| Wirkstoff | Paracetamol |
| Wirkung |  Schmerzstillend, fiebersenkend, leicht entzündungshemmend, hemmt die Bildung von Entzündungsmediatoren (Prostaglandine) |
| Indikation | Fieber mit Krampfanamnese |
| Klinik | >38,5 °C, Fieberkrämpfe anamnestisch, Krampf bei eintreffen sistiert |
| Dosierung | 0 - 2a: 1/2 Zäpfchen (=125mg), 2 - 8a: 1 Zäpfchen (=250mg) |
| Nebenwirkungen | Leberschädigend (v.a. bei Überdosierung), Blutbildveränderungen|

## PatientInneninformation

+ Aufklärung über Maßnahme
+ rektale Verabreichung (durch Eltern)

## Kontraindikationen

+ Bekannte Unverträglichkeit
+ Überschreitung der Tageshöchstdosis
    + 0 - 2a: 500 mg
    + 2 - 8a: 1000 mg