title:          Sterilisation
date:           2018/12/28
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Hygiene
md5:            b17533ec6ba6071e4d595d4fba011541
date_modified:  2019/01/07

Nahezu völlige Keimfreiheit. Kann nur theoretisch erreicht werden. 

Steril müssen sein:

+ Wundversorgungsmaterial
+ Alles, was unter Hautoberfläche gelangt (Nadeln etc.)
+ Alles, was für Operationen bestimmt ist (Handschuhe, Instrumente etc.)