title:          Desinfektion
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Hygiene
md5:            0e8c792c20ea880bab79687b08579abc
date_modified:  2019/01/07

## Definition
+ Abtötung bzw irreversible Inaktivierung von krankheitserregenden Keimen an und in kontaminierten Objekten.
+ dient der Unterbrechung der Infektionskette

## Mittel und Verfahren
+ Wischdesinfektion
+ Hautdesinfektion
+ Geräte- und Instrumentendesinfektion