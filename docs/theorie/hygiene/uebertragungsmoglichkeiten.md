title:          Übertragungsmöglichkeiten
date:           2018/12/28
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Hygiene
md5:            936e3b24d5588a1baf9f15e8827c0a61
date_modified:  2019/01/07

| | Beschreibung | Beispiel |
| --- | --- | --- |
| `Direkter Kontakt` | mit der Infektionsquelle | |
| `Tröpfcheninfektion` | Husten / Niesen, Tröpfchen > 5 µm &#8960; | Influenza |
| `Aerogene Infektion` | Husten / Niesen, Aerosole < 5 µm &#8960; länger im Raum => Übertragung auch über größere Strecken/Dauer möglich | Feuchtblattern |
| `Schmierinfektion` | direkte und indirekte Kontaktinfektion | Polio, Herpes, Hepatitis A |
| `fäkal-oral` | Mit Stuhlkeimen verunreinigte Nahrung oder Wasser | |
| `Insektenstich, Tierbiss` | | FSME, Borreliose |
| `Geschlechtsverkehr` | | HIV, Syphilis |
| `diaplazentar` | Von Mutter auf Embryo | Syphilis, Windpocken |