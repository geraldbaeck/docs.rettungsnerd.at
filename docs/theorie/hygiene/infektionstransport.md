title:          Infektionstransport
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Hygiene
md5:            0f39d53e24a36538c3fde0022418f09c
date_modified:  2019/06/06
date:           2019/04/01

## Vorbereitung

+ Schiebefenster zum Fahrer schließen, mit Klebeband sichern um gedankenloses Öffnen zu erschweren
+ Schreibmöglichkeit vorbereiten zur Kommunikation mit FahrerIn
+ Belüftung abschalten, Lüftungsauslässe verkleben
+ Patientenraum möglichst leeren

## PSA anlegen

FahrerIn leitet die beiden anderen SanitäterInnen an.

## Tranportdurchführung

+ ZAO
    + Kontakt mit dem BO-Krankenhaus aufnehmen und Details der Übergabe besprechen
    + PSA ist bereits vor dem ersten Patientenkontakt voll angelegt  
+ FahrerIn BO/ZAO
    + FahrerIn schützt sich am BO durch Einmalhandschuhe, FFP3 Maske und Abstand zum Patienten
    + FahrerIn ist für das Öffnen und Schließen von Türen, Kommunikation und Dokumentation zuständig
    + Vor Öffnen des Fahrerraum, PSA ablegen und hygienische Händedesinfektion  
+ SanitäterInnen BO/ZAO
    + Patient erhält FFP2 Maske ohne Ausatemventil oder eine OP Maske
    + auf wertschätzenden Umgang achten  
+ AO
    + durch FahrerIn anmelden und Details der Übergabe klären
    + Bei offensichtlicher Kontamination (Anhusten, Blutspritzer) Stelle sofort desinfizieren und kontaminiertes Material am AO übergeben  

## Nachbereitung

+ SanitäterInnen bleiben im Patientenraum
+ An der Dienststelle Desinfektion durchführen erst danach PSA ablegen