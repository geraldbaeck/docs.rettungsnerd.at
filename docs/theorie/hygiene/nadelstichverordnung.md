title:          Nadelstichverordnung
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Hygiene
md5:            25e1f64edc6b6c4875e63b13fbc7a4e0
date_modified:  2019/01/07

+ Gefahren sind in die Arbeitsplatzevaluierung aufzunehmen
+ integrierte Schutzmechanismen müssen verwendet werden
+ Recapping Verbot
+ Entsorgung in eigenen Abwurfbehälter
+ Mitarbeiter sind einzuschulen

## Kanülen
folgende Maßnahmen dienen Verhütung von Nadelstich- und Schnittverletzungen:

+ Kanülen nie in Hülle zurückstecken
+ Kanülen und geöffnete Glasampullen sofort nach Benutzung persönlich in Sammelbehälter entsorgen
+ Nie in Sammelbehälter greifen
+ Sammelbehälter nur ca. zwei Drittel füllen, nicht umfüllen oder ausleeren