title:          Dispositionsprophylaxe
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Hygiene
md5:            65fce276d2602aae3834ac103574de91
date_modified:  2019/01/07

## Definition
+ Vorbeugende Maßnahmen um das Infektionsrisiko zu senken bzw. auszuschließen
+ zielt  nicht auf eine Vermeidung eines Erregerkontaktes, sondern auf den Ausbruch einer Erkrankung oder einen weniger schweren Krankheitsverlauf nach Infektion ab

## Beispiele/Maßnahmen
+ Impfen
+ PSA
+ Hygienische Händedesinfektion
+ Antiinfektiva (Antibiotika, Malariaprophylaxe)
+ Maßnahmen zur Steigerung der individuellen Infektabwehr
    + Bewegung
    + Ernährung
    + persönliche Hygiene