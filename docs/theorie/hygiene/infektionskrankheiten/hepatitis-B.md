title:          Hepatitis B
version:        0.0.6
authors:        gerald@baeck.at
ankikat:        Hygiene
md5:            45ae0160dd6f2e2c3a779db06da0dae2
date_modified:  2019/06/07
date:           2019/04/04

## Definition

Der Erreger ist das Hepatitis B-Virus.

+ Das Virus ist extrem ansteckend,
+ ein Tropfen infiziertes Blut, ist auch verdünnt auf 1:1,000.000 infektiös!
+ sehr stabil gegenüber Hitze und Desinfektionsmittel
+ sichere Vernichtung besteht erst bei 10 min Erhitzung über 90°
+ Inkubationszeit 2-6 Monate

## Übertragung

+ Blut (kontaminierte Spritzen und Nadeln)
+ Virushaltige Körperflüssigkeiten wie Speichel
+ Geschlechtsverkehr
+ Unreine Gerätschaften beim Tätowieren und Piercen
+ Zahnbürsten und Rasierklingen
+ Von der Mutter auf das Kind (diaplazentar)

## Symptome

+ Vorstadium
    + Gelenksbeschwerden
    + Müdigkeit
    + Appetitlosigkeit
+ Akute Hepatitis
    + Leberzerfall
    + Ikterus
    + Blutgerinnungsstörung
    + Leberversagen
    + Tod
+ Fulminante Hepatitis
    + Erkrankung binnen Stunden > 60% lethal
+ Abheilung mit Dauerimmunität, oft mit Leberschaden oder
+ Chronische Hepatitis bei bestehen über 6 Monaten
    + Wechsel von klinisch aktiven Infektionen und symptomfreien Intervallen
    + Leberzirrhose
    + bei ca. 15% Leberkrebs.

## Maßnahmen / Prophylaxe

+ Schutzimpfung
+ Hygienische Händedesinfektion
+ Wischdesinfektion