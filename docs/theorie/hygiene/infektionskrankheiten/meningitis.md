title:          Meningitis
date:           2019/01/06
version:        0.0.4
authors:        gerald@baeck.at
ankikat:        Hygiene
md5:            4147857341469916060be84b8d0d6079
date_modified:  2019/04/21

## Definition

+ = Entzündung der Hirnhaut
+ Erreger: Bakterien (Meningokokken)
+ Tröpfcheninfektion
+ Hohe Letalität (Sterberate), da die Diagnose oft zu spät gestellt wird 

## Symptome

+ Hohes Fieber
+ Nackensteifheit
+ Kopfschmerzen
+ Lichtempfindlichkeit
+ Erbrechen
+ Bewusstseinstrübung

## Vorbeugung

+ Impfen
+ Antibiotika bei Exposition

## Komplikationen

+ Hirnabzess
+ Meningokokkensepsis
    + Blutgerinnungsstörung
    + Multiorganversagen
+ manchmal tödlich

## Maßnahmen

+ Expositionsprophylaxe Typ 2
    + Mundschutz
    + Handschuhe
    + gründliche Wischdesinfektion
+ Bei nicht wissentlichem Kontakt evt AB Prophylaxe nötig => Rücksprache mit Oberarzt erforderlich

[Why is meningitis so dangerous?](https://www.youtube.com/watch?v=IaQdv_dBDqM){: .youtube}