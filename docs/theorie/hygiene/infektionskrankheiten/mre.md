title:          MRE
desc:           Multiresistente Erreger
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Hygiene
md5:            c2021dbd612456b84eaff09d3619ca26
date_modified:  2019/01/07

zb ESBL, VRE, MRSA. Bei Transport ist abzuklären, ob sich der Erreger im Nasen-Rachen-Raum befindet oder nur Wunden befallen sind.

## Symptome
+ häufig chronische Wunden
+ Abszesse
+ Pneumonie
+ Harnwegsinfekt

## Maßnahmen
+ Expositionsprophylaxe Typ 1
+ Bei engem Patientenkontakt zusätzlich Einwegschürze
+ Wischdesinfektion
+ Patient
    + hygienische Händedesinfektion
    + frische Kleidung
    + ggf Maske
    + sämtliche Maßnahmen gelten nur für den Transport, für gesunde Familienmitglieder stellt der Keim üblicherweise kein Risiko dar