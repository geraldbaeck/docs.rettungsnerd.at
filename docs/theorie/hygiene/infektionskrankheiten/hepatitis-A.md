title:          Hepatitis A
version:        0.0.3
authors:        gerald@baeck.at
ankikat:        Hygiene
md5:            43e3d5367387d79cba889603d2aefd0f
date_modified:  2019/04/04
date:           2019/04/04

## Definition

Der Erreger ist das Hepatitis A-Virus.

+ hoch infektiös
+ relativ thermostabil
+ säureresistent
+ bereits wenige Viren genügen für eine Infektion

## Übertragung

+ Schmierinfektion
+ selten fäko-oral
+ kommt weltweit vor, besonders stark in den Tropen
+ Sichere Zerstörung nach 5 Minuten kochen
+ Inkubationszeit 4-6 Wochen

## Symptome

+ Vorstadium
    + Fieber
    + Erbrechen
    + Durchfall
+ Leberentzündung mit erhöhten Leberwerten, <50% Ikterus
    + lehmartiger Stuhl
    + dunkler Harn
    + Müdigkeit
    + Völlegefühl, Appetitlosigkeit
    + Gelenk-, Muskelschmerzen
    + Juckreiz
    + Grippeartige Symptome

## Maßnahmen / Prophylaxe

+ Schutzimpfung
+ Hygienische Händedesinfektion
+ Wischdesinfektion