title:          SARS
desc:           Schweres Akutes Respiratorisches Syndrom
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Hygiene
md5:            ba05d7a93af34be012dfe302746592f9
date_modified:  2019/01/07

## Definition
+ Kontakt- und Tröpfcheninfektion
+ ähnlich einer atypischen Pneumonie

## Symptome
+ Plötzlich auftretendes schnell steigendes Fieber
+ Halsentzündung mit Husten & Heiserkeit
+ Atemnot
+ Muskelschmerzen
+ Kopfschmerzen
+ Entzündung beider Lungenflügel

## Komplikationen
+ Tod

## Maßnahmen
+ Transport durch HRI Team