title:          Noroviren
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Hygiene
md5:            5145c00cfe12f3b7f0af45ff10bdacac
date_modified:  2019/01/07

## Definition
+ plötzlicher Brechdurchfall von 12-24 Std. Dauer
+ fäkoorale Übertragung.
+ Patienten Häufig < 5 und > 70 Jahre

## Symptome
+ akut einsetzender Brechdurchfall

## Komplikationen
+ Dehydration

## Maßnahmen
+ Exp.prophylaxe Typ 2.
+ Mundschutz ist für Patienten nicht möglich. Selbst Mundschutz tragen.
+ Noroviren sind sehr umweltstabil. Strengste Handhygiene und gründliche Wischdesinfektion erforderlich