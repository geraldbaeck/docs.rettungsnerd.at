title:          Gonorrhoe
desc:           Tripper
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Hygiene
md5:            e8689fd301d92636d65ef2ceb4166fc0
date_modified:  2019/04/21
date:           2019/04/20

## Definition

+ Erreger: Gonokokken, semmelförmig und paarig angelegte Bakterien

## Übertragung

+ Kontaktinfektion beim Geschlechtsverkehr
+ Befallen wird die dabei betroffene Schleimhaut
+ Eine Infektion des Kindes bei der Geburt durch die Mutter ist ebenfalls möglich
+ Inkubationszeit: einige Tage

## Symptome

+ keine Symptome bei 5% der Betroffenen
+ Eitrige Entzündungen der befallenen Schleimhaut (oral, genital, anal), brennen, Schmerzen
+ Ausbreitung der Infektion bis zu den Nieren, der Prostata oder den Eierstöcken

## Vorbeugung

+ Kondom
+ bei Neugeborenen Crede´sche Prophylaxe (1 Tropfen AB-Lösung oder Silberacetat in jedes Auge)