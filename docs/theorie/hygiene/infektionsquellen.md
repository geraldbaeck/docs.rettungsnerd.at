title:          Infektionsquellen
date:           2019/01/06
version:        0.0.2
authors:        gerald@baeck.at
ankikat:        Hygiene
md5:            cb480678fc7a8af2eb15d883e9fe17d8
date_modified:  2019/01/07

Infektionen sind Erkrankungen, die durch Eindringen und Vermehrung von Mikroorganismen im Körper entstehen.

## Quellen
+ Infizierte Menschen am Ende der Inkubationszeit und während Erkrankung, nach überstandener Krankheit, ohne Erkrankung (Keimträger)
+ Leichen
+ Umwelt
+ infizierte Tiere

## Krankheitserreger
+ Viren
+ Bakterien
+ Pilze
+ Würmer u.a.