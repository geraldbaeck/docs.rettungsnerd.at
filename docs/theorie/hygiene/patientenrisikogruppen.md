title:          Patientenrisikogruppen
date:           2018/12/28
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Hygiene
md5:            335722e30cfb0468f89a8845d7ed1861
date_modified:  2019/01/07

| PRG | Expositionsprophylaxe | &nbsp; |
| --: | --: | --- |
| `1` | 1 | Kein Anhaltspunkt für Vorliegen einer Inf.krankheit, kein Risiko |
| `2` | 1 | Infektion besteht, jedoch kein Risiko für Übertragung durch übliche Kontakte bei Transport |
| `3` | 2-3 | Hoch infektiöse Erkrankung oder Verdacht darauf (zB: Brechdurchfall durch Noroviren, Masern, Mumps, Tuberkulose...) |
| `4` | 1-2 | Hohe Infektionsgefährdung für Patienten (Frühgeburt, Verbrennungspatienten, immunsupprimierte) |