title:          Epidemie, Pandemie, Endemie
date:           2018/12/28
version:        0.0.1
authors:        gerald@baeck.at
ankikat:        Hygiene
md5:            c652282287424353e76cba2a842c5ec7
date_modified:  2019/01/07

| | Auftreten der Infektionskrankheit | Beispiele |
| --- | --- | ---|
| `Epidemie` | zeitlich & örtlich | Grippe, Salmonellen |
| `Pandemie` | über Länder und Kontinente | Pest, AIDS, spanische Grippe 1918 |
| `Endemie` | örtlich begrenzt | FSME, Malaria, Lepra |