#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
anki_export.py
Exports all markdown files from the docs folder to anki importable txt files into the anki folder.

Usage: anki_export.py.py [-hv]
       anki_export.py.py [--logLevel=<LOGLEVEL>][--environment=<ENV>]

Options:
  -h --help             Show this screen.
  -v --version          Show version.
  --logLevel=LOGLEVEL   The level of the logging output  [default: INFO]
  --environment=ENV     The execution environment
                        (development, staging or production)  [default: development]
"""

__appname__ = "Markdown export to Anki"
__author__  = "Gerald Bäck (https://github.com/geraldbaeck/)"
__version__ = "0.0.1"
__license__ = "UNLICENSE"

DEFAULT_ENVIRONMENT = "development"

# System libraries
import glob
import logging
import logging.config
import os
import re
import shutil
from urllib.parse import urljoin, urlparse

# 3rd party libraries
from docopt import docopt
import markdown

def set_up_logging(logtofile=False, coloredlog=True):
    try:
        logging.config.fileConfig(fname='deploy/logging.conf', disable_existing_loggers=False)

        # Get the logger specified in the file
        logger = logging.getLogger("defaultLogger")
        
        if coloredlog:
            pass

        #markdown.logger.setLevel(logging.WARNING)

        logger.debug("Logging configured.")
        return logger

    except AttributeError:
        raise Exception(
            "Unsupported logLevel. Use [DEBUG, INFO, WARNING, ERROR, CRITICAL]")


def createAnkiLine(question, answer, link=''):

    if link is not '':
        backlink = f"<a href='{link}' id=''>{link}</a>"

        # Helper function to return the domain if present.
        def is_absolute(url):
            """ Returns True if the passed url string is an absolute path
                False if not
            """
            return bool(urlparse(url).netloc)

        # makes all the relative links absolute
        def _href_replace(m):
            href = m.group()
            href_content = href[href.find('"')+1:href.rfind('"')]
            if is_absolute(href[6:-1]):
                return href
            href = href.replace(href_content, urljoin(link, href_content)).replace('docs/', '').replace('.md', '.html')
            return href

        answer = backlink + re.sub('href="(.*md)"', _href_replace, answer)

    return f"{question}, {answer.replace(os.linesep, '').replace(',', '&#44;')}\n"

def main():
    """ Main entry point of the app """
    logger.debug("{!s} started".format(__appname__))

    # del all files in the anki folder first, to avoid old files mitmatch
    files = glob.glob('anki/*')
    for f in files:
        os.remove(f)

    anki_export = dict()  # contains the final export data

    for filename in glob.iglob('docs/**/*.md', recursive=True):
        logger.info(filename)

        link = f"https://nfs.rettungsnerd.at/{filename.replace('docs/', '').replace('.md', '.html')}"
        with open(filename, 'r', encoding='utf-8') as f:

            # open file and convert to html
            md = markdown.Markdown(extensions = ['meta', 'tables', 'def_list', 'attr_list'])
            md.page_root = os.path.dirname(filename)
            html = md.convert(f.read())  # converts tp plain html without metadata
            
            if 'ankikat' in md.Meta:
                for kat in md.Meta['ankikat']:
                    if kat not in anki_export:
                        anki_export[kat] = ""
                    try:
                        anki_export[kat] += createAnkiLine(md.Meta['title'][0].replace(",", " /"), html, link)
                    except KeyError as e:
                        logger.error(f"{filename} has no title!")
                        raise(e)
            else:
                logger.warning(f"{filename} has no ankikat.")
    
    for filepath in glob.iglob('docs/**/*.csv', recursive=True):
        logger.info(filepath)
        path, filename = os.path.split(filepath)
        shutil.copyfile(filepath, f"anki/{filename}")

    for anki_kat, cards in anki_export.items():
        with open(f"anki/{anki_kat} __export.txt", "w") as f:
            f.write(cards)


if __name__ == "__main__":
    """ This is executed when run from the command line """
    arguments = docopt(__doc__, version=__version__)
    ENVIRONMENT = arguments['--environment']
    logger = set_up_logging(arguments['--logLevel'])
    main()
