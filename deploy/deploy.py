#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
deploy.py
- Changes the version number if there was a change detected
- generates an md5 hash for every document to detect changes
- sorts documents withan a given folder

Usage: deploy.py.py [-hv]
       deploy.py.py [--logLevel=<LOGLEVEL>][--environment=<ENV>]

Options:
  -h --help             Show this screen.
  -v --version          Show version.
  --logLevel=LOGLEVEL   The level of the logging output  [default: INFO]
  --environment=ENV     The execution environment
                        (development, staging or production)  [default: development]
"""

__appname__ = "prepare deployment of mkdocs to gitlab"
__author__  = "Gerald Bäck (https://github.com/geraldbaeck/)"
__version__ = "0.0.1"
__license__ = "UNLICENSE"

import datetime
from glob import glob
import hashlib
import logging
import logging.config
import os
import re
import unicodedata

from docopt import docopt
import markdown

def set_up_logging(logtofile=False, coloredlog=True):
    try:
        logging.config.fileConfig(fname='deploy/logging.conf', disable_existing_loggers=False)

        # Get the logger specified in the file
        logger = logging.getLogger("defaultLogger")
        
        if coloredlog:
            pass

        # markdown.logger.setLevel(logging.WARNING)

        logger.debug("Logging configured.")
        return logger

    except AttributeError:
        raise Exception(
            "Unsupported logLevel. Use [DEBUG, INFO, WARNING, ERROR, CRITICAL]")


def createNewMarkdown(filename):
    with open(filename, 'r', encoding='utf-8') as f:
        md = markdown.Markdown(extensions = ['meta'])
        md.page_root = os.path.dirname(filename)
        html = md.convert(f.read())  # converts tp plain html without metadata

        # manipulate metadata
        ## check and compare md5
        ## set version number
        new_md5 = hashlib.md5(medicalReplacements(html).encode('utf-8')).hexdigest()
        if 'md5' not in md.Meta or md.Meta['md5'][0] != new_md5:
            md.Meta['md5'] = [new_md5]
            md.Meta['date_modified'] = [datetime.datetime.now().strftime("%Y/%m/%d")]
            if 'version' in md.Meta:
                ver = md.Meta['version'][0].split('.')
                ver[-1] = str(int(ver[-1]) + 1)
                md.Meta['version'] = ['.'.join(ver)]
            else:
                md.Meta['version'] = ["0.0.1"]
        
        ## set date
        if 'date' not in md.Meta:
                md.Meta['date'] = [datetime.datetime.now().strftime("%Y/%m/%d")]

        saveMarkdown(md, filename)
        
        

def saveMarkdown(md, filename, colwidth=16):
    output = ""
    for k in md.Meta:
        metakey = f"{k}:"
        metaline = f"{metakey.ljust(colwidth)}{md.Meta[k][0]}\n"
        for v in md.Meta[k][1:]:
            metaline += f"{''.ljust(colwidth)}{v}\n" # add additional values
        output += metaline  # recreate metadata for new markdown
    output += "\n" + u"\n".join(md.lines)  # add orignal markdown content
    output = unicodedata.normalize('NFC', medicalReplacements(output.strip()))

    with open(filename, 'w', encoding='utf-8') as f:
        f.write(output)

def medicalReplacements(str):
    str = re.sub(r"([\n ]V)([1-9])", r'\1<sub>\2</sub>', str)  # V1 - V6 Ableitungen
    return str.replace("SpO2", "S<sub>p</sub>O&#8322;").replace("CO2", "CO&#8322;").replace("O2", "O&#8322;")

def main():
    """ Main entry point of the app """
    logger.debug("{!s} started".format(__appname__))
    for fn in glob('docs/**/*.md', recursive=True):
        if not fn.startswith("docs/xtras"):
            logger.info(f"Checking {fn}")
            createNewMarkdown(fn)  # checks version number and date

if __name__ == "__main__":
    """ This is executed when run from the command line """
    arguments = docopt(__doc__, version=__version__)
    ENVIRONMENT = arguments['--environment']
    logger = set_up_logging(arguments['--logLevel'])
    main()